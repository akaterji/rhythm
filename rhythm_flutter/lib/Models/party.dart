class Party {
  final String _userId;
  String _name;
  String _artist;
  String _username;
  bool _isPlaying;
  bool _repeat;
  bool _shuffle;
  String _duration;
  String _position;
  String _url;
  String _trackId;
  String _imageURL;
  Party(
      this._userId,
      this._username,
      this._name,
      this._artist,
      this._url,
      this._duration,
      this._position,
      this._isPlaying,
      this._repeat,
      this._shuffle,
      this._trackId,
      this._imageURL);

  String get userId => _userId;
  String get name => _name;
  String get duration => _duration;
  String get position => _position;
  String get url => _url;
  String get id => _trackId;
  String get imageURL => _imageURL;
  bool get isPlaying => _isPlaying;
  bool get repeat => _repeat;
  bool get shuffle => _shuffle;
  String get username => _username;
  void updateParty(String name, String url, String duration, String position) {
    _name = name;
    _url = url;
    _duration = duration;
    _position = position;
  }

  void setName(String name) {
    _name = name;
  }

  void setDuration(String duration) {
    _duration = duration;
  }

  void setPosition(String position) {
    _position = position;
  }

  void setUrl(String url) {
    _url = url;
  }

  void setRepeat(bool repeat) {
    _repeat = repeat;
  }

  void setShuffle(bool shuffle) {
    _shuffle = shuffle;
  }

  void setPause(bool pause) {
    _isPlaying = pause;
  }

  void setImage(String url) {
    _imageURL = url;
  }

  String get artist => _artist;
  void newSong(String url, String imageURL, bool isPlaying, String position,
      String duration, String name, String artist) {
    _url = url;
    _imageURL = imageURL;
    _isPlaying = isPlaying;
    _position = position;
    _duration = _duration;
    _name = name;
    _artist = artist;
  }
}
