import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../Providers/profile.dart';

class NotificationsModal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _profileProvider = Provider.of<Profile>(context);
    Future.delayed(const Duration(milliseconds: 500), () {
      _profileProvider.notificationsViewed();
    });
    return Scaffold(
      body: Column(
        children: <Widget>[
          Column(
            children: _profileProvider.notifications.map(
              (notification) {
                return Container(
                  child: Row(
                    children: <Widget>[
                      Text(notification['name'] + " wants to add you"),
                      Spacer(),
                      IconButton(
                        onPressed: () {
                          _profileProvider
                              .acceptFriendRequest(notification['userId']);
                        },
                        icon: Icon(Icons.check),
                      ),
                      IconButton(
                        onPressed: () {
                          _profileProvider.removeFriendRequestFromNotifications(
                              notification['userId']);
                        },
                        icon: Icon(Icons.close),
                      ),
                    ],
                  ),
                );
              },
            ).toList(),
          ),
          Spacer(),
          RaisedButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Done'),
          )
        ],
      ),
    );
  }
}

showAlertDialog(BuildContext context) {
  showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.black45,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext buildContext, Animation animation,
          Animation secondaryAnimation) {
        return Center(
          child: Container(
              width: MediaQuery.of(context).size.width - 10,
              height: MediaQuery.of(context).size.height - 80,
              padding: EdgeInsets.all(20),
              color: Colors.white,
              child: NotificationsModal()),
        );
      });
}
