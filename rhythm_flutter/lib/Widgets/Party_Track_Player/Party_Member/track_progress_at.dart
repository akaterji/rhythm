import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import '../../../Providers/party_member.dart';

class TrackProgressAt extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _partyMemberProvider = Provider.of<PartyMember>(context);
    return Container(
      child: Text(
        _partyMemberProvider.getPosition.toString().substring(2, 7),
      ),
      margin: EdgeInsets.only(left: 10),
    );
  }
}
