<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Auth\AuthController@login')->name('login');
    Route::post('register', 'Auth\AuthController@register');
    Route::group([
      'middleware' => 'auth:api'
    ], function () {
        Route::get('logout', 'Auth\AuthController@logout');
        Route::get('user', 'Auth\AuthController@user');
        //tracks
    });
});
Route::group([
    'middleware' => 'auth:api'
  ], function () {
      //tracks
      Route::get('tracks', 'TrackController@index');
      Route::get('tracks/search', 'TrackController@search');
      Route::post('tracks/addToFavorites', 'TrackController@addToFavorites');
      Route::post('tracks/removeFromFavorites', 'TrackController@removeFromFavorites');
      Route::get('tracks/getFavorites', 'TrackController@getFavorites');
     
      //playlists
      Route::get('playlist', 'PlaylistController@index');
      Route::post('playlist', 'PlaylistController@store');
      Route::post('playlists/delete', 'PlaylistController@deletePlaylist');
      Route::get('playlists', 'PlaylistController@getPlaylists');
      Route::post('playlist/add', 'PlaylistController@addToPlaylist');
      Route::post('playlist/delete', 'PlaylistController@removeFromPlaylist');
      Route::post('playlist/addTracks', 'PlaylistController@addMultipleTracksToPlaylist');
      Route::get('playlist/sharedWith', 'PlaylistController@sharedWith');
      Route::get('playlist/notSharedWith', 'PlaylistController@friendsNotSharedWith');
      Route::get('playlist/sharedPlaylists', 'PlaylistController@sharedPlaylists');
      Route::post('playlist/sharedPlaylists/add', 'PlaylistController@addSharedPlaylist');
      Route::post('playlist/sharedPlaylists/remove', 'PlaylistController@removeSharedPlaylist');
      Route::get('playlists/sharedPlaylists', 'PlaylistController@getPlaylistsWithSharedInfo');
      //user
      Route::get('users/search', 'UserController@search');
      Route::get('users/getName', 'UserController@getFullName');
      Route::post('users/add', 'UserController@addFriend');
      Route::post('users/remove', 'UserController@removeFriend');
      Route::get('users/friends', 'UserController@getFriends');
      Route::post('users/sendFriendRequest', 'UserController@sendFriendRequest');
      Route::post('users/removeFriendRequest', 'UserController@removeFriendRequest');
      Route::post('users/removeFriendRequestAfterAccepting', 'UserController@removeFriendRequestAfterAccepting');
      Route::get('users/isFriendRequested', 'UserController@isFriendRequested');
      Route::post('users/recentlyPlayed', 'UserController@addRecentlyPlayed');
      Route::get('users/recentlyPlayed', 'UserController@getRecentlyPlayed');

      Route::post('users/fcmToken/add', 'FcmTokenController@store');
      Route::post('users/fcmToken/delete', 'FcmTokenController@delete');
      Route::post('users/addRecentlyPlayed', 'UserController@addNewRecentlyPlayed');
      Route::get('users/getAllRecentlyPlayed', 'UserController@getAllRecentlyPlayed');

      Route::get('albums', 'AlbumController@index');
  });
