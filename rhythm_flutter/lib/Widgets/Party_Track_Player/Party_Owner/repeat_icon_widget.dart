import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../Providers/party.dart';

class RepeatIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _partyProvider = Provider.of<Party>(context);
    return Container(
      width: 40,
      height: 40,
      child: RawMaterialButton(
        shape: CircleBorder(),
        elevation: 0.0,
        child: _partyProvider.isRepeat == true
            ? Icon(
                Icons.repeat,
                color: Colors.red,
              )
            : Icon(Icons.repeat),
        onPressed: () {
          _partyProvider.toggleRepeat();
        },
      ),
    );
  }
}
