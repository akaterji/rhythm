import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../constants.dart';
import 'package:firebase_database/firebase_database.dart';
import '../Models/user.dart';

import '../Models/party.dart';

class PartyLobby with ChangeNotifier {
  var _loading = true;
  var _isInit = true;
  final String _token;
  final String _userId;
  List<User> _friends = [];
  var _isListening = false;
  final url = kUrl;
  final databaseReference = FirebaseDatabase.instance.reference();
  var _newPartySubscription;
  var _deletedPartySubscription;
  var _onChangePartySubscription = [];
  List<Party> _parties = [];
  PartyLobby(this._token, this._userId);

  Future<void> fetch(url, callback) async {
    try {
      final response =
          await http.get(url, headers: {'authorization': 'Bearer $_token'});
      if (response.statusCode != 200) {
        throw json.decode(response.body)['message'];
      }
      callback(response);
    } catch (error) {
      kError = true;
      kErrorMessage = error;
      notifyListeners();
    }
  }

  Future<void> post(String url, dynamic body, callback) async {
    try {
      final response = await http.post(url,
          headers: {'authorization': 'Bearer $_token'}, body: body);
      if (response.statusCode != 200) {
        throw json.decode(response.body)['message'];
      }
      callback(response);
    } catch (error) {
      kError = true;
      kErrorMessage = error;
      notifyListeners();
    }
  }

  void init() async {
    _isInit = true;
    _loading = true;
    await getFriends();
    _newPartySubscription =
        databaseReference.child('Party').onChildAdded.listen(_onNewParty);
    _deletedPartySubscription =
        databaseReference.child('Party').onChildRemoved.listen(_onRemovedParty);
    _isListening = true;

    notifyListeners();
  }

  void _onRemovedParty(Event event) {
    if (event.snapshot.key == _userId) return;
    if (parties.length == 1) {
      _parties = [];
      notifyListeners();
    } else {
      List<Party> temp = [];
      _parties.forEach((party) {
        if (party.userId != event.snapshot.value['user_id']) temp.add(party);
      });
      _parties = temp;
      notifyListeners();
    }
  }

  void _onChangedParty(Event event, String ownerId) {
    if (event.snapshot.key == 'track_id') {
      databaseReference
          .child('Party')
          .child(ownerId)
          .once()
          .then((DataSnapshot snapshot) {
        var url = snapshot.value['url'];
        var imageURL = snapshot.value['image'];
        var name = snapshot.value['name'];
        var artist = snapshot.value['artist'];
        var position = snapshot.value['position'];
        var duration = snapshot.value['duration'];
        var isPlaying = snapshot.value['isPlaying'];
        List<Party> _temp = [];
        _parties.forEach((party) {
          if (party.userId == ownerId) {
            party.newSong(
                url, imageURL, isPlaying, position, duration, name, artist);
            _temp.add(party);
          } else
            _temp.add(party);
        });
        _parties = _temp;
        notifyListeners();
      });
    }
    if (event.snapshot.key == 'duration') {
      List<Party> _temp = [];
      _parties.forEach((party) {
        if (party.userId == ownerId) {
          party.setDuration(event.snapshot.value);
          _temp.add(party);
        } else
          _temp.add(party);
      });
      _parties = _temp;
      notifyListeners();
    }
    if (event.snapshot.key == 'position') {
      List<Party> _temp = [];
      _parties.forEach((party) {
        if (party.userId == ownerId) {
          party.setPosition(event.snapshot.value);
          _temp.add(party);
        } else
          _temp.add(party);
      });
      _parties = _temp;
      notifyListeners();
    }
    if (event.snapshot.key == 'isPlaying') {
      List<Party> _temp = [];
      _parties.forEach((party) {
        if (party.userId == ownerId) {
          party.setPause(event.snapshot.value);
          _temp.add(party);
        } else
          _temp.add(party);
      });
      _parties = _temp;
      notifyListeners();
    }
  }

  void _onNewParty(Event event) async {
    event.snapshot.value;
    event.snapshot.key;
    var _partyOwnerId = event.snapshot.key;
    if (_friends.isNotEmpty) {
      _friends.forEach(
        (friend) {
          if (friend.id == _partyOwnerId) {
            _onChangePartySubscription.add(databaseReference
                .child('Party')
                .child(_partyOwnerId)
                .onChildChanged
                .listen((Event e) => _onChangedParty(e, _partyOwnerId)));

            var _trackUrl = event.snapshot.value['url'];
            var _trackName = event.snapshot.value['name'];
            var _trackArtist = event.snapshot.value['artist'];
            var _duration = event.snapshot.value['duration'];
            var _position = event.snapshot.value['position'];
            var _repeat = event.snapshot.value['repeat'];
            var _shuffle = event.snapshot.value['shuffle'];
            var _isPlaying = event.snapshot.value['isPlaying'];
            var _trackId = event.snapshot.value['track_id'];
            var _trackImageURL = event.snapshot.value['image'];
            if (_parties.isEmpty) {
              _parties.add(
                Party(
                    _partyOwnerId,
                    friend.firstName,
                    _trackName,
                    _trackArtist,
                    _trackUrl,
                    _duration,
                    _position,
                    _isPlaying,
                    _repeat,
                    _shuffle,
                    _trackId,
                    _trackImageURL),
              );
              if (!_isInit) {
                _loading = false;
                notifyListeners();
              }
            } else {
              var _isAdded = false;
              _parties.forEach((party) {
                if (party.userId == _partyOwnerId) _isAdded = true;
              });
              if (!_isAdded) {
                _parties.add(
                  Party(
                      _partyOwnerId,
                      friend.firstName,
                      _trackName,
                      _trackArtist,
                      _trackUrl,
                      _duration,
                      _position,
                      _isPlaying,
                      _repeat,
                      _shuffle,
                      _trackId,
                      _trackImageURL),
                );
                if (!_isInit) {
                  _loading = false;
                  notifyListeners();
                }
              }
            }
            if (_isInit) {
              _isInit = false;
              _loading = false;
              notifyListeners();
            }
          }
        },
      );
    } else {
      if (_isInit) {
        _isInit = false;
        _loading = false;
        notifyListeners();
      }
    }
    if (_parties.isEmpty) {
      if (_isInit) {
        _isInit = false;
        _loading = false;
        notifyListeners();
      }
    }
  }

  Future<void> getData() async {}

  Future<void> getFriends() async {
    _friends = [];
    await fetch(url + 'users/friends?user_id=$_userId', (response) {
      var friends = json.decode(response.body)['users'];
      friends.forEach((friend) {
        _friends.add(
          User(
              friend['id'].toString(),
              friend['firstname'],
              friend['lastname'],
              friend['image'],
              true,
              friend['recently_played'],
              friend['recently_played_at']),
        );
      });
    });
  }

  void stopListening() {
    _newPartySubscription.cancel();
    _deletedPartySubscription.cancel();
    _onChangePartySubscription.forEach((party) => party.cancel());
    _isListening = false;
  }

  bool get loading => _loading;
  bool get isListening => _isListening;
  get parties => _parties;
}
