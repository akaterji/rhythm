import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../Providers/party_member.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class PausePlayIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _partyMemberProvider = Provider.of<PartyMember>(context);
    return _partyMemberProvider.isSet
        ? _partyMemberProvider.isTrackPlaying
            ? Container(
                width: 66,
                height: 66,
                child: FittedBox(
                  child: FloatingActionButton(
                    backgroundColor: Colors.white,
                    heroTag: 'pauseButton',
                    child: Icon(
                      Icons.pause,
                      color: Colors.black,
                      size: 40,
                    ),
                    // onPressed: () {
                    //   _partyProvider.pause();
                    // },
                  ),
                ),
              )
            : Container(
                width: 66,
                height: 66,
                child: FittedBox(
                  child: FloatingActionButton(
                    backgroundColor: Colors.white,
                    heroTag: 'playButton',
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.black,
                      size: 40,
                    ),
                    // onPressed: () {
                    //   _partyProvider.play();
                    // },
                  ),
                ),
              )
        : Container(
            width: 66,
            height: 66,
            child: FittedBox(
              child: FloatingActionButton(
                backgroundColor: Colors.white,
                heroTag: 'playButton',
                child: SpinKitThreeBounce(
                  color: Colors.black,
                  size: 20,
                ),
                onPressed: () {
                  // _trackProvider.play();
                },
              ),
            ),
          );
  }
}
