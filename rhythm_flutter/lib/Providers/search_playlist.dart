import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import '../Models/track.dart';
import '../constants.dart';

class SearchPlaylistProvider with ChangeNotifier {
  final String _token;
  final String _userId;
  String _playlistId;
  List<Track> _playlistTracks;
  List<Track> _result = [];
  var _modified = false;
  List<Track> _selectedTracks = [];
  SearchPlaylistProvider(this._token, this._userId);
  String url = kUrl;
  Future<void> fetch(url, callback) async {
    try {
      final response =
          await http.get(url, headers: {'authorization': 'Bearer $_token'});
      if (response.statusCode != 200) {
        throw json.decode(response.body)['message'];
      }
      callback(response);
    } catch (error) {
      kError = true;
      key.currentState.removeCurrentSnackBar();
      key.currentState.showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          content: Text('Something went wrong'),
          action: SnackBarAction(
            label: 'Retry',
            onPressed: () {
              notifyListeners();
            },
          ),
        ),
      );
    }
  }

  Future<void> post(String url, dynamic body, callback) async {
    try {
      final response = await http.post(url,
          headers: {'authorization': 'Bearer $_token'}, body: body);
      if (response.statusCode != 200) {
        throw json.decode(response.body)['message'];
      }
      callback(response);
    } catch (error) {
      kError = true;
      key.currentState.removeCurrentSnackBar();
      key.currentState.showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          content: Text('Something went wrong'),
          action: SnackBarAction(
            label: 'Retry',
            onPressed: () {
              notifyListeners();
            },
          ),
        ),
      );
    }
  }

  void setPlaylistIdAndTracks(String id, List<Track> tracks) {
    _playlistId = id;
    _playlistTracks = tracks;
  }

  void search(String text, String searchBy) {
    _selectedTracks = [];
    if (_result.length > 0) {
      _result.forEach((track) {
        if (track.selected) {
          _selectedTracks.add(track);
        }
      });
    }
    _result = [..._selectedTracks];

    fetch(url + 'tracks/search?text=$text&searchby=$searchBy', (response) {
      var _decoded = json.decode(response.body);
      List<dynamic> _resultBackend = _decoded['tracks'];
      _resultBackend.forEach((track) {
        var _playlistTrackExists = false;
        if (_playlistTracks.isNotEmpty) {
          _playlistTracks.forEach((playlistTrack) {
            if (track['id'].toString() == playlistTrack.getId) {
              _playlistTrackExists = true;
              return;
            }
          });
        }
        if (!_playlistTrackExists) {
          var _trackExists = false;
          if (_selectedTracks.length > 0) {
            _selectedTracks.forEach((existingTrack) {
              if (track['id'].toString() == existingTrack.getId) {
                {
                  _trackExists = true;
                  return;
                }
              }
            });
            if (!_trackExists) {
              _result.insert(
                0,
                Track(
                    track['id'].toString(),
                    track['name'],
                    track['artist']['name'],
                    track['album']['name'],
                    track['url'],
                    track['image'],
                    track['isFavorite'],
                    false),
              );
            }
          } else {
            _result.forEach(
              (existingTrack) {
                if (track['id'].toString() == existingTrack.getId) {
                  {
                    _trackExists = true;
                    return;
                  }
                }
              },
            );
            if (!_trackExists)
              _result.add(
                Track(
                    track['id'].toString(),
                    track['name'],
                    track['artist']['name'],
                    track['album']['name'],
                    track['url'],
                    track['image'],
                    track['isFavorite'],
                    false),
              );
          }
        }
      });

      notifyListeners();
    });
  }

  void select(trackId) {
    List<Track> _newListResult = [];
    List<Track> _newListSelected = [];
    _result.forEach((track) {
      if (track.getId == trackId) {
        track.setSelect();
        _newListResult.add(track);
        _newListSelected.add(track);
      } else {
        _newListResult.add(track);
      }
    });
    _result = _newListResult;
    _selectedTracks = [..._selectedTracks, ..._newListSelected];

    notifyListeners();
  }

  void empty() {
    _result = [..._selectedTracks];
    notifyListeners();
  }

  void reset() {
    if (_result.isNotEmpty) _result = [];
    if (_selectedTracks.isNotEmpty) _selectedTracks = [];
  }

  void addTracks() {
    List<String> _trackIds = [];
    _selectedTracks.forEach((track) {
      _trackIds.add(track.getId);
    });

    post(
      kUrl + 'playlist/addTracks',
      {
        'user_id': _userId,
        'playlist_id': _playlistId,
        'trackIds': jsonEncode(_trackIds)
      },
      (response) {
        _modified = true;
        notifyListeners();
      },
    );
  }

  List<Track> get tracks => _result;
  List<Track> get selectedTracks => _selectedTracks;
}
