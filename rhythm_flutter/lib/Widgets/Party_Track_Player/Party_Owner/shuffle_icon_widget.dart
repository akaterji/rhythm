import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../Providers/party.dart';

class ShuffleIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _partyProvider = Provider.of<Party>(context);
    return Container(
      width: 40,
      height: 40,
      child: RawMaterialButton(
        shape: CircleBorder(),
        elevation: 0.0,
        child: _partyProvider.isShuffle
            ? Icon(
                Icons.shuffle,
                color: Colors.red,
              )
            : Icon(Icons.shuffle),
        onPressed: () {
          _partyProvider.toggleShuffle();
        },
      ),
    );
  }
}
