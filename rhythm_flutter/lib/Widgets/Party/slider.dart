import 'package:flutter/material.dart';

class PartyListSlider extends StatelessWidget {
  final _position;
  final _timeLeft;
  final _isListening;
  PartyListSlider(this._position, this._timeLeft, this._isListening);
  @override
  Widget build(BuildContext context) {
    return Slider(
      activeColor: _isListening ? Colors.redAccent : Colors.purple,
      inactiveColor: _isListening ? Colors.white : Colors.blue[200],
      value: _position,
      min: 0,
      max: _timeLeft,
      onChanged: (double value) {},
    );
  }
}
