import 'package:flutter/material.dart';
import '../Widgets/Auth/home_widget.dart';
import '../Widgets/Auth/login_widget.dart';
import '../Widgets/Auth/signup_widget.dart';

class LoginScreen3 extends StatefulWidget {
  @override
  _LoginScreen3State createState() => _LoginScreen3State();
}

class _LoginScreen3State extends State<LoginScreen3>
    with TickerProviderStateMixin {
  gotoLogin() {
    _controller.animateToPage(
      0,
      duration: Duration(milliseconds: 800),
      curve: Curves.bounceOut,
    );
  }

  gotoSignup() {
    _controller.animateToPage(
      2,
      duration: Duration(milliseconds: 800),
      curve: Curves.bounceOut,
    );
  }

  PageController _controller =
      PageController(initialPage: 1, viewportFraction: 1.0);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: PageView(
        controller: _controller,
        physics: AlwaysScrollableScrollPhysics(),
        children: <Widget>[
          LoginPage(),
          HomePage(gotoLogin, gotoSignup),
          SignUp(gotoLogin)
        ],
        scrollDirection: Axis.horizontal,
      ),
    );
  }
}
