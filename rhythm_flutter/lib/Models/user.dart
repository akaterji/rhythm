class User {
  final String _id;
  final String _firstname;
  final String _lastname;
  final String _recentlyPlayedTrackId;
  final String _recentlyPlayedAt;
  String _image;
  bool _isFriend;
  bool _isFriendRequested;
  bool _isFriendRequesting;
  User(this._id, this._firstname, this._lastname, this._image, this._isFriend,
      this._recentlyPlayedTrackId, this._recentlyPlayedAt,
      [this._isFriendRequested, this._isFriendRequesting]);

  String get id => _id;
  String name() {
    return _firstname + " " + _lastname;
  }

  String get firstName => _firstname;
  String get recentlyPlayedTrackId => _recentlyPlayedTrackId;

  bool get isFriend => _isFriend;
  bool get isFriendRequested => _isFriendRequested;
  bool get isFriendRequesting => _isFriendRequesting;
  String get image => _image;
  void friendRequest() {
    _isFriendRequested = true;
  }

  void removeFriendRequest() {
    _isFriendRequested = false;
  }

  void removeFriend() {
    _isFriend = false;
    _isFriendRequested = false;
    _isFriendRequesting = false;
  }

  void accepted() {
    _isFriend = true;
    _isFriendRequested = false;
    _isFriendRequesting = false;
  }
}
