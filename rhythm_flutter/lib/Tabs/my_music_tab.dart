import 'package:flutter/material.dart';
import 'package:rhythm_flutter/Pages/favorites_recent.dart';
import 'package:rhythm_flutter/Pages/track_list_page.dart';
import 'package:rhythm_flutter/Providers/playlists.dart';
import 'package:rhythm_flutter/Providers/tracks.dart';
import 'package:rhythm_flutter/Widgets/Mini_Player/mini_player.dart';
import 'package:rhythm_flutter/constants.dart';
import '../Widgets/Playlist/playlist_widget.dart';
import 'package:provider/provider.dart';

class MyMusicTab extends StatefulWidget {
  static const route = '/music_tab';

  @override
  _MyMusicTabState createState() => _MyMusicTabState();
}

class _MyMusicTabState extends State<MyMusicTab> {
  Tracks _trackProvider;
  Playlists _playlistsProvider;
  var _isInit = true;
  @override
  void didChangeDependencies() {
    if (_isInit) {
      _trackProvider = Provider.of<Tracks>(context);
      _playlistsProvider = Provider.of<Playlists>(context);

      _isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    Tracks _tracksProvider = Provider.of<Tracks>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('My Music'),
        actions: <Widget>[],
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              PlaylistWidget(),
            ],
          ),
          MiniPlayer()
        ],
      ),
    );
  }
}
