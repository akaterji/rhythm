import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../Providers/profile.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class UserSearchList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var _profileProvider = Provider.of<Profile>(context);
    var _userList = _profileProvider.result;
    return Container(
      child: Column(
        children: _userList.map(
          (user) {
            return Row(
              children: <Widget>[
                Text(
                  user.name(),
                ),
                Spacer(),
                user.isFriend
                    ? IconButton(
                        icon: Icon(MdiIcons.accountRemove),
                        onPressed: () {
                          _profileProvider.removeFriend(user.id);
                        },
                      )
                    : user.isFriendRequested
                        ? IconButton(
                            icon: Icon(MdiIcons.accountArrowRight),
                            onPressed: () {
                              _profileProvider.removeFriendRequest(user.id);
                            },
                          )
                        : user.isFriendRequesting
                            ? IconButton(
                                icon: Icon(MdiIcons.accountMultipleCheck),
                                onPressed: () {
                                  _profileProvider.acceptFriendRequest(user.id);
                                },
                              )
                            : IconButton(
                                icon: Icon(MdiIcons.accountPlus),
                                onPressed: () {
                                  _profileProvider.sendFriendRequest(user.id);
                                },
                              ),
              ],
            );
          },
        ).toList(),
      ),
    );
  }
}
