import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import '../Models/track.dart';
import '../Models/user.dart';
import '../constants.dart';

class Playlist with ChangeNotifier {
  String _id;
  String _name;
  String _userId;
  String _token;
  bool _isShared;
  String _sharedFriendName;
  List<User> _notSharedWithList = [];
  List<User> _sharedWithList = [];
  List<Track> _tracks = [];
  final String url = kUrl;
  Playlist({id, name, tracks, userId, token, sharedFriendName})
      : _id = id,
        _name = name,
        _tracks = tracks,
        _userId = userId,
        _token = token,
        _sharedFriendName = sharedFriendName;
  setPlaylist(String id, String name) {
    _id = id;
    _name = name;
    _tracks = [];
  }

  Future<void> getTracks() async {
    final response = await http.get(url + 'playlist/?playlist_id=$_id',
        headers: {'authorization': 'Bearer $_token'});

    var body = json.decode(response.body) as List<dynamic>;
    body.forEach((track) {
      _tracks.add(
        Track(
          track['id'].toString(),
          track['name'],
          track['artist']['name'],
          track['album']['name'],
          track['url'],
          track['image'],
          track['isFavorite'],
        ),
      );
    });
    notifyListeners();
  }

  void add(Track track) {
    _tracks.add(track);
  }

  void remove(String id) {
    _tracks = _tracks.where((track) => track.getId != id).toList();
    notifyListeners();
  }

  Future<void> fetch(url, callback) async {
    try {
      final response =
          await http.get(url, headers: {'authorization': 'Bearer $_token'});
      if (response.statusCode != 200)
        throw json.decode(response.body)['message'];
      callback(response);
    } catch (error) {
      kError = true;
      key.currentState.removeCurrentSnackBar();
      key.currentState.showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          content: Text('Something went wrong'),
          action: SnackBarAction(
            label: 'Retry',
            onPressed: () {
              notifyListeners();
            },
          ),
        ),
      );
    }
  }

  Future<void> post(url, body, callback) async {
    try {
      final response = await http.post(url,
          headers: {'authorization': 'Bearer $_token'}, body: body);
      if (response.statusCode != 200)
        throw json.decode(response.body)['message'];

      callback(response);
    } catch (error) {
      kError = true;
      key.currentState.removeCurrentSnackBar();
      key.currentState.showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          content: Text('Something went wrong'),
          action: SnackBarAction(
            label: 'Retry',
            onPressed: () {
              notifyListeners();
            },
          ),
        ),
      );
    }
  }

  void getFriendsPlaylistNotShared(String playlistId) {
    fetch(
      url + 'playlist/notSharedWith?playlist_id=$playlistId',
      (response) {
        _notSharedWithList = [];
        var friends = json.decode(response.body);
        friends.forEach(
          (friend) {
            _notSharedWithList.add(
              User(
                  friend['id'].toString(),
                  friend['firstname'],
                  friend['lastname'],
                  friend['image'],
                  true,
                  friend['recently_played'],
                  friend['recently_played_at']),
            );
          },
        );
      },
    );
  }

  void unSharePlaylist(String playlistId) {
    post(url + 'playlist/sharedPlaylists/remove',
        {'playlist_id': playlistId, 'user_id': _userId}, (response) {});
  }

  void sharedWith(String playlistId) {
    fetch(url + 'playlist/sharedWith?playlist_id=$playlistId', (response) {
      var friends = json.decode(response.body);
      if (friends.isEmpty) {
        _isShared = false;
      } else {
        _isShared = true;
        _sharedWithList = [];
        friends.forEach(
          (friend) {
            _sharedWithList.add(User(
                friend['id'].toString(),
                friend['firstname'],
                friend['lastname'],
                friend['image'],
                true,
                friend['recently_played'],
                friend['recently_played_at']));
          },
        );
      }
    });
  }

  void shareWith(String playlistId, String userId, List<User> friends) {
    post(url + 'playlist/sharedPlaylists/add',
        {'user_id': userId, 'playlist_id': playlistId}, (response) {
      User _friend = friends.firstWhere((friend) => friend.id == userId);
      _sharedWithList.add(_friend);
      notifyListeners();
    });
  }

  void removeShareWith(String playlistId, String userId) {
    post(url + 'playlist/sharedPlaylists/remove',
        {'user_id': userId, 'playlist_id': playlistId}, (response) {
      _sharedWithList =
          _sharedWithList.where((friend) => friend.id != userId).toList();
      notifyListeners();
    });
  }

  void setTracks(List<Track> tracks) {
    if (_tracks == null)
      _tracks = [...tracks];
    else
      _tracks = [..._tracks, ...tracks];
    notifyListeners();
  }

  String get id => _id;
  void setAllTracks(List<Track> tracks) {
    _tracks = [...tracks];
    notifyListeners();
  }

  String get name => _name;
  bool get isShared => _isShared;
  List<Track> get tracks => _tracks;
  List<User> get sharedWithList => _sharedWithList;
  List<User> get notSharedWithList => _notSharedWithList;
  String get sharedFriendName => _sharedFriendName;
}
