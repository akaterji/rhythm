import 'package:flutter/material.dart';
import 'package:rhythm_flutter/Providers/party.dart';
import 'package:rhythm_flutter/Providers/party_member.dart';

import './slider.dart';
import './track_progress_at.dart';
import './track_progress_left.dart';
import 'package:rhythm_flutter/Pages/music_playback_page.dart';
import '../../Pages/party_member_music_playback_page.dart';
import 'package:provider/provider.dart';

class PartyDetails extends StatefulWidget {
  final _partyName;
  final _imageURL;
  final _songName;
  final _songArtist;
  final _ownerId;
  final _trackId;
  final _position;
  final _duration;
  final bool _isPlaying;
  bool _isListening = false;

  PartyDetails(
      this._partyName,
      this._ownerId,
      this._trackId,
      this._imageURL,
      this._songName,
      this._songArtist,
      this._position,
      this._duration,
      this._isPlaying,
      [this._isListening]);

  @override
  _PartyDetailsState createState() => _PartyDetailsState();
}

class _PartyDetailsState extends State<PartyDetails> {
  var _loading;
  Party _partyProvider;
  PartyMember _partyMemberProvider;
  var _isInit = true;
  @override
  void didChangeDependencies() {
    if (_isInit) {
      _partyProvider = Provider.of<Party>(context);
      _partyMemberProvider = Provider.of<PartyMember>(context);
      _isInit = true;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    if (getDuration(widget._duration) == Duration(seconds: 0)) {
      _loading = true;
    } else {
      _loading = false;
    }
    var deviceSize = MediaQuery.of(context).size;
    Duration _timeLeft =
        getDuration(widget._duration) - getDuration(widget._position);
    return Column(
      children: <Widget>[
        SizedBox(
          height: 20,
        ),
        widget._isListening
            ? Container(
                width: deviceSize.width * 0.6,
                height: deviceSize.height * 0.62,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pushNamed(
                      PartyMemberMusicPlaybackPage.route,
                      arguments: {
                        'ownerId': widget._ownerId,
                        'trackId': widget._trackId,
                        'image': widget._imageURL,
                      },
                    );
                  },
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(12.0),
                      ),
                    ),
                    // color: Color.fromRGBO(148, 0, 211, 1),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Text(
                            widget._partyName + '\'s party',
                            style: TextStyle(color: Colors.white, fontSize: 20),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Stack(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.center,
                              child: Container(
                                margin: EdgeInsets.only(top: 20),
                                width: deviceSize.width * 0.45,
                                height: deviceSize.height * 0.29,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(8.0),
                                  child: FadeInImage.assetNetwork(
                                      placeholder: 'assets/colorful_loader.gif',
                                      image: widget._imageURL,
                                      fit: BoxFit.fill),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: Container(
                                margin: EdgeInsets.only(top: 95),
                                width: deviceSize.width * 0.08,
                                height: deviceSize.height * 0.04,
                                child: Image.asset(
                                  'assets/party.gif',
                                  fit: BoxFit.fill,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                            height: 10,
                            margin: EdgeInsets.only(top: 10),
                            width: deviceSize.width * 0.5,
                            child: Center(
                              child: Text(
                                widget._songName + ' - ' + widget._songArtist,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 15),
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            child: Column(
                              children: <Widget>[
                                widget._isPlaying
                                    ? Container(
                                        child: Icon(
                                          Icons.pause,
                                          color: Colors.white,
                                        ),
                                      )
                                    : Container(
                                        child: Icon(
                                          Icons.play_arrow,
                                          color: Colors.white,
                                        ),
                                      ),
                                // SizedBox(
                                //   height: 10,
                                // ),
                                _loading
                                    ? Column(
                                        children: <Widget>[
                                          SizedBox(
                                            height: deviceSize.height * 0.025,
                                          ),
                                          Container(
                                            width: 30,
                                            height: 30,
                                            child: CircularProgressIndicator(
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(
                                                      Colors.white),
                                              strokeWidth: 1.5,
                                            ),
                                          ),
                                        ],
                                      )
                                    : Column(
                                        children: <Widget>[
                                          Container(
                                            height: 20,
                                            width: deviceSize.width * 0.7,
                                            child: PartyListSlider(
                                                getDuration(widget._position)
                                                    .inSeconds
                                                    .toDouble(),
                                                getDuration(widget._duration)
                                                    .inSeconds
                                                    .toDouble(),
                                                widget._isListening),
                                          ),
                                          Container(
                                            height: 20,
                                            width: deviceSize.width * 0.7,
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 25),
                                                  child:
                                                      PartyListTrackProgressAt(
                                                          widget._position,
                                                          true),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      right: 25),
                                                  child:
                                                      PartyListTrackProgressLeft(
                                                          _timeLeft
                                                              .toString()
                                                              .substring(2, 7),
                                                          true),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            : Container(
                width: deviceSize.width * 0.5,
                height: deviceSize.height * 0.5,
                child: InkWell(
                  onTap: () {
                    if (widget._isListening) {
                      Navigator.of(context).pushNamed(
                        PartyMemberMusicPlaybackPage.route,
                        arguments: {
                          'ownerId': widget._ownerId,
                          'trackId': widget._trackId,
                          'image': widget._imageURL,
                        },
                      );
                    } else {
                      if (_partyProvider.didTrackStart) {
                        showDialog(
                          context: context,
                          barrierDismissible: true,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text("Joining a new party"),
                              content: Text(
                                  "Your party will be disbanded. Are you sure you want to join this party?"),
                              actions: <Widget>[
                                // usually buttons at the bottom of the dialog
                                FlatButton(
                                  child: Text("Cancel"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                FlatButton(
                                  child: Text("Yes"),
                                  onPressed: () {
                                    _partyProvider.exit();
                                    Navigator.of(context).pushReplacementNamed(
                                      PartyMemberMusicPlaybackPage.route,
                                      arguments: {
                                        'ownerId': widget._ownerId,
                                        'trackId': widget._trackId,
                                        'image': widget._imageURL,
                                      },
                                    );
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      } else if (_partyMemberProvider.enteredParty) {
                        showDialog(
                          context: context,
                          barrierDismissible: true,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text("Joining a new party"),
                              content: Text(
                                  "You are already in a party. Are you sure you want to join a new party?"),
                              actions: <Widget>[
                                // usually buttons at the bottom of the dialog
                                FlatButton(
                                  child: Text("Cancel"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                FlatButton(
                                  child: Text("Yes"),
                                  onPressed: () {
                                    _partyMemberProvider.exit();
                                    Navigator.of(context).pushReplacementNamed(
                                      PartyMemberMusicPlaybackPage.route,
                                      arguments: {
                                        'ownerId': widget._ownerId,
                                        'trackId': widget._trackId,
                                        'image': widget._imageURL,
                                      },
                                    );
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      } else {
                        Navigator.of(context).pushNamed(
                          PartyMemberMusicPlaybackPage.route,
                          arguments: {
                            'ownerId': widget._ownerId,
                            'trackId': widget._trackId,
                            'image': widget._imageURL,
                          },
                        );
                      }
                    }
                  },
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(12.0),
                      ),
                    ),
                    color: Color.fromRGBO(225, 230, 230, 1),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Text(
                            widget._partyName + '\'s party',
                            style: TextStyle(color: Colors.black, fontSize: 20),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          width: deviceSize.width * 0.25,
                          height: deviceSize.height * 0.15,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: FadeInImage.assetNetwork(
                                placeholder: 'assets/colorful_loader.gif',
                                image: widget._imageURL,
                                fit: BoxFit.fill),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                            height: 10,
                            margin: EdgeInsets.only(top: 10),
                            width: deviceSize.width * 0.5,
                            child: Center(
                              child: Text(
                                widget._songName + ' - ' + widget._songArtist,
                                style: TextStyle(color: Colors.black),
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            child: Column(
                              children: <Widget>[
                                widget._isPlaying
                                    ? Container(
                                        child: Icon(
                                          Icons.pause,
                                          color: Colors.black,
                                        ),
                                      )
                                    : Container(
                                        child: Icon(
                                          Icons.play_arrow,
                                          color: Colors.black,
                                        ),
                                      ),
                                SizedBox(
                                  height: 10,
                                ),
                                _loading
                                    ? Column(
                                        children: <Widget>[
                                          SizedBox(
                                            height: deviceSize.height * 0.025,
                                          ),
                                          Container(
                                            width: 30,
                                            height: 30,
                                            child: CircularProgressIndicator(
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(
                                                      Colors.black),
                                              strokeWidth: 1.5,
                                            ),
                                          ),
                                        ],
                                      )
                                    : Column(
                                        children: <Widget>[
                                          Container(
                                            height: 20,
                                            width: deviceSize.width * 0.7,
                                            child: PartyListSlider(
                                                getDuration(widget._position)
                                                    .inSeconds
                                                    .toDouble(),
                                                getDuration(widget._duration)
                                                    .inSeconds
                                                    .toDouble(),
                                                widget._isListening),
                                          ),
                                          Container(
                                            height: 20,
                                            width: deviceSize.width * 0.7,
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 25),
                                                  child:
                                                      PartyListTrackProgressAt(
                                                          widget._position,
                                                          false),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      right: 25),
                                                  child:
                                                      PartyListTrackProgressLeft(
                                                          _timeLeft
                                                              .toString()
                                                              .substring(2, 7),
                                                          false),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
      ],
    );
  }

  Duration getDuration(String time) {
    double minutes = double.parse(time.substring(0, 2));
    double seconds = double.parse(time.substring(3, 5));
    return Duration(minutes: minutes.toInt(), seconds: seconds.toInt());
  }
}
