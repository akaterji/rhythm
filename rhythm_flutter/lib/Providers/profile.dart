import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rhythm_flutter/Models/track.dart';
import '../Models/user.dart';
import '../constants.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:firebase_database/firebase_database.dart';

class Profile with ChangeNotifier {
  var _loading;
  String _userName;
  String _userImage;
  bool _newNotification = false;
  List<User> _result = [];
  List<User> _friends = [];
  List<Map<String, String>> _notifications = [];
  final String _token;
  final String _userId;
  final url = kUrl;
  var _popped = false;
  bool _isInit = true;
  final databaseReference = FirebaseDatabase.instance.reference();
  var _onNewNotificationListener;
  var _onDeletedNotificationListener;
  var _isListening = false;
  List<Map<dynamic, dynamic>> _recentlyPlayed = [];
  Profile(this._token, this._userId, this._userName, this._userImage);
  Future<void> fetch(url, callback) async {
    try {
      final response =
          await http.get(url, headers: {'authorization': 'Bearer $_token'});
      if (response.statusCode != 200) {
        throw json.decode(response.body)['message'];
      }
      callback(response);
    } catch (error) {
      kError = true;
      key.currentState.removeCurrentSnackBar();
      key.currentState.showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          content: Text('Something went wrong'),
          action: SnackBarAction(
            label: 'Retry',
            onPressed: () {
              notifyListeners();
            },
          ),
        ),
      );
    }
  }

  Future<void> post(String url, dynamic body, callback) async {
    try {
      final response = await http.post(url,
          headers: {'authorization': 'Bearer $_token'}, body: body);
      if (response.statusCode != 200) {
        throw json.decode(response.body)['message'];
      }
      callback(response);
    } catch (error) {
      kError = true;
      key.currentState.removeCurrentSnackBar();
      key.currentState.showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          content: Text('Something went wrong'),
          action: SnackBarAction(
            label: 'Retry',
            onPressed: () {
              notifyListeners();
            },
          ),
        ),
      );
    }
  }

  void getFriends() {
    fetch(url + 'users/friends?user_id=$_userId', (response) {
      _friends = [];
      var friends = json.decode(response.body)['users'];
      friends.forEach((friend) {
        _friends.add(
          User(
            friend['id'].toString(),
            friend['firstname'],
            friend['lastname'],
            friend['image'],
            true,
            friend['recently_played'],
            friend['recently_played_at'],
          ),
        );
      });
    });
  }

  void initFirebase() async {
    if (_isInit) {
      await getData();
      _onNewNotificationListener = databaseReference
          .child('Notifications')
          .child(_userId)
          .onChildAdded
          .listen(_onNewNotification);

      _onDeletedNotificationListener = databaseReference
          .child('Notifications')
          .child(_userId)
          .onChildRemoved
          .listen(_onRemovedNotification);
      _isListening = true;
      _isInit = false;
    }
  }

  void _onRemovedNotification(Event event) {
    List<Map<String, String>> _tempArray = [];
    _notifications.forEach(
      (notification) {
        if (notification['userId'] != event.snapshot.value)
          _tempArray.add(notification);
      },
    );
    _notifications = _tempArray;
    notifyListeners();
  }

  void _onNewNotification(Event event) async {
    var _requestUserId = event.snapshot.value;
    fetch(
      url + 'users/getName?user_id=$_requestUserId',
      (response) {
        var notificationExists = false;
        _notifications.forEach(
          (notification) {
            if (notification['userId'] == _requestUserId) {
              notificationExists = true;
            }
          },
        );

        if (!notificationExists) {
          var decodedResponse = json.decode(response.body);
          _notifications.add(
            {
              'userId': _requestUserId,
              'name': decodedResponse['name'],
              'image': decodedResponse['image']
            },
          );
          _newNotification = true;
          notifyListeners();
        }
      },
    );
  }

  Future<void> getData() async {
    await databaseReference.child('Notifications').child(_userId).once().then(
      (DataSnapshot snapshot) {
        if (snapshot.value != null) {
          snapshot.value.forEach(
            (f, k) {
              var _requestUserId = k;
              fetch(
                url + 'users/getName?user_id=$_requestUserId',
                (response) {
                  _notifications = [];
                  var _decodedResponse = json.decode(response.body);
                  _notifications.add(
                    {
                      'userId': _requestUserId,
                      'name': _decodedResponse['name'],
                      'image': _decodedResponse['image'],
                    },
                  );
                  notifyListeners();
                },
              );
            },
          );
        } else {
          _newNotification = false;
          notifyListeners();
        }
      },
    );
  }

  void sendFriendRequest(String friendId) async {
    databaseReference
        .child('Notifications')
        .child(friendId)
        .push()
        .set(_userId);
    post(
      url + 'users/sendFriendRequest',
      {'friend_id': friendId},
      (response) {},
    );
    _result.forEach((user) {
      if (user.id == friendId) {
        user.friendRequest();
        notifyListeners();
      }
    });
  }

  void removeFriendRequest(String friendId) async {
    post(url + 'users/removeFriendRequest', {'friend_id': friendId},
        (response) async {
      _result.forEach((user) {
        if (user.id == friendId) {
          {
            user.removeFriendRequest();
            List<Map<String, String>> _tempArray = [];
            _notifications.forEach((notification) {
              if (notification['userId'] != friendId)
                _tempArray.add(notification);
            });
            _notifications = _tempArray;
          }
          notifyListeners();
        }
      });
      String key;
      await databaseReference
          .child('Notifications')
          .child(friendId)
          .orderByValue()
          .equalTo(_userId)
          .once()
          .then(
        (DataSnapshot snapshot) {
          snapshot.value.forEach(
            (f, k) {
              key = f;
            },
          );
        },
      );
      await databaseReference
          .child('Notifications')
          .child(friendId)
          .child(key)
          .remove();
    });
  }

  void removeFriendRequestAfterAccepting(String friendId) async {
    post(url + 'users/removeFriendRequestAfterAccepting',
        {'friend_id': friendId}, (response) async {
      _result.forEach((user) {
        if (user.id == friendId) {
          {
            user.accepted();

            List<Map<String, String>> _tempArray = [];
            _notifications.forEach((notification) {
              if (notification['userId'] != friendId)
                _tempArray.add(notification);
            });
            _notifications = _tempArray;
          }
          notifyListeners();
        }
      });
      String key;
      await databaseReference
          .child('Notifications')
          .child(_userId)
          .orderByValue()
          .equalTo(friendId)
          .once()
          .then(
        (DataSnapshot snapshot) {
          snapshot.value.forEach(
            (f, k) {
              key = f;
            },
          );
        },
      );
      await databaseReference
          .child('Notifications')
          .child(_userId)
          .child(key)
          .remove();
    });
  }

  void removeFriendRequestFromNotifications(String friendId) async {
    post(url + 'users/removeFriendRequestAfterAccepting',
        {'friend_id': friendId}, (response) async {
      _result.forEach((user) {
        if (user.id == friendId) {
          {
            user.accepted();

            var _tempArray = [];
            _notifications.forEach((notification) {
              if (notification['userId'] != friendId)
                _tempArray.add(notification);
            });
            _notifications = _tempArray;
          }
          notifyListeners();
        }
      });
      String key;
      await databaseReference
          .child('Notifications')
          .child(_userId)
          .orderByValue()
          .equalTo(friendId)
          .once()
          .then(
        (DataSnapshot snapshot) {
          snapshot.value.forEach(
            (f, k) {
              key = f;
            },
          );
        },
      );
      await databaseReference
          .child('Notifications')
          .child(_userId)
          .child(key)
          .remove();
    });
  }

  void removeFriend(String friendId) {
    post(url + 'users/remove', {'user_id': _userId, 'friend_id': friendId},
        (response) {
      _result.forEach((user) {
        if (user.id == friendId) {
          user.removeFriend();
          notifyListeners();
        }
      });
    });
  }

  void search(String text) {
    if (!_popped) if (text.length != 0)
      fetch(
        url + 'users/search?text=$text',
        (response) {
          _result = [];
          List<dynamic> users = json.decode(response.body)['users'];
          if (users.isNotEmpty) {
            users.forEach(
              (user) {
                if (user['id'].toString() != _userId)
                  _result.add(
                    User(
                      user['id'].toString(),
                      user['firstname'],
                      user['lastname'],
                      user['image'],
                      user['isFriend'],
                      user['recently_played'],
                      user['recently_played_at'],
                      user['isFriendRequested'],
                      user['isFriendRequesting'],
                    ),
                  );
              },
            );
            notifyListeners();
          }
        },
      );
  }

  void getRecentlyPlayed() {
    fetch(
      url + 'users/recentlyPlayed',
      (response) {
        _recentlyPlayed = [];
        var result = json.decode(response.body);
        result.forEach(
          (recentlyPlayed) {
            _recentlyPlayed.add(
              {
                'friend': User(
                    recentlyPlayed['friend']['id'].toString(),
                    recentlyPlayed['friend']['firstname'],
                    recentlyPlayed['friend']['lastname'],
                    recentlyPlayed['friend']['image'],
                    true,
                    recentlyPlayed['friend']['recently_played'],
                    recentlyPlayed['friend']['recently_played_at']),
                'track': Track(
                  recentlyPlayed['track']['id'].toString(),
                  recentlyPlayed['track']['name'],
                  recentlyPlayed['track']['artist']['name'],
                  recentlyPlayed['track']['album']['name'],
                  recentlyPlayed['track']['url'],
                  recentlyPlayed['track']['image'],
                  recentlyPlayed['track']['isFavorite'],
                ),
                'playedAt': recentlyPlayedAt(
                    DateTime.parse(recentlyPlayed['played_at']))
              },
            );
          },
        );
        notifyListeners();
      },
    );
  }

  void acceptFriendRequest(friendId) {
    post(url + 'users/add', {'user_id': _userId, 'friend_id': friendId},
        (response) {
      removeFriendRequestAfterAccepting(friendId);
    });
  }

  void clear() {
    _result = [];
    notifyListeners();
  }

  List<User> get friends => _friends;
  List<User> get result => _result;
  List<Map<String, String>> get notifications => _notifications;
  bool get newNotification => _newNotification;
  bool get loading => _loading;
  String get username => _userName;
  void notificationsViewed() {
    _newNotification = false;
    kNotificationsViewed = true;
    notifyListeners();
  }

  void stopListening() {
    _onNewNotificationListener.cancel();
    _onDeletedNotificationListener.cancel();
    _isListening = false;
  }

  List<Map<dynamic, dynamic>> get recentlyPlayed => _recentlyPlayed;
  bool get isListening => _isListening;
  String get userImage => _userImage;
  String recentlyPlayedAt(DateTime date) {
    if (DateTime.now().difference(date) < Duration(days: 1))
      return 'today';
    else if (DateTime.now().difference(date) > Duration(days: 1) &&
        DateTime.now().difference(date) < Duration(days: 2))
      return 'yesterday';
    else if (DateTime.now().difference(date) > Duration(days: 2) &&
        DateTime.now().difference(date) < Duration(days: 3))
      return 'two days ago';
    else if (DateTime.now().difference(date) > Duration(days: 3) &&
        DateTime.now().difference(date) < Duration(days: 4))
      return 'three days ago';
    else if (DateTime.now().difference(date) > Duration(days: 4) &&
        DateTime.now().difference(date) < Duration(days: 5))
      return 'four days ago';
    else if (DateTime.now().difference(date) > Duration(days: 5) &&
        DateTime.now().difference(date) < Duration(days: 6))
      return 'five days ago';
    else if (DateTime.now().difference(date) > Duration(days: 6) &&
        DateTime.now().difference(date) < Duration(days: 7))
      return 'six days ago';
    else if (DateTime.now().difference(date) > Duration(days: 7) &&
        DateTime.now().difference(date) < Duration(days: 8))
      return 'a week ago';
    else
      return 'more than a week ago';
  }

  void popped() {
    _popped = true;
  }

  void unPopped() {
    _popped = false;
  }

  List<Track> recentlyPlayedTracks() {
    List<Track> _recentlyPlayedTracks = [];
    _recentlyPlayed.forEach((recentlyPlayed) {
      _recentlyPlayedTracks.add(recentlyPlayed['track']);
    });
    return _recentlyPlayedTracks;
  }
}
