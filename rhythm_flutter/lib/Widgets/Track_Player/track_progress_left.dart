import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import '../../Providers/track_player.dart';

class TrackProgressLeft extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _trackProvider = Provider.of<TrackPlayer>(context);
    return Container(
      alignment: Alignment.centerRight,
      child: Text(
        _trackProvider.isSet
            ? _trackProvider.getTimeLeft().toString().substring(2, 7)
            : '00:00',
        style: TextStyle(fontSize: 12),
      ),
      margin: EdgeInsets.only(right: 10),
    );
  }
}
