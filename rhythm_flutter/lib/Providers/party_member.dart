import 'package:flutter/widgets.dart';
import 'dart:async';
import 'package:firebase_database/firebase_database.dart';

import 'package:audioplayers/audioplayers.dart';

class PartyMember with ChangeNotifier {
  AudioPlayer _trackPlayer = AudioPlayer(playerId: 'partyMember');
  Duration _position = Duration(seconds: 0);
  Duration _duration = Duration(seconds: 0);
  Duration _timeLeft = Duration(seconds: 0);

  String _url;
  String _trackId;
  String _ownerId;
  Timer _timer;

  final _timerInterval = const Duration(seconds: 8);
  var _partyDisbanded = false;
  var _name = '';
  var _isLoading = true;
  var _volume = 0.5;
  var _isSet = false;
  var _isRepeat = false;
  var _isShuffle = false;
  var _startedPlaying = false;
  var _enteredParty = false;
  var _isListening = false;
  var _image = '';
  var _album = '';
  var _artist = '';
  var firebaseOnChangeSubscription;
  var firebaseOnDeleteSubscription;
  var audioPlayerOnPositionSubscription;
  var _isPlaying = false;
  final databaseReference = FirebaseDatabase.instance.reference();

  int _index;
  void init(
    String ownerId,
  ) async {
    _partyDisbanded = false;
    Future.delayed(Duration(milliseconds: 500), () {
      _enteredParty = true;
      notifyListeners();
    });
    _ownerId = ownerId;
    await databaseReference
        .child('Party')
        .child(ownerId)
        .once()
        .then((DataSnapshot snapshot) {
      var values = snapshot.value;
      _trackId = snapshot.value['track_id'];

      prepareUI(
        values['url'],
        values['track_id'],
        values['name'],
        values['position'],
        values['duration'],
        values['image'],
        values['isPlaying'],
        values['shuffle'],
        values['repeat'],
        values['album'],
        values['artist'],
      );

      _timer = interval(_timerInterval, synchronize);
    });
    firebaseOnChangeSubscription = databaseReference
        .child('Party')
        .child(ownerId)
        .onChildChanged
        .listen(_onChangedParty);
    firebaseOnDeleteSubscription = databaseReference
        .child('Party')
        .child(ownerId)
        .onChildRemoved
        .listen(_onRemovedParty);

    _isLoading = false;

    notifyListeners();
  }

  void _onChangedParty(Event event) {
    var _key = event.snapshot.key;
    var _value = event.snapshot.value;
    if (_key == 'position') {
      var _newPosition = parseDuration(_value);
      if (_newPosition - _position > Duration(seconds: 8) ||
          _position - _newPosition > Duration(seconds: 0)) {
        if (_newPosition > Duration(seconds: 2)) {
          if (_newPosition - _position > Duration(seconds: 0) ||
              _position - _newPosition > Duration(seconds: 2)) {
            _position = _newPosition - Duration(seconds: 1);
            _trackPlayer.seek(_position);
            notifyListeners();
          }
        }
      }
    } else if (_key == 'duration') {
      var _newDuration = parseDuration(_value);
      _duration = _newDuration;
      notifyListeners();
    } else if (_key == 'url') {
      _isSet = false;
      notifyListeners();
      _timer.cancel();
      _timer.cancel();
      databaseReference
          .child('Party')
          .child(_ownerId)
          .once()
          .then((DataSnapshot snapshot) {
        reset();
        _isPlaying = snapshot.value['isPlaying'];
        _artist = snapshot.value['artist'];
        _album = snapshot.value['album'];
        _image = snapshot.value['image'];

        setAudioPlayer(
          snapshot.value['url'],
          snapshot.value['track_id'],
          snapshot.value['name'],
          snapshot.value['position'],
          snapshot.value['duration'],
        );
        _timer = interval(_timerInterval, synchronize);
      });
    } else if (_key == 'isPlaying') {
      if (_value == false) {
        _trackPlayer.pause();
        _isPlaying = false;
        databaseReference
            .child('Party')
            .child(_ownerId)
            .once()
            .then((DataSnapshot snapshot) {
          var position = parseDuration(snapshot.value['position']);
          _trackPlayer.seek(position);
          _position = position;
          notifyListeners();
        });
        notifyListeners();
      } else {
        _trackPlayer.resume();
        _isPlaying = true;
        notifyListeners();
      }
    } else if (_key == 'shuffle') {
      if (_value == true) {
        _isShuffle = true;
        notifyListeners();
      } else {
        _isShuffle = false;
        notifyListeners();
      }
    } else if (_key == 'repeat') {
      if (_value == true) {
        _isRepeat = true;
        notifyListeners();
      } else {
        _isRepeat = false;
        notifyListeners();
      }
    }
  }

  void _onRemovedParty(Event event) async {
    await exit();
    _partyDisbanded = true;
    notifyListeners();
  }

  Future<void> setAudioPlayer(
    String trackUrl,
    String trackId,
    String trackName,
    String position,
    String duration,
  ) async {
    _name = trackName;
    _url = trackUrl;
    final result = await _trackPlayer.setUrl(trackUrl);
    if (result == 1) {
      _position = parseDuration(position);
      _duration = parseDuration(duration);
      _trackPlayer.seek(_position);
      if (_isPlaying) {
        _trackPlayer.resume();
        _startedPlaying = true;
      }
      audioPlayerOnPositionSubscription =
          _trackPlayer.onAudioPositionChanged.listen((Duration p) {
        _position = p;

        notifyListeners();
      });
      _isSet = true;
      notifyListeners();
    } else {
      print('error');
    }
  }

  Duration parseDuration(String s) {
    int hours = 0;
    int minutes = 0;
    int micros;
    List<String> parts = s.split(':');
    if (parts.length > 2) {
      hours = int.parse(parts[parts.length - 3]);
    }
    if (parts.length > 1) {
      minutes = int.parse(parts[parts.length - 2]);
    }
    micros = (double.parse(parts[parts.length - 1]) * 1000000).round();
    return Duration(hours: hours, minutes: minutes, microseconds: micros);
  }

  reset() {
    _duration = Duration(seconds: 0);
    _timeLeft = Duration(seconds: 0);
    _url = '';
    _isSet = false;
    _startedPlaying = false;
    _name = '';
  }

  play() async {
    _trackPlayer.resume();
    _startedPlaying = true;
    _isPlaying = true;
  }

  pause() async {
    await _trackPlayer.pause();
    _isPlaying = false;
    notifyListeners();
  }

  changeVolume(double volume) {
    _volume = volume;
    _trackPlayer.setVolume(_volume);
    notifyListeners();
  }

  void seekToSecond(int second) {
    Duration newDuration = Duration(seconds: second);
    _trackPlayer.seek(newDuration);
  }

  synchronize() {
    databaseReference
        .child('Party')
        .child(_ownerId)
        .once()
        .then((DataSnapshot snapshot) {
      Duration _newPosition = parseDuration(snapshot.value['position']);
      Duration _newDuration = parseDuration(snapshot.value['duration']);
      String _newUrl = snapshot.value['url'];
      bool _newIsPlaying = snapshot.value['isPlaying'];
      bool _newIsShuffle = snapshot.value['shuffle'];
      bool _newIsRepeat = snapshot.value['repeat'];
      String _newTrackId = snapshot.value['track_id'];
      String _newTrackName = snapshot.value['name'];
      if (_newPosition - _position > Duration(seconds: 5) ||
          _newPosition - _position < Duration(seconds: 0)) {
        _position = _newPosition;
        _trackPlayer.seek(_position);
      }
      if (_duration != _newDuration) {
        _duration = _newDuration;
        notifyListeners();
      }
      if (_newUrl != _url) {
        databaseReference
            .child('Party')
            .child(_ownerId)
            .once()
            .then((DataSnapshot snapshot) {
          reset();
          setAudioPlayer(
              snapshot.value['url'],
              snapshot.value['track_id'],
              snapshot.value['name'],
              snapshot.value['position'],
              snapshot.value['duration']);
        });
      }
      if (_newIsPlaying != _isPlaying) {
        _isPlaying ? _trackPlayer.resume() : _trackPlayer.pause();
        _isPlaying = !_isPlaying;
        notifyListeners();
      }
      if (_newIsShuffle != _isShuffle) {
        _isShuffle = !_isShuffle;
        notifyListeners();
      }
      if (_newIsRepeat != _isRepeat) {
        _isRepeat = !_isRepeat;
        notifyListeners();
      }
      if (_newTrackId != _trackId) {
        _trackId = _newTrackId;
        notifyListeners();
      }
      if (_newTrackName != _name) {
        _name = _newTrackName;
        notifyListeners();
      }
    });
  }

  Future<void> exit() async {
    firebaseOnDeleteSubscription.cancel();
    firebaseOnChangeSubscription.cancel();
    audioPlayerOnPositionSubscription.cancel();
    _enteredParty = false;
    _isLoading = true;
    reset();
    await _trackPlayer.release();
    _timer.cancel();
    if (_timer.isActive) {
      _timer.cancel();
    }
  }

  void prepareUI(
      String trackURL,
      String trackId,
      String trackName,
      String trackPosition,
      String trackDuration,
      String trackImage,
      bool isTrackPlaying,
      bool isShuffle,
      bool isRepeat,
      String album,
      String artist) {
    _isPlaying = isTrackPlaying;
    _isRepeat = isRepeat;
    _isShuffle = isShuffle;
    _name = trackName;
    _artist = artist;
    _album = album;
    _image = trackImage;
    notifyListeners();
    setAudioPlayer(trackURL, trackId, trackName, trackPosition, trackDuration);
  }

  Timer interval(Duration duration, func) {
    Timer function() {
      Timer timer = new Timer(duration, function);

      func();

      return timer;
    }

    return new Timer(duration, function);
  }

  String get artist => _artist;
  String get album => _album;
  bool get isSet => _isSet;

  bool get didTrackStart => _startedPlaying;

  bool get isTrackPlaying => _isPlaying;

  bool get isRepeat => _isRepeat;

  bool get isShuffle => _isShuffle;
  bool get partyDisbanded => _partyDisbanded;
  Duration get getPosition => _position;

  Duration get getDuration => _duration;

  double get getVolume => _volume;

  Duration getTimeLeft() {
    _timeLeft = _duration - _position;
    return _timeLeft;
  }

  void setIndex(int index) {
    _index = index;
  }

  int get index => _index;
  bool get isLoading => _isLoading;
  bool get enteredParty => _enteredParty;
  String get getURL => _url;
  String get getName => _name;
  String get trackId => _trackId;
  String get ownerId => _ownerId;
  void setPosition(Duration p) {
    _position = p;
    notifyListeners();
  }

  String get image => _image;
}
