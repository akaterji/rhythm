import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../Providers/party_member.dart';

class TrackProgressSlider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _partyMemberProvider = Provider.of<PartyMember>(context);
    return Slider(
      activeColor: Colors.white,
      value: _partyMemberProvider.getPosition.inSeconds.toDouble() == -1
          ? 0.0
          : _partyMemberProvider.getPosition.inSeconds.toDouble(),
      min: 0,
      max: _partyMemberProvider.isSet
          ? _partyMemberProvider.getDuration.inSeconds.toDouble() == 0
              ? 999999999999999
              : _partyMemberProvider.getDuration.inSeconds.toDouble()
          : 999999999999999,
      onChanged: (value) {},
    );
  }
}
