import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rhythm_flutter/Models/track.dart';
import 'package:rhythm_flutter/Pages/favorites_recent.dart';
import 'package:rhythm_flutter/Pages/music_playback_page.dart';
import 'package:rhythm_flutter/Providers/recently_played.dart';
import 'package:rhythm_flutter/Widgets/Mini_Player/mini_player.dart';
import 'package:rhythm_flutter/Widgets/Track_List/PopupTracks.dart';
import '../Providers/auth.dart';
import '../Providers/track_player.dart';
import '../Providers/party.dart';
import '../Providers/party_member.dart';
import '../Providers/party_lobby.dart';
import '../Providers/profile.dart';
import '../Providers/albums.dart';
import '../Models/album.dart';
import '../Pages/track_list_page.dart';
import '../constants.dart';

class HomeTab extends StatefulWidget {
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  var _isInit = true;
  Albums _albumsProvider;
  List<Album> _albums;
  List<Track> _recentlyPlayedTracks = [];
  RecentlyPlayed _recentlyPlayedProvider;
  Auth _authProvider;
  String _imageUrl;
  @override
  void didChangeDependencies() {
    if (_isInit) {
      _albumsProvider = Provider.of<Albums>(context);
      _albums = _albumsProvider.albums;
      _authProvider = Provider.of<Auth>(context);
      _recentlyPlayedProvider = Provider.of<RecentlyPlayed>(context);

      _imageUrl = _authProvider.userImage;

      _isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    _recentlyPlayedTracks = _recentlyPlayedProvider.tracks;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Home',
        ),
        actions: <Widget>[
          profilePopupButton(),
        ],
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.77,
                width: MediaQuery.of(context).size.width * 0.95,
                margin: EdgeInsets.all(10),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25.0),
                  ),
                  child: _albumsProvider.albums.length == 0
                      ? SizedBox.shrink()
                      : Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                margin: EdgeInsets.only(left: 18, top: 10),
                                child: Text(
                                  'Popular albums',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 15),
                              width: MediaQuery.of(context).size.width * 0.95,
                              child: Column(
                                children: List.generate(
                                  2,
                                  (index) {
                                    return Container(
                                      margin: index == 0
                                          ? EdgeInsets.only(top: 15, bottom: 15)
                                          : EdgeInsets.only(
                                              top: 15, bottom: 15),
                                      height: 180,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          Column(
                                            children: <Widget>[
                                              InkWell(
                                                child: Container(
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8.0),
                                                    child: FadeInImage.assetNetwork(
                                                        placeholder:
                                                            'assets/colorful_loader.gif',
                                                        image:
                                                            _albums[index * 2]
                                                                .imageURL,
                                                        height: 60,
                                                        width: 60),
                                                  ),
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.2,
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.2,
                                                ),
                                                onTap: () {
                                                  Navigator.of(context)
                                                      .pushNamed(
                                                    TrackListPage.route,
                                                    arguments: {
                                                      'id':
                                                          _albums[index * 2].id,
                                                      'name': _albums[index * 2]
                                                          .name,
                                                      'image':
                                                          _albums[index * 2]
                                                              .imageURL,
                                                      'routingFrom': 'album',
                                                      'partyMode': false
                                                    },
                                                  );
                                                },
                                              ),
                                              Center(
                                                child: Container(
                                                  margin:
                                                      EdgeInsets.only(top: 5),
                                                  width: 140,
                                                  child: Center(
                                                    child: Text(
                                                      _albums[index * 2].name +
                                                          ' by ' +
                                                          _albums[index * 2]
                                                              .artistName,
                                                      style: TextStyle(
                                                          fontSize: 12),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Column(
                                            children: <Widget>[
                                              InkWell(
                                                child: Container(
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8.0),
                                                    child: FadeInImage.assetNetwork(
                                                        placeholder:
                                                            'assets/colorful_loader.gif',
                                                        image: _albums[
                                                                index * 2 + 1]
                                                            .imageURL,
                                                        height: 60,
                                                        width: 60),
                                                  ),
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.2,
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.2,
                                                ),
                                                onTap: () {
                                                  Navigator.of(context)
                                                      .pushNamed(
                                                    TrackListPage.route,
                                                    arguments: {
                                                      'id':
                                                          _albums[index * 2 + 1]
                                                              .id,
                                                      'name':
                                                          _albums[index * 2 + 1]
                                                              .name,
                                                      'image':
                                                          _albums[index * 2 + 1]
                                                              .imageURL,
                                                      'routingFrom': 'album',
                                                      'partyMode': false
                                                    },
                                                  );
                                                },
                                              ),
                                              Center(
                                                child: Container(
                                                  margin:
                                                      EdgeInsets.only(top: 5),
                                                  width: 140,
                                                  child: Center(
                                                    child: Text(
                                                      _albums[index * 2 + 1]
                                                              .name +
                                                          ' by ' +
                                                          _albums[index * 2 + 1]
                                                              .artistName,
                                                      style: TextStyle(
                                                          fontSize: 12),
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                ),
              ),
              _recentlyPlayedTracks.length != 0
                  ? Container(
                      height: MediaQuery.of(context).size.height * 0.6,
                      width: MediaQuery.of(context).size.width * 0.95,
                      margin: EdgeInsets.all(10),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                        child: Container(
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(left: 18, top: 10),
                                    child: Text(
                                      'Recently played',
                                      style: TextStyle(
                                          fontSize: 30,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                  Spacer(),
                                  Container(
                                    width: 70,
                                    margin: EdgeInsets.only(right: 18, top: 10),
                                    child: RaisedButton(
                                      color: Colors.grey[800],
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(18.0),
                                          side: BorderSide(
                                              color: Colors.white, width: 2)),
                                      child: Text(
                                        'View all',
                                        style: TextStyle(fontSize: 10),
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pushNamed(
                                            FavoritesRecentPage.route,
                                            arguments: {
                                              'routingFrom': 'recentlyPlayedAll'
                                            });
                                      },
                                    ),
                                  )
                                ],
                              ),
                              Column(
                                children: List.generate(
                                    _recentlyPlayedTracks.length > 4
                                        ? 4
                                        : _recentlyPlayedTracks.length,
                                    (index) {
                                  return trackWidget(
                                      _recentlyPlayedTracks[index]);
                                }),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  : SizedBox.shrink()
            ],
          ),
          MiniPlayer(),
        ],
      ),
    );
  }

  Widget profilePopupButton() => PopupMenuButton(
        offset: Offset(0, 100),
        itemBuilder: (context) => [
          PopupMenuItem(
            value: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(Icons.exit_to_app),
                Text(
                  'Log out',
                  style: TextStyle(fontSize: 12),
                )
              ],
            ),
          ),
        ],
        onSelected: (value) {
          if (value == 1) {
            logout();
          }
        },
        child: Container(
          margin: EdgeInsets.all(8),
          padding: EdgeInsets.only(right: 10),
          child: ClipOval(
            child: Image.network(_imageUrl),
          ),
        ),
      );
  void logout() {
    TrackPlayer _trackProvider = Provider.of<TrackPlayer>(context);
    Party _partyProvider = Provider.of<Party>(context);
    PartyMember _partyMemberProvider = Provider.of<PartyMember>(context);
    PartyLobby _partyLobbyprovider = Provider.of<PartyLobby>(context);
    Profile _profileProvider = Provider.of<Profile>(context);
    if (_trackProvider.didTrackStart) {
      _trackProvider.stop();
    }
    if (_partyMemberProvider.didTrackStart) {
      _partyMemberProvider.exit();
    }
    if (_partyProvider.didTrackStart) {
      _partyProvider.exit();
    }
    if (_partyLobbyprovider.isListening) {
      _partyLobbyprovider.stopListening();
    }
    if (_profileProvider.isListening) _profileProvider.stopListening();
    kError = false;
    kNotificationsViewed = false;
    kIsParty = false;
    shouldPlaylistRefresh = false;
    shouldFavoriteRefresh = false;
    Provider.of<Auth>(context).logout();
  }

  Widget trackWidget(Track track) {
    return Container(
      margin: EdgeInsets.only(top: 10, left: 10),
      child: InkWell(
        onTap: () {
          Navigator.of(context).pushNamed(
            MusicPlaybackPage.route,
            arguments: {
              'url': track.getURL,
              'currentlyPlaying': 'recentlyPlayedAll',
              'image': track.getImage,
              'trackList': _recentlyPlayedTracks,
              'index': _recentlyPlayedTracks.indexOf(track)
            },
          );
        },
        child: Row(
          children: <Widget>[
            Container(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: FadeInImage.assetNetwork(
                    placeholder: 'assets/colorful_loader.gif',
                    image: track.getImage,
                    height: 60,
                    width: 60),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    margin: EdgeInsets.only(left: 10),
                    width: 170,
                    child: Text(
                      track.getName,
                      textAlign: TextAlign.left,
                      style:
                          TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    margin: EdgeInsets.only(left: 10, top: 5),
                    width: 170,
                    child: Text(
                      track.getArtist,
                      style: TextStyle(color: Colors.grey),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
              ],
            ),
            Spacer(),
            Expanded(
              child: PopupTracks(track.getId),
            ),
          ],
        ),
      ),
    );
  }
}
