import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../Providers/party.dart';

class PlayPrevIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _partyProvider = Provider.of<Party>(context);
    return IconButton(
      icon: Icon(
        Icons.skip_previous,
        size: 30,
      ),
      onPressed: () {
        _partyProvider.playPrev();
      },
    );
  }
}
