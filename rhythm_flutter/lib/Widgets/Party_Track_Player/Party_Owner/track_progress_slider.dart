import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../Providers/party.dart';

class TrackProgressSlider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _partyProvider = Provider.of<Party>(context);
    return Slider(
      activeColor: Colors.white,
      value: _partyProvider.getPosition.inSeconds.toDouble(),
      min: 0,
      max: _partyProvider.isSet
          ? _partyProvider.getDuration == Duration(seconds: 0)
              ? 999999999999999
              : _partyProvider.getDuration.inSeconds.toDouble()
          : 999999999999999,
      onChanged: (double value) {
        if (_partyProvider.isSet) if (_partyProvider.didTrackStart)
          _partyProvider.seekToSecond(
            value.toInt(),
          );
      },
    );
  }
}
