import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import '../../../Providers/party.dart';

class TrackProgressAt extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _partyProvider = Provider.of<Party>(context);
    return Container(
      child: Text(
        _partyProvider.getPosition.toString().substring(2, 7),
        style: TextStyle(fontSize: 12),
      ),
      margin: EdgeInsets.only(left: 10),
    );
  }
}
