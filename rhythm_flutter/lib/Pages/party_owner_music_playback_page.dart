import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../constants.dart';
import '../Providers/party.dart';
import '../Providers/party_member.dart';
import '../Providers/track_player.dart';

import '../Widgets/Party_Track_Player/Party_Owner/track_player_widget.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class PartyMusicPlaybackPage extends StatefulWidget {
  static const route = '/party_player';
  @override
  _PartyMusicPlaybackPageState createState() => _PartyMusicPlaybackPageState();
}

class _PartyMusicPlaybackPageState extends State<PartyMusicPlaybackPage> {
  var _isInit = true;
  String _imageURL;
  @override
  void didChangeDependencies() {
    if (_isInit) {
      kIsParty = true;

      final routeArgs =
          ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
      final Party _partyProvider = Provider.of<Party>(context);
      final String _url = routeArgs['url'];
      var _trackList = routeArgs['trackList'];
      var _index = routeArgs['index'];
      _imageURL = routeArgs['image'];
      var _currentlyPlaying = currentlyPlaying;
      if (_currentlyPlaying == '')
        ;
      else if (_currentlyPlaying == 'partyMember') {
        Provider.of<PartyMember>(context).exit();
      } else {
        Provider.of<TrackPlayer>(context).stop();
      }
      currentlyPlaying = 'partyOwner';
      _partyProvider.setAudioPlayer(_url, _imageURL, _index, _trackList);
      _isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<Party>(
      builder: (ctx, _partyProvider, _) => Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            Center(
              child: Text('Exit', style: TextStyle(fontSize: 18)),
            ),
            IconButton(
              icon: Icon(MdiIcons.exitToApp),
              onPressed: () {
                openDialog(context);
              },
            ),
          ],
        ),
        body: PartyOwnerTrackPlayerWidget(_partyProvider.image),
      ),
    );
  }

  openDialog(BuildContext context) {
    var _partyProvider = Provider.of<Party>(context);
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text("Confirm exit party"),
          content: Text(
              "Exiting party will force all members to quit. Are you sure you want to exit?"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("Quit"),
              onPressed: () {
                _partyProvider.exit();
                Navigator.popUntil(context, ModalRoute.withName('/'));
              },
            ),
          ],
        );
      },
    );
  }
}
