import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../Providers/track_player.dart';

class PlayNextIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _trackProvider = Provider.of<TrackPlayer>(context);
    return IconButton(
      icon: Icon(Icons.skip_next, size: 30),
      onPressed: () {
        _trackProvider.playNext();
      },
    );
  }
}
