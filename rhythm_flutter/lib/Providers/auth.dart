import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../constants.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class Auth with ChangeNotifier {
  String _token;
  String _userID;
  String _name;
  String _image;
  Future<void> _authenticate(
      String type, Map<String, dynamic> body, Function callback) async {
    try {
      final url = kUrl + 'auth/$type';
      final response = await http.post(url, body: body);
      if (response.statusCode != 200)
        throw json.decode(response.body)['message'];
      callback(response);
    } catch (error) {
      kError = true;
      authKey.currentState.removeCurrentSnackBar();
      authKey.currentState.showSnackBar(
        SnackBar(
          duration: Duration(seconds: 3),
          content: Text('Wrong email or password'),
        ),
      );
    }
  }

  Future<void> login(String email, String password) async {
    return _authenticate('login', {'email': email, 'password': password},
        (response) async {
      final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
      final _fcmtoken = await _firebaseMessaging.getToken();
      final responseContent = json.decode(response.body);
      _token = responseContent['access_token'];
      _userID = responseContent['user_id'].toString();
      _name = responseContent['name'];
      _image = responseContent['image'];
      final prefs = await SharedPreferences.getInstance();
      final userData = json.encode(
        {
          'token': _token,
          'userId': _userID,
          'username': _name,
          'image': _image
        },
      );
      prefs.setString('userData', userData);
      http.post(kUrl + 'users/fcmToken/add', headers: {
        'authorization': 'Bearer $_token'
      }, body: {
        'token': _fcmtoken,
      });
      notifyListeners();
    });
  }

  Future<void> register(
      String firstname, String lastname, String email, String password) async {
    _authenticate(
        'register',
        {
          'firstname': firstname,
          'lastname': lastname,
          'email': email,
          'password': password,
        },
        (response) {});
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
        json.decode(prefs.getString('userData')) as Map<String, Object>;
    _token = extractedUserData['token'];
    _userID = extractedUserData['userId'];
    _name = extractedUserData['username'];
    _image = extractedUserData['image'];
    notifyListeners();

    return true;
  }

  Future<void> logout() async {
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
    http.post(kUrl + 'users/fcmToken/delete', headers: {
      'authorization': 'Bearer $_token'
    }, body: {
      'token': await _firebaseMessaging.getToken(),
    });
    _token = null;
    _userID = null;
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('userData');
    prefs.clear();
  }

  bool get isAuth {
    return token != null;
  }

  String get token {
    return _token;
  }

  String get userID {
    return _userID;
  }

  String get userImage => _image;

  String get name => _name;
}
