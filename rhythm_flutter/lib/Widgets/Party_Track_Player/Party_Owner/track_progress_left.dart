import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import '../../../Providers/party.dart';

class TrackProgressLeft extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _partyProvider = Provider.of<Party>(context);
    return Container(
      alignment: Alignment.centerRight,
      child: Text(
        _partyProvider.isSet
            ? _partyProvider.getTimeLeft().toString().substring(2, 7)
            : '00:00',
        style: TextStyle(fontSize: 12),
      ),
      margin: EdgeInsets.only(right: 10),
    );
  }
}
