<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Album;

class AlbumController extends Controller
{
    public function index()
    {
        $albums = Album::all();
        foreach ($albums as $album) {
            $tracks = $album->tracks;
            foreach ($tracks as $track) {
                $track->album;
                $track->artist;
            }
            $album->artist;
        }
        return $albums;
    }
}
