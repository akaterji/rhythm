<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function playlists()
    {
        return $this->hasMany('App\Playlist');
    }
    public function friends()
    {
        return $this->belongsToMany('App\User', 'friends', 'user_id', 'friend_id');
    }

    public function addFriend(String $id)
    {
        $this->friends()->attach($id);
    }

    public function removeFriend(String $id)
    {
        $this->friends()->detach($id);
    }
    public function friendRequests()
    {
        return $this->belongsToMany('App\User', 'friend_requests', 'user_id', 'friend_id');
    }
    public function friendRequesting()
    {
        return $this->belongsToMany('App\User', 'friend_requests', 'friend_id', 'user_id');
    }
    public function addFriendRequest(String $id)
    {
        $this->friendRequests()->attach($id);
    }

    public function deleteFriendRequest(String $id)
    {
        $this->friendRequests()->detach($id);
    }
    public function sharedPlaylists()
    {
        return $this->belongsToMany('App\Playlist', 'shared_playlists');
    }
    public function favorites()
    {
        return $this->belongsToMany('App\Track', 'user_favorites');
    }
    public function fcmTokens()
    {
        return $this->hasMany('App\FcmToken');
    }
    public function recentlyPlayed()
    {
        return $this->belongsToMany('App\Track', 'recently_played')->withTimestamps();
        ;
    }
}
