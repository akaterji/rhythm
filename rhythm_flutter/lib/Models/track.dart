import 'package:flutter/widgets.dart';

class Track with ChangeNotifier {
  String _id;
  String _name;
  String _artist;
  String _album;
  String _trackURL;
  String _trackImageURL;
  bool _selected;
  bool _favorite;

  Track(this._id, this._name, this._artist, this._album, this._trackURL,
      this._trackImageURL, this._favorite,
      [this._selected]);
  void setSelect() {
    _selected ? _selected = false : _selected = true;
  }

  bool get selected => _selected;
  String get getId => _id;
  String get getName => _name;
  String get getArtist => _artist;
  String get getAlbum => _album;
  String get getURL => _trackURL;
  String get getImage => _trackImageURL;
  bool get favorite => _favorite;
  void toggleFavorite() => _favorite = !_favorite;
}
