import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../Providers/track_player.dart';

class TrackProgressSlider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _trackProvider = Provider.of<TrackPlayer>(context);
    return Slider(
      activeColor: Colors.white,
      value: _trackProvider.getPosition.inSeconds.toDouble(),
      min: 0,
      max: _trackProvider.isSet
          ? _trackProvider.getDuration == Duration(seconds: 0)
              ? 999999999999999
              : _trackProvider.getDuration.inSeconds.toDouble()
          : 999999999999999,
      onChanged: (double value) {
        if (_trackProvider.isSet) if (_trackProvider.didTrackStart)
          _trackProvider.seekToSecond(
            value.toInt(),
          );
      },
    );
  }
}
