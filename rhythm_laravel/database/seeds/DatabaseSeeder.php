<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;
use App\Track;
use App\Playlist;
use App\Album;
use App\Artist;
use App\Friend;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($x = 1 ; $x<11;$x++) {
            if ($x == 1) {
                $fn = 'Abed';
                $ln = 'Katerji';
                $email = 'abed@gmail.com';
                $image = 'http://18.195.20.184/users/abed-profile.jpeg';
            } elseif ($x == 2) {
                $fn = 'John';
                $ln = 'Thompson';
                $email = 'john@gmail.com';
                $image = 'http://18.195.20.184/users/ali.jpeg';
            } elseif ($x == 3) {
                $fn = 'Ali';
                $ln = 'Najjar';
                $email = 'a@a.com';
                $image = 'http://18.195.20.184/users/ayman.jpeg';
            } elseif ($x == 4) {
                $fn = 'Ayman';
                $ln = 'Hamdoun';
                $email = 'b@b.com';
                $image = 'http://18.195.20.184/users/spare.jpeg';
            } else {
                $fn = $faker->firstname;
                $ln = $faker->lastname;
                $email = $faker->email;
                $image = 'http://18.195.20.184/users/default.png';
            }
            DB::table('users')->insert([
                'firstname'=> $fn,
                'lastname'=> $ln,
                'email'=> $email,
                'password'=> password_hash('password', PASSWORD_DEFAULT),
                'image'=> $image,
                'recently_played'=>mt_rand(1, 20),
                'recently_played_at'=>now(),
                'created_at'=>now(),
                'updated_at'=>now(),
            ]);
            
            DB::table('playlists')->insert([
                'name'=>$faker->word,
                'user_id'=>$x,
                'created_at'=>now(),
                'updated_at'=>now(),
            ]);
        }
        DB::table('artists')->insert([
            'name'=>'Adele',
            'created_at'=>now(),
            'updated_at'=>now(),
            ]);
        DB::table('albums')->insert([
                'name'=>'25',
                'artist_id'=>1,
                'imageURL'=>'http://18.195.20.184/tracks/adele/25.png',
                'created_at'=>now(),
                'updated_at'=>now(),
            ]);
        DB::table('artists')->insert([
            'name'=>'Eminem',
            'created_at'=>now(),
            'updated_at'=>now(),
            ]);
        DB::table('albums')->insert([
                'name'=>'Kamikaze',
                'artist_id'=>2,
                'imageURL'=>'http://18.195.20.184/tracks/eminem/Kamikaze.jpeg',
                'created_at'=>now(),
                'updated_at'=>now(),
            ]);
        DB::table('artists')->insert([
            'name'=>'Ed Sheeran',
            'created_at'=>now(),
            'updated_at'=>now(),
            ]);
        DB::table('albums')->insert([
                'name'=>'Divide',
                'artist_id'=>3,
                'imageURL'=>'http://18.195.20.184/tracks/ed_sheeran/Divide.jpeg',
                'created_at'=>now(),
                'updated_at'=>now(),
            ]);
    
        DB::table('artists')->insert([
            'name'=>'Taylor Swift',
            'created_at'=>now(),
            'updated_at'=>now(),
            ]);
        DB::table('albums')->insert([
                'name'=>'Reputation',
                'artist_id'=>4,
                'imageURL'=>'http://18.195.20.184/tracks/taylor_swift/Reputation.jpeg',
                'created_at'=>now(),
                'updated_at'=>now(),
            ]);
        DB::table('artists')->insert([
                'name'=>'Sia',
                'created_at'=>now(),
                'updated_at'=>now(),
                ]);
        DB::table('albums')->insert([
                    'name'=>'This Is Acting',
                    'artist_id'=>5,
                    'imageURL'=>'http://18.195.20.184/tracks/sia/This Is Acting.png',
                    'created_at'=>now(),
                    'updated_at'=>now(),
                ]);
        for ($x = 6;$x<11;$x++) {
            DB::table('artists')->insert([
                'name'=>$faker->firstname,
                'created_at'=>now(),
                'updated_at'=>now(),
                ]);
            DB::table('albums')->insert([
                    'name'=>$faker->word,
                    'artist_id'=>$x,
                    'imageURL'=>'http://18.195.20.184/tracks/albums/album-1.jpg',
                    'created_at'=>now(),
                    'updated_at'=>now(),
                ]);
        }
        $user = User::find(1);
        for ($y=2;$y<10;$y++) {
            $user2 = User::find($y);
            $user->addFriend($user2->id);
            $user2->addFriend($user->id);
        }
        for ($z=1;$z<11;$z++) {
            $user = User::find($z);
            if ($z+1<9) {
                $user->sharedPlaylists()->attach($z+1);
            }
        }


        //adele
        DB::table('tracks')->insert([
                    'name'=>'Hello',
                    'url'=>'http://18.195.20.184/tracks/adele/25/Hello.mp3',
                    'image'=>'http://18.195.20.184/tracks/adele/25/Hello.png',
                    'artist_id' => 1,
                    'album_id' => 1,
                    ]);
        DB::table('tracks')->insert([
                    'name'=>'Million Years Ago',
                    'url'=>'http://18.195.20.184/tracks/adele/25/Million Years Ago.mp3',
                    'image'=>'http://18.195.20.184/tracks/adele/25/Million Years Ago.png',
                    'artist_id' => 1,
                    'album_id' => 1,
                    ]);
        DB::table('tracks')->insert([
                    'name'=>'I Miss You',
                    'url'=>'http://18.195.20.184/tracks/adele/25/I Miss You.mp3',
                    'image'=>'http://18.195.20.184/tracks/adele/25/I Miss You.png',
                    'artist_id' => 1,
                    'album_id' => 1,
                    ]);
        DB::table('tracks')->insert([
                    'name'=>'Water Under The Bridge',
                    'url'=>'http://18.195.20.184/tracks/adele/25/Water Under The Bridge.mp3',
                    'image'=>'http://18.195.20.184/tracks/adele/25/Water Under The Bridge.png',
                    'artist_id' => 1,
                    'album_id' => 1,
                    ]);
        DB::table('tracks')->insert([
                    'name'=>'When We Were Young',
                    'url'=>'http://18.195.20.184/tracks/adele/25/When We Were Young.mp3',
                    'image'=>'http://18.195.20.184/tracks/adele/25/When We Were Young.png',
                    'artist_id' => 1,
                    'album_id' => 1,
                    ]);
        //eminem
        DB::table('tracks')->insert([
                    'name'=>'Venom',
                    'url'=>'http://18.195.20.184/tracks/eminem/kamikaze/Venom.mp3',
                    'image'=>'http://18.195.20.184/tracks/eminem/kamikaze/Venom.jpeg',
                    'artist_id' => 2,
                    'album_id' =>2,
                    ]);
        DB::table('tracks')->insert([
                    'name'=>'Kamikaze',
                    'url'=>'http://18.195.20.184/tracks/eminem/kamikaze/Kamikaze.mp3',
                    'image'=>'http://18.195.20.184/tracks/eminem/kamikaze/Kamikaze.jpeg',
                    'artist_id' => 2,
                    'album_id' => 2,
                    ]);
        DB::table('tracks')->insert([
                    'name'=>'The Ringer',
                    'url'=>'http://18.195.20.184/tracks/eminem/kamikaze/The Ringer.mp3',
                    'image'=>'http://18.195.20.184/tracks/eminem/kamikaze/The Ringer.jpeg',
                    'artist_id' => 2,
                    'album_id' => 2,
                    ]);
        DB::table('tracks')->insert([
                    'name'=>'Fall',
                    'url'=>'http://18.195.20.184/tracks/eminem/kamikaze/Fall.mp3',
                    'image'=>'http://18.195.20.184/tracks/eminem/kamikaze/Fall.jpeg',
                    'artist_id' => 2,
                    'album_id' => 2,
                    ]);
        //ed sheeran
        DB::table('tracks')->insert([
                    'name'=>'Shape Of You',
                    'url'=>'http://18.195.20.184/tracks/ed_sheeran/divide/Shape Of You.mp3',
                    'image'=>'http://18.195.20.184/tracks/ed_sheeran/divide/Shape Of You.jpeg',
                    'artist_id' => 3,
                    'album_id' => 3,
        ]);
        DB::table('tracks')->insert([
                    'name'=>'Perfect',
                    'url'=>'http://18.195.20.184/tracks/ed_sheeran/divide/Perfect.mp3',
                    'image'=>'http://18.195.20.184/tracks/ed_sheeran/divide/Perfect.jpeg',
                    'artist_id' => 3,
                    'album_id' => 3,
                    ]);
        DB::table('tracks')->insert([
                    'name'=>'Happier',
                    'url'=>'http://18.195.20.184/tracks/ed_sheeran/divide/Happier.mp3',
                    'image'=>'http://18.195.20.184/tracks/ed_sheeran/divide/Happier.jpeg',
                    'artist_id' => 3,
                    'album_id' => 3,
                    ]);
        DB::table('tracks')->insert([
                    'name'=>'What Do I Know',
                    'url'=>'http://18.195.20.184/tracks/ed_sheeran/divide/What Do I Know.mp3',
                    'image'=>'http://18.195.20.184/tracks/ed_sheeran/divide/What Do I Know.jpeg',
                    'artist_id' => 3,
                    'album_id' => 3,
                    ]);
        //taylor swift
        DB::table('tracks')->insert([
                        'name'=>'Delicate',
                        'url'=>'http://18.195.20.184/tracks/taylor_swift/reputation/Delicate.mp3',
                        'image'=>'http://18.195.20.184/tracks/taylor_swift/reputation/Delicate.jpeg',
                        'artist_id' => 4,
                        'album_id' => 4,
                        ]);
        DB::table('tracks')->insert([
                        'name'=>'End Game',
                        'url'=>'http://18.195.20.184/tracks/taylor_swift/reputation/End Game.mp3',
                        'image'=>'http://18.195.20.184/tracks/taylor_swift/reputation/End Game.jpeg',
                        'artist_id' => 4,
                        'album_id' =>4,
                        ]);
        DB::table('tracks')->insert([
                        'name'=>'Gorgeous',
                        'url'=>'http://18.195.20.184/tracks/taylor_swift/reputation/Gorgeous.mp3',
                        'image'=>'http://18.195.20.184/tracks/taylor_swift/reputation/Gorgeous.jpeg',
                        'artist_id' => 4,
                        'album_id' => 4,
                        ]);
        DB::table('tracks')->insert([
                        'name'=>'Look What You Made Me Do',
                        'url'=>'http://18.195.20.184/tracks/taylor_swift/reputation/Look What You Made Me Do.mp3',
                        'image'=>'http://18.195.20.184/tracks/taylor_swift/reputation/Look What You Made Me Do.jpeg',
                        'artist_id' =>4,
                        'album_id' => 4,
                        ]);
        DB::table('tracks')->insert([
                        'name'=>'I Did Something Bad',
                        'url'=>'http://18.195.20.184/tracks/taylor_swift/reputation/I Did Something Bad.mp3',
                        'image'=>'http://18.195.20.184/tracks/taylor_swift/reputation/I Did Something Bad.jpeg',
                        'artist_id' => 4,
                        'album_id' => 4,
                        ]);
        //sia
        DB::table('tracks')->insert([
                            'name'=>'Alive',
                            'url'=>'http://18.195.20.184/tracks/sia/this is acting/Alive.mp3',
                            'image'=>'http://18.195.20.184/tracks/sia/this is acting/Alive.png',
                            'artist_id' => 5,
                            'album_id' => 5,
                            ]);
        DB::table('tracks')->insert([
                            'name'=>'Bird Set Free',
                            'url'=>'http://18.195.20.184/tracks/sia/this is acting/Bird Set Free.mp3',
                            'image'=>'http://18.195.20.184/tracks/sia/this is acting/Bird Set Free.png',
                            'artist_id' => 5,
                            'album_id' => 5,
                            ]);
        DB::table('tracks')->insert([
                            'name'=>'Cheap Thrills',
                            'url'=>'http://18.195.20.184/tracks/sia/this is acting/Cheap Thrills.mp3',
                            'image'=>'http://18.195.20.184/tracks/sia/this is acting/Cheap Thrills.png',
                            'artist_id' => 5,
                            'album_id' => 5,
                            ]);
        DB::table('tracks')->insert([
                            'name'=>'Move Your Body',
                            'url'=>'http://18.195.20.184/tracks/sia/this is acting/Move Your Body.mp3',
                            'image'=>'http://18.195.20.184/tracks/sia/this is acting/Move Your Body.png',
                            'artist_id' => 5,
                            'album_id' => 5,
                            ]);
        DB::table('tracks')->insert([
                            'name'=>'Reaper',
                            'url'=>'http://18.195.20.184/tracks/sia/this is acting/Reaper.mp3',
                            'image'=>'http://18.195.20.184/tracks/sia/this is acting/Reaper.png',
                            'artist_id' => 5,
                            'album_id' => 5,
                            ]);
               
        for ($x=1;$x<11;$x++) {
            $playlist = Playlist::find($x);
            $faker = Faker::create();
            for ($y = 1;$y<4;$y++) {
                $playlist->tracks()->attach($faker->unique()->numberBetween($min=1, $max=23));
            }
        }
        for ($x=1;$x<11;$x++) {
            $user = User::find($x);
            $faker = Faker::create();
            for ($y = 1;$y<11;$y++) {
                $user->recentlyPlayed()->attach($faker->unique()->numberBetween($min=1, $max=23));
            }
        }
        for ($x=1;$x<11;$x++) {
            $user = User::find($x);
            $faker = Faker::create();
            for ($y = 1;$y<4;$y++) {
                $user->favorites()->attach($faker->unique()->numberBetween($min=1, $max=23));
            }
        }
    }
}
