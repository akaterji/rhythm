import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import '../../Providers/track_player.dart';

class PartyListTrackProgressAt extends StatelessWidget {
  final _position;
  final _isListening;
  PartyListTrackProgressAt(this._position, this._isListening);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        _position,
        style: TextStyle(
            fontSize: 12,
            color: this._isListening ? Colors.white : Colors.black),
      ),
      margin: EdgeInsets.only(left: 10),
    );
  }
}
