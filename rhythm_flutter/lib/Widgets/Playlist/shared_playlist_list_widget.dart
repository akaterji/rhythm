import 'package:flutter/material.dart';
import 'package:rhythm_flutter/Providers/playlists.dart';
import 'package:provider/provider.dart';
import './PopupMenuButtonCustomPlaylist.dart';
import './PopupMenuButtonCustomSharedPlaylist.dart';

import '../../Providers/playlist.dart';

import '../../Pages/tracks_playlist_page.dart';

class SharedPlaylistListWidget extends StatelessWidget {
  final bool _partyMode;
  SharedPlaylistListWidget(this._partyMode);
  @override
  Widget build(BuildContext context) {
    final _playlistsProvider = Provider.of<Playlists>(context);
    List<Playlist> _sharedPlaylists = _playlistsProvider.sharedPlaylists;
    return Container(
      child: Column(
          children: _sharedPlaylists.map(
        (item) {
          return InkWell(
            onTap: () {
              Navigator.pushNamed(
                context,
                TracksPlaylistPage.route,
                arguments: {
                  'id': item.id,
                  'name': item.name,
                  'trackList': item.tracks,
                  'partyMode': _partyMode,
                  'sharedPlaylist': true
                },
              );
            },
            child: Container(
              margin: EdgeInsets.only(top: 15),
              height: 100,
              child: Row(
                children: <Widget>[
                  // Image.network(item.getImage),

                  Text(item.name),
                  Text(' shared by '),
                  Text(item.sharedFriendName),

                  Spacer(), // fix it to make it position of pop up menu to be dynamic
                  PopupMenuButtonCustomSharedPlaylist(item.id)
                ],
              ),
            ),
          );
        },
      ).toList()),
    );
  }
}
