import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import '../Models/track.dart';
import '../constants.dart';

class SearchProvider with ChangeNotifier {
  final String _token;
  List<Track> _result = [];
  SearchProvider(this._token);
  String url = kUrl;
  Future<void> fetch(url, callback) async {
    try {
      final response =
          await http.get(url, headers: {'authorization': 'Bearer $_token'});
      if (response.statusCode != 200)
        throw json.decode(response.body)['message'];
      callback(response);
    } catch (error) {
      kError = true;
      key.currentState.removeCurrentSnackBar();
      key.currentState.showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          content: Text('Something went wrong'),
          action: SnackBarAction(
            label: 'Retry',
            onPressed: () {
              notifyListeners();
            },
          ),
        ),
      );
    }
  }

  void search(String text, String searchBy) {
    fetch(url + 'tracks/search?text=$text&searchby=$searchBy', (response) {
      _result = [];
      var _decoded = json.decode(response.body);
      List<dynamic> _resultBackend = _decoded['tracks'];
      _resultBackend.forEach((track) {
        _result.add(
          Track(track['id'].toString(), track['name'], track['artist']['name'],
              track['album']['name'], track['url'], track['image'], false),
        );
      });
      notifyListeners();
    });
  }

  void empty() {
    _result = [];
    notifyListeners();
  }

  List<Track> get tracks => _result;
}
