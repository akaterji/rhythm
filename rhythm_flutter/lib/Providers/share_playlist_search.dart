import 'package:flutter/widgets.dart';
import 'package:rhythm_flutter/Models/user.dart';

class SharePlaylistSearch with ChangeNotifier {
  List<dynamic> _friends;
  String _playlistId;
  List<dynamic> _result = [];
  void search(String text) {
    _result = [];
    _friends.forEach(
      (friend) {
        if (friend.firstName.toLowerCase().contains(text)) {
          _result.add(friend);
        }
      },
    );
    notifyListeners();
  }

  void reset() {
    _result = [];
    notifyListeners();
  }

  void setFriends(List<dynamic> friends) => _friends = friends;
  List<dynamic> get result => _result;
}
