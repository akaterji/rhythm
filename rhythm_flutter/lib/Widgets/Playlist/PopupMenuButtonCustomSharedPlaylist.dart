import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../Providers/playlists.dart';
import '../../Providers/playlist.dart';
import '../../Models/user.dart';

class PopupMenuButtonCustomSharedPlaylist extends StatefulWidget {
  final String _playlistId;
  PopupMenuButtonCustomSharedPlaylist(this._playlistId);

  @override
  _PopupMenuButtonCustomSharedPlaylistState createState() =>
      _PopupMenuButtonCustomSharedPlaylistState();
}

class _PopupMenuButtonCustomSharedPlaylistState
    extends State<PopupMenuButtonCustomSharedPlaylist> {
  Playlists _playlistsProvider;
  Playlist _playlistProvider;
  var _isInit = true;
  @override
  void didChangeDependencies() {
    if (_isInit) {
      _playlistsProvider = Provider.of<Playlists>(context);
      _playlistProvider = Provider.of<Playlist>(context);
      _isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
        PopupMenuItem<String>(
          value: '1',
          child: Row(
            children: <Widget>[
              Icon(Icons.delete),
              SizedBox(
                width: 10,
              ),
              Text(
                'Remove playlist',
              )
            ],
          ),
        ),
      ],
      onSelected: (value) {
        if (value == '1') {
          _playlistProvider.unSharePlaylist(widget._playlistId);
          _playlistsProvider.removeSharedPlaylist(widget._playlistId);
        }
      },
    );
  }
}
