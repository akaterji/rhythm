import 'package:flutter/material.dart';
import 'package:rhythm_flutter/Models/user.dart';
import 'package:rhythm_flutter/Providers/profile.dart';
import 'package:provider/provider.dart';
import 'package:rhythm_flutter/Widgets/Mini_Player/mini_player.dart';

class SearchFriendsPage extends StatefulWidget {
  static const route = '/search_friends';
  @override
  _SearchFriendsPageState createState() => _SearchFriendsPageState();
}

class _SearchFriendsPageState extends State<SearchFriendsPage> {
  TextEditingController _searchController;
  Profile _profileProvider;
  var _isInit = true;
  @override
  void initState() {
    _searchController = TextEditingController();
    _searchController.addListener(() {
      String text = _searchController.text;
      if (text.length == 0) {
        _profileProvider.clear();
      } else if (text.length > 2) {
        _profileProvider.search(text);
      }
    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      _profileProvider = Provider.of<Profile>(context);
      _profileProvider.unPopped();
      _isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          title: TextField(
            controller: _searchController,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: 'Search',
            ),
            style: TextStyle(fontSize: 25, decoration: TextDecoration.none),
          ),
        ),
        body: Stack(
          children: <Widget>[
            ListView(
              children: _profileProvider.result.map(
                (user) {
                  return Column(
                    children: <Widget>[
                      friendWidget(user),
                      Divider(
                        color: Colors.redAccent,
                      ),
                    ],
                  );
                },
              ).toList(),
            ),
            MiniPlayer()
          ],
        ),
      ),
      onWillPop: () {
        _profileProvider.clear();
        _profileProvider.popped();
        Navigator.pop(context, false);
        return Future.value(false);
      },
    );
  }

  Widget friendWidget(User user) {
    var deviceSize = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(top: 15),
      height: deviceSize.height * 0.2,
      child: Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 10, right: 10),
            height: deviceSize.height * 0.1,
            width: deviceSize.width * 0.15,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                  image: NetworkImage(user.image), fit: BoxFit.cover),
            ),
          ),
          Text(
            user.name(),
            style: TextStyle(fontSize: 17),
          ),
          Spacer(),
          user.isFriend
              ? Container(
                  margin: EdgeInsets.only(right: 15),
                  child: RaisedButton(
                    color: Colors.red,
                    child: Text('Remove'),
                    onPressed: () {
                      _profileProvider.removeFriend(user.id);
                    },
                  ),
                )
              : user.isFriendRequesting
                  ? Container(
                      margin: EdgeInsets.only(right: 15),
                      child: RaisedButton(
                        color: Colors.blue,
                        child: Text('Accept'),
                        onPressed: () {
                          _profileProvider.acceptFriendRequest(user.id);
                        },
                      ),
                    )
                  : user.isFriendRequested
                      ? Container(
                          margin: EdgeInsets.only(right: 15),
                          child: RaisedButton(
                            color: Colors.blue,
                            child: Text('Cancel request'),
                            onPressed: () {
                              _profileProvider.removeFriendRequest(user.id);
                            },
                          ),
                        )
                      : Container(
                          margin: EdgeInsets.only(right: 15),
                          child: RaisedButton(
                            color: Colors.blue,
                            child: Text('Add'),
                            onPressed: () {
                              _profileProvider.sendFriendRequest(user.id);
                            },
                          ),
                        ),
        ],
      ),
    );
  }
}
