import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../Providers/track_player.dart';

class RepeatIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _trackProvider = Provider.of<TrackPlayer>(context);
    return Container(
      width: 40,
      height: 40,
      child: RawMaterialButton(
        shape: CircleBorder(),
        elevation: 0.0,
        child: _trackProvider.isRepeat == true
            ? Icon(
                Icons.repeat,
                color: Colors.red,
              )
            : Icon(Icons.repeat),
        onPressed: () {
          _trackProvider.toggleRepeat();
        },
      ),
    );
  }
}
