import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../constants.dart';
import '../Providers/party_member.dart';
import '../Providers/party.dart';
import '../Providers/track_player.dart';

import '../Widgets/Party_Track_Player/Party_Member/track_player_widget.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class PartyMemberMusicPlaybackPage extends StatefulWidget {
  static const route = '/party_member_player';
  @override
  _PartyMemberMusicPlaybackPageState createState() =>
      _PartyMemberMusicPlaybackPageState();
}

class _PartyMemberMusicPlaybackPageState
    extends State<PartyMemberMusicPlaybackPage> {
  var _isInit = false;
  String _trackId;
  String _imageURL;
  int _index;
  PartyMember _partyMemberProvider;
  @override
  void didChangeDependencies() {
    if (!_isInit) {
      final routeArgs =
          ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
      _partyMemberProvider = Provider.of<PartyMember>(context);
      final String _ownerId = routeArgs['ownerId'];
      _partyMemberProvider.init(_ownerId);
      _trackId = routeArgs['trackId'];
      _imageURL = routeArgs['image'];
      var _currentlyPlaying = currentlyPlaying;
      if (_currentlyPlaying == '')
        ;
      else if (_currentlyPlaying == 'partyOwner') {
        Provider.of<Party>(context).exit();
      } else {
        Provider.of<TrackPlayer>(context).stop();
      }
      currentlyPlaying = 'partyMember';
      _isInit = true;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    kIsParty = true;
    return Consumer<PartyMember>(
      builder: (ctx, _partyMemberProvider, _) => Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            Center(
              child: Text('Exit', style: TextStyle(fontSize: 18)),
            ),
            IconButton(
              icon: Icon(MdiIcons.exitToApp),
              onPressed: () {
                openDialog(context);
              },
            ),
          ],
        ),
        body: _partyMemberProvider.partyDisbanded
            ? () {
                _partyMemberProvider.exit();
                Future.delayed(
                  Duration(seconds: 1),
                  () {
                    Navigator.of(context).pop();
                    showDialog(
                      context: context,
                      builder: (BuildContext ctx) {
                        return AlertDialog(
                          title: Center(
                            child: Text('Party disbanded!'),
                          ),
                          // content: Text('Party disbanded!'),
                          actions: <Widget>[
                            FlatButton(
                              child: Text('Okay'),
                              onPressed: () => Navigator.of(ctx).pop(),
                            )
                          ],
                        );
                      },
                    );
                  },
                );
              }()
            : PartyMemberTrackPlayerWidget(_imageURL, _trackId),
      ),
    );
  }

  void openDialog(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text("Confirm leave party"),
          content: Text("Are you sure you want to leave the party?"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("Exit"),
              onPressed: () {
                _partyMemberProvider.exit();
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
