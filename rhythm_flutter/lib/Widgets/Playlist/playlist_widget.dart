import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rhythm_flutter/Providers/tracks.dart';
import 'package:rhythm_flutter/Widgets/Favorites/favorites_widget.dart';
import 'package:rhythm_flutter/Widgets/Playlist/shared_playlists_widget.dart';
import '../Track_List/track_list_widget.dart';
import '../../Providers/playlists.dart';
import '../../Providers/playlist.dart';

import '../../Widgets/Playlist/my_playlists_widget.dart';

class PlaylistWidget extends StatefulWidget {
  static const route = '/playlist_tab';
  @override
  _PlaylistWidgetState createState() => _PlaylistWidgetState();
}

class _PlaylistWidgetState extends State<PlaylistWidget> {
  var _isInit = true;
  Playlists _playlistsProvider;
  List<Playlist> _sharedPlaylists;
  Tracks _tracksProvider;
  @override
  void didChangeDependencies() {
    if (_isInit) {
      _playlistsProvider = Provider.of<Playlists>(context);
      _tracksProvider = Provider.of<Tracks>(context);
      _isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var _deviceSize = MediaQuery.of(context).size;
    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    var partyMode;
    var _sharedPlaylists = _playlistsProvider.sharedPlaylists;
    routeArgs != null ? partyMode = true : partyMode = false;

    return partyMode
        ? Scaffold(
            appBar: AppBar(
              title: Text('Choose a playlist'),
            ),
            body: Container(
              height: _deviceSize.height,
              width: _deviceSize.width,
              child: ListView(
                children: <Widget>[
                  MyPlaylistsWidget(partyMode),
                  _sharedPlaylists.isNotEmpty
                      ? SharedPlaylistsWidget(partyMode)
                      : SizedBox(),
                  SizedBox(
                    height: _deviceSize.height * 0.3,
                  )
                ],
              ),
            ),
          )
        : _tracksProvider.favoriteTracks.length > 0
            ? Container(
                height: _deviceSize.height,
                width: _deviceSize.width,
                child: ListView(
                  children: <Widget>[
                    MyPlaylistsWidget(partyMode),
                    Container(
                      margin: EdgeInsets.only(bottom: 10, left: 15, top: 15),
                      child: Text(
                        'Likes',
                        style: TextStyle(
                            fontSize: 30, fontWeight: FontWeight.w600),
                      ),
                    ),
                    FavoritesWidget(),
                    SizedBox(
                      height: 140,
                    )
                  ],
                ),
              )
            : Container(
                height: _deviceSize.height,
                width: _deviceSize.width,
                child: ListView(
                  children: <Widget>[
                    MyPlaylistsWidget(partyMode),
                  ],
                ),
              );
  }
}
