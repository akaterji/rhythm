import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:loading/loading.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:rhythm_flutter/Providers/recently_played.dart';
import 'package:rhythm_flutter/constants.dart';

import '../Providers/track_player.dart';
import '../Providers/party.dart';
import '../Providers/party_member.dart';
import '../Widgets/Track_Player/track_player_widget.dart';
import '../Widgets/Track_List/PopupTracks.dart';

class MusicPlaybackPage extends StatefulWidget {
  static const route = '/player';
  @override
  _MusicPlaybackPageState createState() => _MusicPlaybackPageState();
}

class _MusicPlaybackPageState extends State<MusicPlaybackPage> {
  var _isInit = true;

  @override
  Widget build(BuildContext context) {
    if (currentlyPlaying == 'partyOwner') Provider.of<Party>(context).exit();
    if (currentlyPlaying == 'partyMember')
      Provider.of<PartyMember>(context).exit();
    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    final TrackPlayer _trackProvider = Provider.of<TrackPlayer>(context);
    final String _url = routeArgs['url'];
    final String _currentlyPlaying = routeArgs['currentlyPlaying'];
    currentlyPlaying = _currentlyPlaying;
    final String _imageURL = routeArgs['image'];
    var _trackList = routeArgs['trackList'];
    var _index = routeArgs['index'];
    if (_isInit) {
      _trackProvider.setCurrentlyPlaying(_currentlyPlaying);
      _trackProvider.setAudioPlayer(_url, _imageURL, _index, _trackList);

      _isInit = false;
    }
    if (_trackProvider.newTrack) {
      Future.delayed(Duration(milliseconds: 500), () {
        Provider.of<RecentlyPlayed>(context).fetchRecentlyPlayed();
        _trackProvider.fetchedRecentlyPlayed();
      });
    }
    return Consumer<TrackPlayer>(
      builder: (ctx, _trackProvider, _) => Scaffold(
        appBar: AppBar(),
        body: TrackPlayerWidget(_imageURL),
      ),
    );
  }
}
