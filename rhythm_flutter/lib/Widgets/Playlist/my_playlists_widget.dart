import 'package:flutter/material.dart';
import 'package:rhythm_flutter/Widgets/Playlist/add_playlist.dart';
import 'package:rhythm_flutter/Widgets/Playlist/playlist_details.dart';
import 'package:rhythm_flutter/constants.dart';
import 'package:provider/provider.dart';
import '../../Providers/playlists.dart';
import '../../Providers/playlist.dart';

class MyPlaylistsWidget extends StatefulWidget {
  final _partyMode;
  MyPlaylistsWidget(this._partyMode);
  @override
  _MyPlaylistsWidgetState createState() => _MyPlaylistsWidgetState();
}

class _MyPlaylistsWidgetState extends State<MyPlaylistsWidget> {
  var _isInit = true;
  Playlists _playlistsProvider;
  List<Playlist> _playlists;
  List<Playlist> _sharedPlaylists;
  var _partyMode;
  @override
  void didChangeDependencies() {
    if (_isInit) {
      _partyMode = widget._partyMode;

      _isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    _playlistsProvider = Provider.of<Playlists>(context);
    _playlists = _playlistsProvider.playlists;
    _sharedPlaylists = _playlistsProvider.sharedPlaylists;
    var _deviceSize = MediaQuery.of(context).size;
    var _playlistsRendered = 0;
    return Container(
      margin: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
                Text(
                  'Your playlists',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w600),
                ),
              ],
            ),
            margin: EdgeInsets.only(left: 10, top: 10, bottom: 20),
          ),
          Container(
            child: Column(
              children: <Widget>[
                _playlists.isEmpty
                    ? AddNewPlaylistWidget(false)
                    : Column(
                        children: List.generate(
                          (_playlists.length / 3).ceil() + 1,
                          (index) {
                            if ((_playlists.length - 1) % 3 != 0)
                              return Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  index == 0
                                      ? AddNewPlaylistWidget(false)
                                      : _playlistsRendered < _playlists.length
                                          ? PlaylistDetails(
                                              _playlists[_playlistsRendered]
                                                      .tracks
                                                      .isEmpty
                                                  ? kDefualtPlaylistImage
                                                  : _playlists[_playlistsRendered]
                                                      .tracks[0]
                                                      .getImage,
                                              _playlists[_playlistsRendered].id,
                                              _playlists[_playlistsRendered++]
                                                  .name,
                                              false,
                                              _partyMode)
                                          : _playlistsRendered <
                                                  _playlists.length +
                                                      _sharedPlaylists.length
                                              ? PlaylistDetails(
                                                  _sharedPlaylists[_playlistsRendered - _playlists.length]
                                                          .tracks
                                                          .isEmpty
                                                      ? kDefualtPlaylistImage
                                                      : _sharedPlaylists[_playlistsRendered -
                                                              _playlists.length]
                                                          .tracks[0]
                                                          .getImage,
                                                  _sharedPlaylists[
                                                          _playlistsRendered -
                                                              _playlists.length]
                                                      .id,
                                                  _sharedPlaylists[
                                                          _playlistsRendered++ -
                                                              _playlists.length]
                                                      .name,
                                                  true,
                                                  _partyMode)
                                              : SizedBox.shrink(),
                                  _playlistsRendered < _playlists.length
                                      ? PlaylistDetails(
                                          _playlists[_playlistsRendered]
                                                  .tracks
                                                  .isEmpty
                                              ? kDefualtPlaylistImage
                                              : _playlists[_playlistsRendered]
                                                  .tracks[0]
                                                  .getImage,
                                          _playlists[_playlistsRendered].id,
                                          _playlists[_playlistsRendered++].name,
                                          false,
                                          _partyMode)
                                      : _playlistsRendered <
                                              _playlists.length +
                                                  _sharedPlaylists.length
                                          ? PlaylistDetails(
                                              _sharedPlaylists[
                                                          _playlistsRendered -
                                                              _playlists.length]
                                                      .tracks
                                                      .isEmpty
                                                  ? kDefualtPlaylistImage
                                                  : _sharedPlaylists[
                                                          _playlistsRendered -
                                                              _playlists.length]
                                                      .tracks[0]
                                                      .getImage,
                                              _sharedPlaylists[
                                                      _playlistsRendered -
                                                          _playlists.length]
                                                  .id,
                                              _sharedPlaylists[
                                                      _playlistsRendered++ -
                                                          _playlists.length]
                                                  .name,
                                              true,
                                              _partyMode)
                                          : SizedBox.shrink(),
                                  _playlistsRendered < _playlists.length
                                      ? PlaylistDetails(
                                          _playlists[_playlistsRendered]
                                                  .tracks
                                                  .isEmpty
                                              ? kDefualtPlaylistImage
                                              : _playlists[_playlistsRendered]
                                                  .tracks[0]
                                                  .getImage,
                                          _playlists[_playlistsRendered].id,
                                          _playlists[_playlistsRendered++].name,
                                          false,
                                          _partyMode)
                                      : _playlistsRendered <
                                              _playlists.length +
                                                  _sharedPlaylists.length
                                          ? PlaylistDetails(
                                              _sharedPlaylists[
                                                          _playlistsRendered -
                                                              _playlists.length]
                                                      .tracks
                                                      .isEmpty
                                                  ? kDefualtPlaylistImage
                                                  : _sharedPlaylists[
                                                          _playlistsRendered -
                                                              _playlists.length]
                                                      .tracks[0]
                                                      .getImage,
                                              _sharedPlaylists[
                                                      _playlistsRendered -
                                                          _playlists.length]
                                                  .id,
                                              _sharedPlaylists[
                                                      _playlistsRendered++ -
                                                          _playlists.length]
                                                  .name,
                                              true,
                                              _partyMode)
                                          : SizedBox.shrink()
                                ],
                              );
                            else {
                              return Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  index == 0
                                      ? AddNewPlaylistWidget(false)
                                      : _playlistsRendered < _playlists.length
                                          ? PlaylistDetails(
                                              _playlists[_playlistsRendered]
                                                      .tracks
                                                      .isEmpty
                                                  ? kDefualtPlaylistImage
                                                  : _playlists[_playlistsRendered]
                                                      .tracks[0]
                                                      .getImage,
                                              _playlists[_playlistsRendered].id,
                                              _playlists[_playlistsRendered++]
                                                  .name,
                                              false,
                                              _partyMode)
                                          : _playlistsRendered <
                                                  _playlists.length +
                                                      _sharedPlaylists.length
                                              ? PlaylistDetails(
                                                  _sharedPlaylists[_playlistsRendered - _playlists.length]
                                                          .tracks
                                                          .isEmpty
                                                      ? kDefualtPlaylistImage
                                                      : _sharedPlaylists[_playlistsRendered -
                                                              _playlists.length]
                                                          .tracks[0]
                                                          .getImage,
                                                  _sharedPlaylists[
                                                          _playlistsRendered -
                                                              _playlists.length]
                                                      .id,
                                                  _sharedPlaylists[
                                                          _playlistsRendered++ -
                                                              _playlists.length]
                                                      .name,
                                                  true,
                                                  _partyMode)
                                              : SizedBox.shrink(),
                                  _playlistsRendered < _playlists.length
                                      ? PlaylistDetails(
                                          _playlists[_playlistsRendered]
                                                  .tracks
                                                  .isEmpty
                                              ? kDefualtPlaylistImage
                                              : _playlists[_playlistsRendered]
                                                  .tracks[0]
                                                  .getImage,
                                          _playlists[_playlistsRendered].id,
                                          _playlists[_playlistsRendered++].name,
                                          false,
                                          _partyMode)
                                      : _playlistsRendered <
                                              _playlists.length +
                                                  _sharedPlaylists.length
                                          ? PlaylistDetails(
                                              _sharedPlaylists[
                                                          _playlistsRendered -
                                                              _playlists.length]
                                                      .tracks
                                                      .isEmpty
                                                  ? kDefualtPlaylistImage
                                                  : _sharedPlaylists[
                                                          _playlistsRendered -
                                                              _playlists.length]
                                                      .tracks[0]
                                                      .getImage,
                                              _sharedPlaylists[
                                                      _playlistsRendered -
                                                          _playlists.length]
                                                  .id,
                                              _sharedPlaylists[
                                                      _playlistsRendered++ -
                                                          _playlists.length]
                                                  .name,
                                              true,
                                              _partyMode)
                                          : FittedBox(
                                              child: Container(
                                                height: 10,
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.25,
                                                margin:
                                                    EdgeInsets.only(left: 10),
                                              ),
                                            ),
                                  _playlistsRendered < _playlists.length
                                      ? PlaylistDetails(
                                          _playlists[_playlistsRendered]
                                                  .tracks
                                                  .isEmpty
                                              ? kDefualtPlaylistImage
                                              : _playlists[_playlistsRendered]
                                                  .tracks[0]
                                                  .getImage,
                                          _playlists[_playlistsRendered].id,
                                          _playlists[_playlistsRendered++].name,
                                          false,
                                          _partyMode)
                                      : _playlistsRendered <
                                              _playlists.length +
                                                  _sharedPlaylists.length
                                          ? PlaylistDetails(
                                              _sharedPlaylists[
                                                          _playlistsRendered -
                                                              _playlists.length]
                                                      .tracks
                                                      .isEmpty
                                                  ? kDefualtPlaylistImage
                                                  : _sharedPlaylists[
                                                          _playlistsRendered -
                                                              _playlists.length]
                                                      .tracks[0]
                                                      .getImage,
                                              _sharedPlaylists[
                                                      _playlistsRendered -
                                                          _playlists.length]
                                                  .id,
                                              _sharedPlaylists[
                                                      _playlistsRendered++ -
                                                          _playlists.length]
                                                  .name,
                                              true,
                                              _partyMode)
                                          : FittedBox(
                                              child: Container(
                                                height: 10,
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.25,
                                                margin:
                                                    EdgeInsets.only(left: 10),
                                              ),
                                            ),
                                ],
                              );
                            }
                          },
                        ),
                      ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
