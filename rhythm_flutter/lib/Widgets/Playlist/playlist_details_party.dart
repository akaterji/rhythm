import 'package:flutter/material.dart';
import 'package:rhythm_flutter/Pages/track_list_page.dart';
import 'package:rhythm_flutter/Widgets/Playlist/PopupMenuButtonCustomPlaylist.dart';
import 'package:rhythm_flutter/Widgets/Playlist/PopupMenuButtonCustomSharedPlaylist.dart';

class PlaylistDetailsParty extends StatelessWidget {
  final _imageURL;
  final _playlistName;
  final _playlistId;
  final _partyMode;
  final _isShared;
  PlaylistDetailsParty(this._imageURL, this._playlistId, this._playlistName,
      this._isShared, this._partyMode);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.25,
      width: MediaQuery.of(context).size.width * 0.27,
      margin: EdgeInsets.only(left: 5),
      child: Column(
        children: <Widget>[
          InkWell(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: FadeInImage.assetNetwork(
                placeholder: 'assets/colorful_loader.gif',
                image: _imageURL,
                height: MediaQuery.of(context).size.height * 0.15,
              ),
            ),
            onTap: () {
              Navigator.of(context).pushNamed(
                TrackListPage.route,
                arguments: {
                  'id': _playlistId,
                  'name': _playlistName,
                  'image': _imageURL,
                  'routingFrom': 'playlist',
                  'partyMode': _partyMode
                },
              );
            },
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.4,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: 45,
                  child: Text(
                    _playlistName,
                    textAlign: TextAlign.left,
                    style: TextStyle(fontSize: 10),
                  ),
                ),
                Spacer(),
                _isShared
                    ? PopupMenuButtonCustomSharedPlaylist(_playlistId)
                    : PopupMenuButtonCustomPlaylist(_playlistId)
              ],
            ),
          ),
        ],
      ),
    );
  }
}
