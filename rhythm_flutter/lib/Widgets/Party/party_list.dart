import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rhythm_flutter/Providers/party_member.dart';
import '../Playlist/playlist_widget_party.dart';
import '../../Providers/party_lobby.dart';
import '../../Models/party.dart';

import './partyDetails.dart';

class PartyList extends StatefulWidget {
  @override
  _PartyListState createState() => _PartyListState();
}

class _PartyListState extends State<PartyList> {
  var _isInit = false;
  var _added = false;

  PartyLobby _partyProvider;
  PartyMember _partyMemberProvider;
  List<Party> _parties;
  @override
  void didChangeDependencies() {
    if (!_isInit) {
      _partyProvider = Provider.of<PartyLobby>(context);
      _partyMemberProvider = Provider.of<PartyMember>(context);
      _partyProvider.init();
      _isInit = true;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    _parties = _partyProvider.parties;
    var _shouldIncrease = false;
    var deviceSize = MediaQuery.of(context).size;
    var _idOfPlaying;
    Party _partyPlaying;
    if (_partyMemberProvider.enteredParty) {
      _parties.forEach((party) {
        if (party.userId == _partyMemberProvider.ownerId) {
          _idOfPlaying = party.userId;
          _partyPlaying = party;
        }
      });
    } else {
      _idOfPlaying = null;
    }
    return _partyProvider.loading
        ? Align(
            alignment: Alignment.center,
            child: Container(
              height: deviceSize.height * 0.77,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Fetching Parties',
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.redAccent),
                  )
                ],
              ),
            ),
          )
        : _parties.length == 0
            ? Align(
                alignment: Alignment.center,
                child: Container(
                  height: deviceSize.height * 0.77,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'There are no available parties',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 20),
                      ),
                      Container(
                        child: RaisedButton(
                          color: Colors.redAccent,
                          child: Text(
                            'Create a new party',
                            style: TextStyle(fontSize: 17),
                          ),
                          onPressed: () {
                            Navigator.of(context).pushNamed(
                              PlaylistWidgetParty.route,
                              arguments: {'isParty': true},
                            );
                          },
                        ),
                      )
                    ],
                  ),
                ),
              )
            : _partyMemberProvider.enteredParty
                ? _partyMemberProvider.isLoading
                    ? Center(
                        child: Text('Loading'),
                      )
                    : _parties.length != 1
                        ? Container(
                            height: deviceSize.height,
                            child: ListView(
                              shrinkWrap: true,
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.topCenter,
                                  child: Container(
                                    height: deviceSize.height * 0.8,
                                    margin: EdgeInsets.only(top: 10),
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Container(
                                              margin: EdgeInsets.only(left: 10),
                                              child: Text(
                                                'Currently Listening To',
                                                style: TextStyle(
                                                    fontSize: 30,
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          height: deviceSize.height * 0.68,
                                          width: deviceSize.width * 0.83,
                                          margin: EdgeInsets.only(top: 10),
                                          decoration: BoxDecoration(
                                            color: Colors.redAccent,
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(15.0),
                                            ),
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
                                                child: PartyDetails(
                                                    _partyPlaying.username,
                                                    _partyPlaying.userId,
                                                    _partyPlaying.id,
                                                    _partyPlaying.imageURL,
                                                    _partyPlaying.name,
                                                    _partyPlaying.artist,
                                                    _partyPlaying.position,
                                                    _partyPlaying.duration,
                                                    _partyPlaying.isPlaying,
                                                    true),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    margin: EdgeInsets.only(left: 10),
                                    child: Text(
                                      'Join Other Parties',
                                      style: TextStyle(
                                          fontSize: 30,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ),
                                Container(
                                  child: Column(
                                    children: (_parties.length - 1) % 2 == 0
                                        ? List.generate(
                                            (_parties.length - 1 / 2).toInt(),
                                            (index) {
                                              return Row(
                                                children: List.generate(
                                                  2,
                                                  (indexOfRow) {
                                                    if (index * 2 <
                                                        (_parties.length - 1)) {
                                                      if (_parties[(index * 2) +
                                                                  indexOfRow]
                                                              .userId ==
                                                          _idOfPlaying) {
                                                        _shouldIncrease = true;

                                                        return PartyWidget(
                                                          (index * 2) +
                                                              indexOfRow +
                                                              1,
                                                        );
                                                      } else {
                                                        return _shouldIncrease
                                                            ? PartyWidget(
                                                                (index * 2 +
                                                                        1) +
                                                                    indexOfRow,
                                                              )
                                                            : PartyWidget(
                                                                (index * 2) +
                                                                    indexOfRow,
                                                              );
                                                      }
                                                    } else
                                                      return SizedBox.shrink();
                                                  },
                                                ),
                                              );
                                            },
                                          )
                                        : List.generate(
                                            (_parties.length - 1 / 2).ceil(),
                                            (index) {
                                              return Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: List.generate(2,
                                                    (indexOfRow) {
                                                  if (index * 2 + indexOfRow <
                                                      _parties.length - 1) {
                                                    if (_parties[(index * 2) +
                                                                indexOfRow]
                                                            .userId ==
                                                        _idOfPlaying) {
                                                      _shouldIncrease = true;
                                                      return PartyWidget(
                                                        (index * 2) +
                                                            indexOfRow +
                                                            1,
                                                      );
                                                    } else {
                                                      return _shouldIncrease
                                                          ? PartyWidget(
                                                              (index * 2 + 1) +
                                                                  indexOfRow,
                                                            )
                                                          : PartyWidget(
                                                              (index * 2) +
                                                                  indexOfRow,
                                                            );
                                                    }
                                                  } else {
                                                    return SizedBox.shrink();
                                                  }
                                                }),
                                              );
                                            },
                                          ),
                                  ),
                                ),
                                SizedBox(
                                  height: 140,
                                )
                              ],
                            ),
                          )
                        : Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              height: deviceSize.height * 0.8,
                              margin: EdgeInsets.only(top: 10),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    child: Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: Text(
                                          'Currently Listening To',
                                          style: TextStyle(
                                              fontSize: 30,
                                              fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: deviceSize.height * 0.68,
                                    width: deviceSize.width * 0.83,
                                    margin: EdgeInsets.only(top: 10),
                                    decoration: BoxDecoration(
                                      color: Colors.redAccent,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(15.0),
                                      ),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          child: PartyDetails(
                                              _partyPlaying.username,
                                              _partyPlaying.userId,
                                              _partyPlaying.id,
                                              _partyPlaying.imageURL,
                                              _partyPlaying.name,
                                              _partyPlaying.artist,
                                              _partyPlaying.position,
                                              _partyPlaying.duration,
                                              _partyPlaying.isPlaying,
                                              true),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                : Container(
                    width: deviceSize.width,
                    // height: deviceSize.height * 0.77,
                    child: Column(
                      children: _parties.length % 2 == 0
                          ? List.generate(
                              (_parties.length / 2).toInt(),
                              (index) {
                                return index == 0
                                    ? Container(
                                        child: Column(
                                          children: <Widget>[
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Container(
                                                margin: EdgeInsets.only(
                                                    left: 10, top: 10),
                                                child: Text(
                                                  'Join your friends\' parties',
                                                  style: TextStyle(
                                                      fontSize: 30,
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                              ),
                                            ),
                                            Row(
                                              children: <Widget>[
                                                PartyDetails(
                                                  _parties[index * 2].username,
                                                  _parties[index * 2].userId,
                                                  _parties[index * 2].id,
                                                  _parties[index * 2].imageURL,
                                                  _parties[index * 2].name,
                                                  _parties[index * 2].artist,
                                                  _parties[index * 2].position,
                                                  _parties[index * 2].duration,
                                                  _parties[index * 2].isPlaying,
                                                  false,
                                                ),
                                                PartyDetails(
                                                    _parties[index * 2 + 1]
                                                        .username,
                                                    _parties[index * 2 + 1]
                                                        .userId,
                                                    _parties[index * 2 + 1].id,
                                                    _parties[index * 2 + 1]
                                                        .imageURL,
                                                    _parties[index * 2 + 1]
                                                        .name,
                                                    _parties[index * 2 + 1]
                                                        .artist,
                                                    _parties[index * 2 + 1]
                                                        .position,
                                                    _parties[index * 2 + 1]
                                                        .duration,
                                                    _parties[index * 2 + 1]
                                                        .isPlaying,
                                                    false),
                                              ],
                                            )
                                          ],
                                        ),
                                      )
                                    : Row(
                                        children: <Widget>[
                                          PartyDetails(
                                            _parties[index * 2].username,
                                            _parties[index * 2].userId,
                                            _parties[index * 2].id,
                                            _parties[index * 2].imageURL,
                                            _parties[index * 2].name,
                                            _parties[index * 2].artist,
                                            _parties[index * 2].position,
                                            _parties[index * 2].duration,
                                            _parties[index * 2].isPlaying,
                                            false,
                                          ),
                                          PartyDetails(
                                              _parties[index * 2 + 1].username,
                                              _parties[index * 2 + 1].userId,
                                              _parties[index * 2 + 1].id,
                                              _parties[index * 2 + 1].imageURL,
                                              _parties[index * 2 + 1].name,
                                              _parties[index * 2 + 1].artist,
                                              _parties[index * 2 + 1].position,
                                              _parties[index * 2 + 1].duration,
                                              _parties[index * 2 + 1].isPlaying,
                                              false),
                                        ],
                                      );
                              },
                            )
                          : List.generate(
                              (_parties.length / 2).ceil(),
                              (index) {
                                return index == 0
                                    ? Container(
                                        child: Column(
                                          children: <Widget>[
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Container(
                                                margin: EdgeInsets.only(
                                                    left: 10, top: 10),
                                                child: Text(
                                                  'Join your friends\' parties',
                                                  style: TextStyle(
                                                      fontSize: 30,
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                              ),
                                            ),
                                            Row(
                                              children: <Widget>[
                                                index * 2 + 1 < _parties.length
                                                    ? PartyDetails(
                                                        _parties[index * 2]
                                                            .username,
                                                        _parties[index * 2]
                                                            .userId,
                                                        _parties[index * 2].id,
                                                        _parties[index * 2]
                                                            .imageURL,
                                                        _parties[index * 2]
                                                            .name,
                                                        _parties[index * 2]
                                                            .artist,
                                                        _parties[index * 2]
                                                            .position,
                                                        _parties[index * 2]
                                                            .duration,
                                                        _parties[index * 2]
                                                            .isPlaying,
                                                        false,
                                                      )
                                                    : Align(
                                                        alignment:
                                                            Alignment.topCenter,
                                                        child: Container(
                                                          width:
                                                              deviceSize.width,
                                                          child: PartyDetails(
                                                            _parties[index * 2]
                                                                .username,
                                                            _parties[index * 2]
                                                                .userId,
                                                            _parties[index * 2]
                                                                .id,
                                                            _parties[index * 2]
                                                                .imageURL,
                                                            _parties[index * 2]
                                                                .name,
                                                            _parties[index * 2]
                                                                .artist,
                                                            _parties[index * 2]
                                                                .position,
                                                            _parties[index * 2]
                                                                .duration,
                                                            _parties[index * 2]
                                                                .isPlaying,
                                                            false,
                                                          ),
                                                        ),
                                                      ),
                                                index * 2 + 1 < _parties.length
                                                    ? PartyDetails(
                                                        _parties[index * 2 + 1]
                                                            .username,
                                                        _parties[index * 2 + 1]
                                                            .userId,
                                                        _parties[index * 2 + 1]
                                                            .id,
                                                        _parties[index * 2 + 1]
                                                            .imageURL,
                                                        _parties[index * 2 + 1]
                                                            .name,
                                                        _parties[index * 2 + 1]
                                                            .artist,
                                                        _parties[index * 2 + 1]
                                                            .position,
                                                        _parties[index * 2 + 1]
                                                            .duration,
                                                        _parties[index * 2 + 1]
                                                            .isPlaying,
                                                        false)
                                                    : SizedBox.shrink(),
                                              ],
                                            )
                                          ],
                                        ),
                                      )
                                    : Container(
                                        child: Row(
                                          mainAxisAlignment:
                                              index * 2 + 1 >= _parties.length
                                                  ? MainAxisAlignment.center
                                                  : MainAxisAlignment.start,
                                          children: <Widget>[
                                            PartyDetails(
                                                _parties[index * 2].username,
                                                _parties[index * 2].userId,
                                                _parties[index * 2].id,
                                                _parties[index * 2].imageURL,
                                                _parties[index * 2].name,
                                                _parties[index * 2].artist,
                                                _parties[index * 2].position,
                                                _parties[index * 2].duration,
                                                _parties[index * 2].isPlaying,
                                                false),
                                            index * 2 + 1 >= _parties.length
                                                ? SizedBox.shrink()
                                                : PartyDetails(
                                                    _parties[index * 2 + 1]
                                                        .username,
                                                    _parties[index * 2 + 1]
                                                        .userId,
                                                    _parties[index * 2 + 1].id,
                                                    _parties[index * 2 + 1]
                                                        .imageURL,
                                                    _parties[index * 2 + 1]
                                                        .name,
                                                    _parties[index * 2 + 1]
                                                        .artist,
                                                    _parties[index * 2 + 1]
                                                        .position,
                                                    _parties[index * 2 + 1]
                                                        .duration,
                                                    _parties[index * 2 + 1]
                                                        .isPlaying,
                                                    false),
                                          ],
                                        ),
                                      );
                              },
                            ),
                    ),
                  );
  }

  Widget PartyWidget(int index) {
    return PartyDetails(
        _parties[index].username,
        _parties[index].userId,
        _parties[index].id,
        _parties[index].imageURL,
        _parties[index].name,
        _parties[index].artist,
        _parties[index].position,
        _parties[index].duration,
        _parties[index].isPlaying,
        false);
  }
}
