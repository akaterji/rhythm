import './track.dart';

class Album {
  final String _id;
  final String _name;
  final String _artistName;
  final List<Track> _trackList;
  final String _imageURL;

  Album(this._id, this._name, this._artistName, this._trackList,
      [this._imageURL]);

  String get id => _id;
  String get name => _name;
  String get artistName => _artistName;
  List<Track> get trackList => _trackList;
  String get imageURL => _imageURL;
}
