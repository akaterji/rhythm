import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../Providers/party_member.dart';

class VolumeSelector extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _partyMemberProvider = Provider.of<PartyMember>(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        RotatedBox(
          quarterTurns: 3,
          child: Container(
            color: Colors.grey[200],
            width: 150,
            child: Slider(
              value: _partyMemberProvider.getVolume,
              min: 0,
              max: 1,
              onChanged: (value) {
                _partyMemberProvider.changeVolume(value);
              },
            ),
          ),
        ),
      ],
    );
  }
}
