import 'package:flutter/material.dart';
import 'package:rhythm_flutter/Models/track.dart';
import 'package:rhythm_flutter/Pages/music_playback_page.dart';
import 'package:rhythm_flutter/Pages/search_friends_page.dart';
import 'package:rhythm_flutter/Providers/profile.dart';
import 'package:provider/provider.dart';
import 'package:rhythm_flutter/Widgets/Mini_Player/mini_player.dart';
import 'package:rhythm_flutter/customBottomSheet.dart';

class SocialTab extends StatefulWidget {
  @override
  _SocialTabState createState() => _SocialTabState();
}

class _SocialTabState extends State<SocialTab> {
  var _isInit = true;
  Profile _profileProvider;
  @override
  void didChangeDependencies() {
    if (_isInit) {
      _profileProvider = Provider.of<Profile>(context);
      _profileProvider.initFirebase();
      _isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var _deviceInfo = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text('Social'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              Navigator.of(context).pushNamed(SearchFriendsPage.route);
            },
          ),
          _profileProvider.notifications.length > 0
              ? Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 2),
                      child: IconButton(
                        onPressed: () {
                          showModalBottomSheetApp(
                            context: context,
                            dismissOnTap: true,
                            builder: (BuildContext context2) =>
                                notificationBottomSheet(context2),
                          );
                        },
                        icon: Icon(Icons.notifications),
                      ),
                    ),
                    Positioned(
                      right: 11,
                      top: 11,
                      child: Container(
                        padding: EdgeInsets.all(2),
                        decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(6),
                        ),
                        constraints: BoxConstraints(
                          minWidth: 14,
                          minHeight: 14,
                        ),
                        child: Text(
                          _profileProvider.notifications.length.toString(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 8,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  ],
                )
              : IconButton(
                  onPressed: () {
                    showModalBottomSheetApp(
                      context: context,
                      dismissOnTap: true,
                      builder: (BuildContext context2) =>
                          notificationBottomSheet(context2),
                    );
                  },
                  icon: Icon(Icons.notifications),
                ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            height: _deviceInfo.height,
            child: ListView(
              children: <Widget>[
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    margin: EdgeInsets.only(top: 15),
                    height: _deviceInfo.height * 0.25,
                    width: _deviceInfo.width * 0.4,
                    child: ClipOval(
                      child: FadeInImage.assetNetwork(
                        image: _profileProvider.userImage,
                        placeholder: 'assets/colorful_loader.gif',
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    margin: EdgeInsets.only(top: 15),
                    height: _deviceInfo.height * 0.1,
                    child: Text(_profileProvider.username),
                  ),
                ),
                Divider(),
                Column(
                  children:
                      _profileProvider.recentlyPlayed.map((recentlyPlayed) {
                    return recentlyPlayedWidget(recentlyPlayed);
                  }).toList(),
                ),
              ],
            ),
          ),
          MiniPlayer()
        ],
      ),
    );
  }

  Widget recentlyPlayedWidget(Map<dynamic, dynamic> recentlyPlayed) {
    var _deviceInfo = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(top: 10, left: 5),
      height: _deviceInfo.height * 0.1,
      child: InkWell(
        onTap: () {
          Track _track = recentlyPlayed['track'];
          Navigator.pushNamed(
            context,
            MusicPlaybackPage.route,
            arguments: {
              'index': _profileProvider.recentlyPlayedTracks().indexOf(_track),
              'id': _track.getId,
              'name': _track.getName,
              'album': _track.getAlbum,
              'artist': _track.getArtist,
              'url': _track.getURL,
              'image': _track.getImage,
              'trackList': _profileProvider.recentlyPlayedTracks(),
              'currentlyPlaying': 'recently_played'
            },
          );
        },
        child: Row(
          children: <Widget>[
            Container(
              height: _deviceInfo.height * 0.06,
              width: _deviceInfo.width * 0.1,
              child: ClipOval(
                child: FadeInImage.assetNetwork(
                  image: recentlyPlayed['friend'].image,
                  placeholder: 'assets/colorful_loader.gif',
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(left: 5),
                child: Text(recentlyPlayed['friend'].firstName +
                    ' played ' +
                    recentlyPlayed['track'].getName +
                    ' by ' +
                    recentlyPlayed['track'].getArtist +
                    ' ' +
                    recentlyPlayed['playedAt']),
              ),
            ),
          ],
        ),
      ),
    );
  }

  notificationBottomSheet(BuildContext ctx) {
    Profile _profileNotificationProvider = Provider.of<Profile>(ctx);
    var deviceInfo = MediaQuery.of(ctx).size;
    return Container(
      height: deviceInfo.height * 0.5,
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Container(
            height: deviceInfo.height * 0.1,
            child: Center(
              child: Text('Notifications'),
            ),
          ),
          Divider(),
          _profileNotificationProvider.notifications.isEmpty
              ? Container(
                  margin: EdgeInsets.only(top: 30),
                  child: Center(
                    child: Text(
                      'No new notifications.',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                )
              : Column(
                  children: _profileNotificationProvider.notifications
                      .map(
                        (notification) => notificationWidget(
                          ctx,
                          notification['name'],
                          notification['userId'],
                          notification['image'],
                        ),
                      )
                      .toList(),
                )
        ],
      ),
    );
  }

  Widget notificationWidget(
      BuildContext ctx, String name, String id, String image) {
    Profile _profileNotificationWidgetProvider = Provider.of<Profile>(ctx);
    var deviceInfo = MediaQuery.of(ctx).size;
    return Container(
      height: deviceInfo.height * 0.15,
      child: Row(
        children: <Widget>[
          Container(
            height: deviceInfo.height * 0.07,
            width: deviceInfo.width * 0.11,
            child: ClipOval(
              child: FadeInImage.assetNetwork(
                image: image,
                placeholder: 'assets/colorful_loader.gif',
                fit: BoxFit.fill,
              ),
            ),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 5),
              child: Text(
                ' $name wants to add you',
                style: TextStyle(fontSize: 12),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 10),
            width: 70,
            child: RaisedButton(
              color: Colors.blue,
              child: Text(
                'Accept',
                style: TextStyle(fontSize: 12),
              ),
              onPressed: () {
                _profileNotificationWidgetProvider.acceptFriendRequest(id);
              },
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 10),
            width: 70,
            child: RaisedButton(
              color: Colors.red,
              child: Text(
                'Reject',
                style: TextStyle(fontSize: 12),
              ),
              onPressed: () {
                _profileNotificationWidgetProvider
                    .removeFriendRequestFromNotifications(id);
              },
            ),
          ),
        ],
      ),
    );
  }
}
