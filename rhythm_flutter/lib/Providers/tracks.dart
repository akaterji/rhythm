import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

import '../Models/track.dart';
import '../constants.dart';

class Tracks with ChangeNotifier {
  var _isError = false;
  var _fetched = false;
  final _token;
  List<Track> _trackList;
  final List<Track> _loadedTracks = [];
  Tracks(this._token, this._trackList);
  final String url = kUrl;
  var _loading = true;
  List<Track> get trackList => _trackList;
  List<Track> _favoriteTracks = [];
  Future<void> fetchTracks() async {
    if (!_fetched) {
      fetch(url + 'tracks', (response) {
        _trackList = [];
        var decodedJson = json.decode(response.body);
        decodedJson.forEach((item) {
          _loadedTracks.add(
            Track(
              item['id'].toString(),
              item['name'],
              item['album']['name'],
              item['artist']['name'],
              item['url'],
              item['image'],
              item['isFavorite'],
            ),
          );
        });
        _trackList = _loadedTracks;
        _fetched = true;
        notifyListeners();
      });
    }
  }

  Future<void> fetch(url, callback) async {
    try {
      if (!_loading) {
        _loading = true;
        notifyListeners();
      }
      final response =
          await http.get(url, headers: {'authorization': 'Bearer $_token'});
      if (response.statusCode != 200) {
        throw json.decode(response.body)['message'];
      }
      callback(response);
      _loading = false;
      // notifyListeners();
    } catch (error) {
      kError = true;
      key.currentState.removeCurrentSnackBar();
      key.currentState.showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          content: Text('Something went wrong'),
          action: SnackBarAction(
            label: 'Retry',
            onPressed: () {
              notifyListeners();
            },
          ),
        ),
      );
    }
  }

  Future<void> post(url, body, callback) async {
    _loading = true;
    notifyListeners();
    try {
      final response = await http.post(url,
          headers: {'authorization': 'Bearer $_token'}, body: body);
      if (response.statusCode != 200)
        throw json.decode(response.body)['message'];
      callback(response);
      _loading = false;
      notifyListeners();
    } catch (error) {
      kError = true;
      kErrorMessage = error;
      notifyListeners();
    }
  }

  void toggleLike(String trackId, bool liked) {
    if (liked) {
      post(url + 'tracks/removeFromFavorites', {'track_id': trackId},
          (response) {});
      _trackList.forEach((track) {
        if (track.getId == trackId) {
          track.toggleFavorite();
        }
      });
      _favoriteTracks =
          _favoriteTracks.where((track) => track.getId != trackId).toList();
      notifyListeners();
    } else {
      post(url + 'tracks/addToFavorites', {'track_id': trackId}, (response) {});
      _trackList.forEach((track) {
        if (track.getId == trackId) {
          track.toggleFavorite();
          _favoriteTracks.add(track);
          notifyListeners();
        }
      });
    }
  }

  bool isFavorite(String trackId) {
    bool favorite;
    _trackList.forEach((track) {
      if (track.getId == trackId) {
        favorite = track.favorite;
      }
    });
    return favorite;
  }

  void fetchFavorites() {
    fetch(url + 'tracks/getFavorites', (response) {
      _favoriteTracks = [];
      var _newTracks = json.decode(response.body)['tracks'];
      _newTracks.forEach(
        (track) {
          _favoriteTracks.add(
            Track(
              track['id'].toString(),
              track['name'],
              track['artist']['name'],
              track['album']['name'],
              track['url'],
              track['image'],
              track['isFavorite'],
            ),
          );
        },
      );
    });
  }

  bool get isError => _isError;
  bool get loading => _loading;
  List<Track> get favoriteTracks => _favoriteTracks;
}
