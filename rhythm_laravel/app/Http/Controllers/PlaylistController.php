<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Playlist;
use App\User;
use App\Track;

class PlaylistController extends Controller
{

    //takes a request with playlist id and returns all the tracks of that playlist
    public function index(Request $request)
    {
        if (!$request->has('playlist_id')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        $playlist = Playlist::find($request->playlist_id);
        if (!$playlist) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        $tracks = $playlist->tracks;
        $checkedTracks = $this->checkFavoriteTracks($tracks);
        foreach ($checkedTracks as $track) {
            $track->artist;
            $track->album;
        }
        return response()->json($checkedTracks, 200);
    }

    //takes a request with the user id of the logged in user and the name of the new playlist
    public function store(Request $request)
    {
        if (!$request->has('user_id')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        if (!$request->has('name')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        $playlist = new Playlist;
        $playlist->name = $request->name;
        $playlist->user_id = $user->id;
        $created = $playlist->save();
        if (!$created) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        return response()->json($playlist, 200);
    }

    //takes a request of the logged in user and returns all the playlists of that user
    public function getPlaylists(Request $request)
    {
        $playlists = Auth::user()->playlists;
        foreach ($playlists as $index=>$playlist) {
            $playlists[$index]->tracks = $playlist->tracks;
            $checkedTracks = $this->checkFavoriteTracks($playlist->tracks);
            foreach ($playlist->tracks as $track) {
                $track->artist;
                $track->album;
            }
        }
        
        return response()->json($playlists);
    }
    //takes a request of user id, playlist id, and track id
    public function addToPlaylist(Request $request)
    {
        if (!$request->has('user_id')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        if (!$request->has('playlist_id')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        if (!$request->has('track_id')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }

        $playlist = Playlist::find($request->playlist_id);
        if (!$playlist) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        $track = Track::find($request->track_id);
        if (!$track) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        $playlist->tracks()->attach($track->id);
        foreach ($playlist->tracks as $track) {
            $track->artist;
            $track->album;
        }
        return response()->json(['message'=>'added', 'new_playlist'=>$playlist, 'playlist_tracks'=>$this->checkFavoriteTracks($playlist->tracks)], 200);
    }
    public function addMultipleTracksToPlaylist(Request $request)
    {
        if (!$request->has('user_id')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        if (!$request->has('playlist_id')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        if (!$request->has('trackIds')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        $playlist = Playlist::find($request->playlist_id);
        $trackIds = json_decode($request->trackIds);
        foreach ($trackIds as $id) {
            $playlist->tracks()->attach($id);
        }
        return response()->json(['message'=>'added', 'updated_playlist'=>$playlist, 'playlist_tracks'=>$playlist->tracks], 200);
    }
    public function deletePlaylist(Request $request)
    {
        if (!$request->has('user_id')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        if (!$request->has('playlist_id')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        $playlist = Playlist::find($request->playlist_id);
        $playlist->delete();
    }
    public function removeFromPlaylist(Request $request)
    {
        if (!$request->has('playlist_id')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        if (!$request->has('track_id')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }

        $playlist = Playlist::find($request->playlist_id);
        if (!$playlist) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        $track = Track::find($request->track_id);
        if (!$track) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        $playlist->tracks()->detach($track->id);
        return response()->json(['message'=>'removed'], 200);
    }
    public function sharedWith(Request $request)
    {
        if (!$request->has('playlist_id')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        $playlist=Playlist::find($request->playlist_id);
        if (!$playlist) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        return $playlist->sharedWith()->get();
    }
    public function friendsNotSharedWith(Request $request)
    {
        if (!$request->has('playlist_id')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        $user = Auth::user();
        $playlist = Playlist::find($request->playlist_id);
        if (!$playlist) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        $users = $playlist->sharedWith()->get();
        $friends = $user->friends()->get();
        $temp = [];
        foreach ($friends as $friend) {
            $isShared = false;
            foreach ($users as $user) {
                if ($user->id == $friend->id) {
                    $isShared = true;
                }
            }
            if (!$isShared) {
                array_push($temp, $friend);
            }
        }
        return $temp;
    }
    private function friendsNotSharedWith2(Playlist $playlist)
    {
        $users = $playlist->sharedWith()->get();
        $friends = Auth::user()->friends()->get();
        $temp = [];
        foreach ($friends as $friend) {
            $isShared = false;
            foreach ($users as $user) {
                if ($user->id == $friend->id) {
                    $isShared = true;
                }
            }
            if (!$isShared) {
                array_push($temp, $friend);
            }
        }
        return $temp;
    }
    public function sharedPlaylists(Request $request)
    {
        $playlists = Auth::user()->sharedPlaylists()->get();
        foreach ($playlists as $index=>$playlist) {
            $playlist->user->id;
            $playlists[$index]->tracks=$playlist->tracks()->get();
            foreach ($playlists[$index]->tracks as $track) {
                $track->artist;
                $track->album;
            }
        }
        return $playlists;
    }
    public function addSharedPlaylist(Request $request)
    {
        if (!$request->has('user_id') || !$request->has('playlist_id')) {
            return response()->json(['message'=>'Unauthorized'], 405);
        }
        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(['message'=>'Unauthorized'], 405);
        }
        
        $playlist = Playlist::find($request->playlist_id);
        if (!$playlist) {
            return response()->json(['message'=>'Unauthorized'], 405);
        }
        if ($playlist->user->id != Auth::id()) {
            return response()->json(['message'=>'Unauthorized'], 405);
        }
        $isFriend = $this->checkIfFriend($request->user_id);
        if (!$isFriend) {
            return response()->json(['message'=>'Unauthorized22'], 405);
        }
        $user->sharedPlaylists()->attach($playlist->id);
        return response()->json(['message'=>'shared'], 200);
    }
    public function removeSharedPlaylist(Request $request)
    {
        if (!$request->has('user_id') || !$request->has('playlist_id')) {
            return response()->json(['message'=>'Unauthorized'], 405);
        }
        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(['message'=>'Unauthorized'], 405);
        }
        
        $playlist = Playlist::find($request->playlist_id);
        if (!$playlist) {
            return response()->json(['message'=>'Unauthorized'], 405);
        }
        $user->sharedPlaylists()->detach($playlist->id);
        return response()->json(['message'=>'removed'], 200);
    }
    private function checkIfFriend($id)
    {
        $user = Auth::user();
        $friends = $user->friends()->get();
        foreach ($friends as $friend) {
            if ($friend->id == $id) {
                return true;
            }
        }
        return false;
    }
    private function checkFavoriteTracks($tracks)
    {
        $favorite_tracks = Auth::user()->favorites()->get();
        if (count($favorite_tracks) != 0) {
            foreach ($tracks as $index=>$track) {
                $isFavorite = false;
                foreach ($favorite_tracks as $favorite_track) {
                    if ($track->id == $favorite_track->id) {
                        $isFavorite = true;
                    }
                }
                $isFavorite ? $tracks[$index]->isFavorite = true : $tracks[$index]->isFavorite = false;
            }
        } else {
            foreach ($tracks as $track) {
                $track->isFavorite = false;
            }
        }
        return $tracks;
    }
    public function getPlaylistsWithSharedInfo()
    {
        $playlists = Auth::user()->playlists;
        foreach ($playlists as $index=>$playlist) {
            $sharedWith = $playlist->sharedWith()->get();
            $playlists[$index]->sharedWith=$sharedWith;
            $notSharedWith = $this->friendsNotSharedWith2($playlist);
            $playlists[$index]->notSharedWith=$notSharedWith;
        }
        return $playlists;
    }
}
