import 'package:flutter/material.dart';
import 'package:rhythm_flutter/CustomBottomSheets/add_playlist_sheet.dart';
import 'package:rhythm_flutter/Providers/playlists.dart';

import '../../constants.dart';
import '../../customBottomSheet.dart';
import './repeat_icon_widget.dart';
import './shuffle_icon_widget.dart';
import './track_progress_slider.dart';
import './track_progress_at.dart';
import './track_progress_left.dart';
import './pause_play_icon.dart';
import './play_next_icon.dart';
import './play_prev_icon.dart';
import '../../Providers/track_player.dart';
import '../../Providers/tracks.dart';
import 'package:provider/provider.dart';
import 'package:transparent_image/transparent_image.dart';

class TrackPlayerWidget extends StatefulWidget {
  final _imageURL;
  TrackPlayerWidget(this._imageURL);

  @override
  _TrackPlayerWidgetState createState() => _TrackPlayerWidgetState();
}

class _TrackPlayerWidgetState extends State<TrackPlayerWidget> {
  @override
  Widget build(BuildContext context) {
    var _trackProvider = Provider.of<TrackPlayer>(context);
    var _tracksProvider = Provider.of<Tracks>(context);
    var _trackId;
    if (_trackProvider.isSet) _trackId = _trackProvider.trackId;
    return Container(
      child: Column(
        children: <Widget>[
          Align(
            child: Container(
              margin: EdgeInsets.only(top: 15, bottom: 35),
              height: MediaQuery.of(context).size.height * 0.45,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: FadeInImage.memoryNetwork(
                    placeholder: kTransparentImage,
                    image: _trackProvider.image == ''
                        ? widget._imageURL
                        : _trackProvider.image,
                    fit: BoxFit.fill),
              ),
            ),
          ),
          _trackProvider.isSet
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    _tracksProvider.isFavorite(_trackId)
                        ? Expanded(
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                margin: EdgeInsets.only(left: 15),
                                width: MediaQuery.of(context).size.width * 0.2,
                                child: IconButton(
                                  icon: Icon(
                                    Icons.favorite,
                                    size: 30,
                                  ),
                                  onPressed: () {
                                    _tracksProvider.toggleLike(_trackId, true);
                                  },
                                ),
                              ),
                            ),
                          )
                        : Expanded(
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                margin: EdgeInsets.only(left: 15),
                                width: MediaQuery.of(context).size.width * 0.2,
                                child: IconButton(
                                  icon: Icon(
                                    Icons.favorite_border,
                                    size: 30,
                                  ),
                                  onPressed: () {
                                    _tracksProvider.toggleLike(_trackId, false);
                                  },
                                ),
                              ),
                            ),
                          ),
                    Column(
                      children: <Widget>[
                        Text(_trackProvider.getName),
                        Text(_trackProvider.artist +
                            ' - ' +
                            _trackProvider.album),
                      ],
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          margin: EdgeInsets.only(right: 15),
                          width: MediaQuery.of(context).size.width * 0.2,
                          child: IconButton(
                            icon: Icon(
                              Icons.playlist_add,
                              size: 30,
                            ),
                            onPressed: () {
                              openPlaylistsSheet(context);
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 48,
                      height: 48,
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                        strokeWidth: 1.5,
                      ),
                    ),
                  ],
                ),
          TrackProgressSlider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 10),
                child: TrackProgressAt(),
              ),
              Container(
                margin: EdgeInsets.only(right: 10),
                child: TrackProgressLeft(),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: RepeatIcon(),
                margin: EdgeInsets.only(left: 10),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.45,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    PlayPrevIcon(),
                    PausePlayIcon(),
                    PlayNextIcon(),
                  ],
                ),
              ),
              Container(
                child: ShuffleIcon(),
                margin: EdgeInsets.only(right: 10),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget addPlaylists() {
    return InkWell(
      child: Align(
        alignment: Alignment.centerLeft,
        child: Container(
          margin: EdgeInsets.only(left: 10, top: 15),
          child: Row(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    color: Colors.black26,
                    border: Border.all(color: Colors.grey)),
                height: 75,
                width: 75,
                child: Icon(
                  Icons.add,
                  size: 30,
                  color: Colors.grey,
                ),
              ),
              Container(
                child: Text('New playlist'),
                width: 200,
                margin: EdgeInsets.only(left: 10),
              ),
            ],
          ),
        ),
      ),
      onTap: () {
        showModalBottomSheetApp(
          dismissOnTap: true,
          context: context,
          builder: (BuildContext context) {
            return AddPlaylistSheet();
          },
        );
      },
    );
  }

  Widget playlists(String id, String name, String imageURL, int numOfTracks) {
    return InkWell(
      child: Align(
        alignment: Alignment.centerLeft,
        child: Container(
          margin: EdgeInsets.only(left: 10, top: 15),
          height: 75,
          child: Row(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: FadeInImage.assetNetwork(
                    placeholder: 'assets/colorful_loader.gif',
                    image: imageURL,
                    height: 75,
                    width: 75),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        width: 200,
                        child: Text(name),
                        margin: EdgeInsets.only(left: 10),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                      width: 200,
                      child: Text(
                        ' ' + numOfTracks.toString() + ' songs',
                        style: TextStyle(color: Colors.grey),
                      ),
                      margin: EdgeInsets.only(left: 10)),
                ],
              ),
            ],
          ),
        ),
      ),
      onTap: () {
        var _trackprovider = Provider.of<TrackPlayer>(context);
        var _exists = Provider.of<Playlists>(context)
            .addTrack(id, _trackprovider.trackId);
        if (!_exists) {
          Navigator.of(context).pop();
          Scaffold.of(context).removeCurrentSnackBar();
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Added to ' + name),
          ));
        } else {
          Navigator.of(context).pop();
          Scaffold.of(context).removeCurrentSnackBar();
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text('This song is already in this playlist.'),
            ),
          );
        }
      },
    );
  }

  void openPlaylistsSheet(BuildContext context) {
    showModalBottomSheetApp(
      dismissOnTap: true,
      context: context,
      builder: (BuildContext ctx) {
        final _playlistsProvider2 = Provider.of<Playlists>(ctx);
        var _playlistsLength = _playlistsProvider2.playlists.length;
        var _playlists = _playlistsProvider2.playlists;
        return Container(
          height: MediaQuery.of(context).size.height * 0.5,
          child: Column(
            children: <Widget>[
              Container(
                child: Text('Choose a playlist'),
                margin: EdgeInsets.only(top: 15),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.4,
                child: ListView(
                  children: List.generate(
                    _playlistsLength + 1,
                    (index) {
                      return index == 0
                          ? addPlaylists()
                          : playlists(
                              _playlists[index - 1].id,
                              _playlists[index - 1].name,
                              _playlists[index - 1].tracks.length == 0
                                  ? kDefualtPlaylistImage
                                  : _playlists[index - 1].tracks[0].getImage,
                              _playlists[index - 1].tracks.length);
                    },
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
