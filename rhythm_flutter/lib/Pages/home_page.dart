import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rhythm_flutter/Providers/profile.dart';
import 'package:rhythm_flutter/Providers/recently_played.dart';
import '../Providers/tracks.dart';
import '../Providers/playlists.dart';
import '../Providers/albums.dart';
import '../Tabs/home_tab.dart';
import '../Tabs/my_music_tab.dart';
import '../Tabs/search_tab.dart';
import '../Tabs/social_tab.dart';
import '../Tabs/party_tab.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import '../constants.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class HomePage extends StatefulWidget {
  static const route = '/home';
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var _currentIndex = 0;
  List<Widget> _children = [
    HomeTab(),
    SearchTab(),
    MyMusicTab(),
    PartyTab(),
    SocialTab()
  ];
  var _isInit = true;
  var _fetched = false;

  @override
  void didChangeDependencies() {
    if (kError) {
      kError = false;
      final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
      _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print("onMessage: $message");
          //  _showItemDialog(message);
        },
        onLaunch: (Map<String, dynamic> message) async {
          setState(() {
            _currentIndex = 4;
          });
          print("onLaunch: $message");
          //  _navigateToItemDetail(message);
        },
        onResume: (Map<String, dynamic> message) async {
          print("onResume: $message");
          //  _navigateToItemDetail(message);
        },
      );

      final routeArgs =
          ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
      routeArgs != null
          ? setState(() {
              _currentIndex = routeArgs['index'];
            })
          : fetch(context);
    }
    if (_isInit) {
      final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
      _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print("onMessage: $message");
        },
        onLaunch: (Map<String, dynamic> message) async {
          setState(() {
            _currentIndex = 4;
          });
          print("onLaunch: $message");
        },
        onResume: (Map<String, dynamic> message) async {
          print("onResume: $message");
          Navigator.of(context).pushNamedAndRemoveUntil(
              HomePage.route, (Route<dynamic> route) => false,
              arguments: {'index': 4});
        },
      );

      final routeArgs =
          ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
      if (routeArgs != null)
        setState(() {
          _currentIndex = routeArgs['index'];
        });

      if (!_fetched) {
        fetch(context);
        _fetched = true;
      }
      _isInit = false;
    }
    super.didChangeDependencies();
  }

  void fetch(BuildContext context) {
    Tracks _trackProvider = Provider.of<Tracks>(context);
    Playlists _playlistsProvider = Provider.of<Playlists>(context);
    Albums _albumsProvider = Provider.of<Albums>(context);
    RecentlyPlayed _recentlyPlayedProvider =
        Provider.of<RecentlyPlayed>(context);
    Profile _profileProvider = Provider.of<Profile>(context);
    _albumsProvider.fetchAlbums();
    _recentlyPlayedProvider.fetchRecentlyPlayed();
    _trackProvider.fetchTracks();
    _trackProvider.fetchFavorites();
    _playlistsProvider.fetchPlaylists();
    _playlistsProvider.fetchSharedPlaylists();
    _playlistsProvider.getFriends();
    _playlistsProvider.sharedWith();
    _profileProvider.getRecentlyPlayed();
  }

  @override
  Widget build(BuildContext context) {
    key = GlobalKey<ScaffoldState>();
    return Scaffold(
      key: key,
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.grey[800],
        selectedItemColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        items: [
          _currentIndex == 0
              ? BottomNavigationBarItem(
                  icon: Icon(MdiIcons.home),
                  title: Text('Home'),
                )
              : BottomNavigationBarItem(
                  icon: Icon(MdiIcons.homeOutline),
                  title: Text('Home'),
                ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            title: Text('Search'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.queue_music),
            title: Text('My Music'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.music_note),
            title: Text('Party'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Social'),
          ),
        ],
        onTap: (int index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
      body: _children[_currentIndex],
    );
  }
}
