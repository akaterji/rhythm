import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/widgets.dart';
import '../constants.dart';

import '../Providers/playlist.dart';
import '../Models/track.dart';
import '../Models/user.dart';

class Playlists with ChangeNotifier {
  String _token;
  List<Playlist> _playlists = [];
  List<Playlist> _sharedPlaylists;
  var _fetched = false;
  String _userId;
  List<Map<dynamic, dynamic>> _playlistsSharedWithList = [];
  List<Map<dynamic, dynamic>> _playlistsNotSharedWithList = [];
  List<User> _friends;
  var _isShared;

  final String url = kUrl;
  var x = 0;
  Playlists(this._token, this._playlists, this._userId);

  Future<void> fetchPlaylists() async {
    await fetch(url + 'playlists', (response) {
      _playlists = [];
      var responseBody = json.decode(response.body);
      if (responseBody.isNotEmpty) {
        responseBody.forEach((playlist) {
          List<Track> _trackList;
          if (playlist['tracks'].isEmpty)
            _trackList = [];
          else
            _trackList = listOfTracks(playlist['tracks']);
          _playlists.add(
            Playlist(
                id: playlist['id'].toString(),
                name: playlist['name'],
                tracks: _trackList),
          );
        });
        // notifyListeners();
      }
    });
  }

  Future<void> fetch(url, callback) async {
    try {
      final response =
          await http.get(url, headers: {'authorization': 'Bearer $_token'});
      if (response.statusCode != 200)
        throw json.decode(response.body)['message'];
      callback(response);
    } catch (error) {
      kError = true;
      key.currentState.removeCurrentSnackBar();
      key.currentState.showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          content: Text('Something went wrong'),
          action: SnackBarAction(
            label: 'Retry',
            onPressed: () {
              notifyListeners();
            },
          ),
        ),
      );
    }
  }

  Future<void> post(url, body, callback) async {
    try {
      final response = await http.post(url,
          headers: {'authorization': 'Bearer $_token'}, body: body);
      if (response.statusCode != 200)
        throw json.decode(response.body)['message'];
      callback(response);
    } catch (error) {
      kError = true;
      key.currentState.removeCurrentSnackBar();
      key.currentState.showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          content: Text('Something went wrong'),
          action: SnackBarAction(
            label: 'Retry',
            onPressed: () {
              notifyListeners();
            },
          ),
        ),
      );
    }
  }

  void addPlaylist(String name) {
    post(
      url + 'playlist',
      {'name': name, 'user_id': _userId},
      (response) {
        var decodedBody = json.decode(response.body);
        List<Track> _emptyPlaylist = [];
        _playlists.add(Playlist(
            id: decodedBody['id'].toString(),
            name: decodedBody['name'],
            tracks: _emptyPlaylist));
        _playlistsNotSharedWithList.add({
          'id': decodedBody['id'].toString(),
          'notSharedWith': [..._friends]
        });
        _playlistsSharedWithList
            .add({'id': decodedBody['id'].toString(), 'sharedWith': []});
        notifyListeners();
      },
    );
  }

  List<Playlist> getPlaylistsWithoutTrack(String trackId) {
    List<Playlist> _playlistsWithoutTrack = [];
    _playlists.forEach((playlist) {
      var _tracks = playlist.tracks;
      var _trackFound = false;
      _tracks.forEach((track) {
        if (track.getId == trackId) {
          _trackFound = true;
        }
      });
      if (!_trackFound) {
        _playlistsWithoutTrack.add(playlist);
      }
    });
    return _playlistsWithoutTrack;
  }

  void deletePlaylist(String id) {
    post(url + 'playlists/delete', {'user_id': _userId, 'playlist_id': id},
        (response) {
      _playlists = _playlists.where((playlist) => playlist.id != id).toList();
      notifyListeners();
    });
  }

  List<Playlist> get playlists => _playlists;
  bool addTrack(String playlistId, String trackId) {
    var _exists = false;
    Playlist _playlist;
    _playlists.forEach((playlist) {
      if (playlist.id == playlistId) {
        return _playlist = playlist;
      }
    });
    if (_playlist != null) {
      _playlist.tracks.forEach((track) {
        if (track.getId == trackId) return _exists = true;
      });
      if (!_exists) {
        post(
          url + 'playlist/add',
          {
            'user_id': _userId,
            'track_id': trackId,
            'playlist_id': _playlist.id
          },
          (response) {
            var decodedRes = json.decode(response.body);
            List<Track> _newTrackList =
                listOfTracks(decodedRes['playlist_tracks']);
            final String playlistId =
                decodedRes['new_playlist']['id'].toString();
            final String playlistName = decodedRes['new_playlist']['name'];
            var _newPlaylist = Playlist(
                id: playlistId, name: playlistName, tracks: _newTrackList);
            List<Playlist> _newList = [];
            _playlists.forEach(
              (playlist) {
                if (playlist.id == _newPlaylist.id.toString()) {
                  _newList.add(_newPlaylist);
                } else {
                  _newList.add(playlist);
                }
              },
            );
            _playlists = _newList;
            _exists = false;
            // notifyListeners();
          },
        );
      }
    }
    return _exists;
  }

  void fetchSharedPlaylists() {
    _sharedPlaylists = [];
    fetch(url + 'playlist/sharedPlaylists', (response) {
      var responseBody = json.decode(response.body);
      if (responseBody.isNotEmpty) {
        responseBody.forEach((playlist) {
          List<Track> _trackList =
              listOfTracks(playlist['tracks'] as List<dynamic>);
          _sharedPlaylists.add(
            Playlist(
                id: playlist['id'].toString(),
                name: playlist['name'],
                tracks: _trackList,
                sharedFriendName: playlist['user']['firstname']),
          );
        });
        // notifyListeners();
      }
    });
  }

  void removeTrack(String playlistId, String trackId) {
    Playlist _playlist;
    _playlists.forEach((playlist) {
      if (playlist.id == playlistId) return _playlist = playlist;
    });
    if (_playlist != null) {
      post(kUrl + 'playlist/delete',
          {'playlist_id': playlistId, 'track_id': trackId}, () {});
      _playlist.remove(trackId);
      List<Playlist> _updatedList = [];
      _playlists.forEach((playlist) {
        if (playlist.id == playlistId)
          _updatedList.add(_playlist);
        else
          _updatedList.add(playlist);
      });
      _playlists = _updatedList;
      notifyListeners();
    }
  }

  List<Track> listOfTracks(List<dynamic> data) {
    List<Track> _trackList = [];
    List<dynamic> _tracks = data;
    _tracks.forEach((track) {
      _trackList.add(
        Track(
          track['id'].toString(),
          track['name'],
          track['artist']['name'],
          track['album']['name'],
          track['url'],
          track['image'],
          track['isFavorite'],
        ),
      );
    });
    return _trackList;
  }

  List<Track> getTracks(String id) {
    List<Track> _playlistTracks = [];
    _playlists.forEach((playlist) {
      if (playlist.id == id) return _playlistTracks = playlist.tracks;
    });
    if (_playlistTracks.isEmpty)
      _sharedPlaylists.forEach((playlist) {
        if (playlist.id == id) return _playlistTracks = playlist.tracks;
      });
    return _playlistTracks;
  }

  void removeSharedPlaylist(String playlistId) {
    _sharedPlaylists = _sharedPlaylists
        .where((playlist) => playlist.id != playlistId)
        .toList();
    notifyListeners();
  }

  void sharedWith() {
    if (!_fetched) {
      fetch(
        url + 'playlists/sharedPlaylists',
        (response) {
          _playlistsSharedWithList = [];
          _playlistsNotSharedWithList = [];
          var result = json.decode(response.body);

          result.forEach(
            (playlist) {
              var friendsSharedWith = playlist['sharedWith'];
              if (friendsSharedWith.isNotEmpty) {
                _playlistsSharedWithList
                    .add({'id': playlist['id'].toString(), 'sharedWith': []});
                friendsSharedWith.forEach(
                  (friend) {
                    _playlistsSharedWithList[
                            _playlistsSharedWithList.length - 1]['sharedWith']
                        .add(
                      User(
                        friend['id'].toString(),
                        friend['firstname'],
                        friend['lastname'],
                        friend['image'],
                        true,
                        friend['recently_played'],
                        friend['recently_played_at'],
                      ),
                    );
                  },
                );
              }
              var friendsNotSharedWith = playlist['notSharedWith'];
              if (friendsNotSharedWith.isNotEmpty) {
                _playlistsNotSharedWithList.add(
                    {'id': playlist['id'].toString(), 'notSharedWith': []});
                friendsNotSharedWith.forEach(
                  (friend) {
                    _playlistsNotSharedWithList[
                                _playlistsNotSharedWithList.length - 1]
                            ['notSharedWith']
                        .add(
                      User(
                        friend['id'].toString(),
                        friend['firstname'],
                        friend['lastname'],
                        friend['image'],
                        true,
                        friend['recently_played'],
                        friend['recently_played_at'],
                      ),
                    );
                  },
                );
              }
            },
          );
        },
      );
      // notifyListeners();
      _fetched = true;
    }
  }

  List<dynamic> playlistsSharedWithList(String playlistId) {
    List<dynamic> _list;
    var _found = false;
    _playlistsSharedWithList.forEach((playlist) {
      if (playlist['id'] == playlistId) {
        _list = playlist['sharedWith'];
        _found = true;
      }
    });
    if (_found)
      return _list;
    else
      return [];
  }

  List<dynamic> playlistsNotSharedWithList(String playlistId) {
    List<dynamic> _list;
    var _found = false;
    _playlistsNotSharedWithList.forEach((playlist) {
      if (playlist['id'] == playlistId) {
        _list = playlist['notSharedWith'];

        _found = true;
      }
    });

    if (_found)
      return _list;
    else
      return [];
  }

  void shareWith(String playlistId, String userId, List<dynamic> friends) {
    post(url + 'playlist/sharedPlaylists/add',
        {'user_id': userId, 'playlist_id': playlistId}, (response) {
      var _friend = friends.firstWhere((friend) => friend.id == userId);

      var _found = false;
      _playlistsSharedWithList.forEach((playlist) {
        if (playlist['id'] == playlistId) {
          playlist['sharedWith'].add(_friend);
          _found = true;
        }
      });
      if (!_found) {
        List<dynamic> _emptyList = [];
        _emptyList.add(_friend);
        _playlistsSharedWithList
            .add({'id': playlistId, 'sharedWith': _emptyList});
      }
      _playlistsNotSharedWithList.forEach((playlist) {
        if (playlist['id'] == playlistId) {
          var _newList = playlist['notSharedWith'];
          _newList = _newList.where((friend) => friend.id != userId).toList();
          playlist['notSharedWith'] = _newList;
        }
      });
      notifyListeners();
    });
  }

  void removeShareWith(
      String playlistId, String userId, List<dynamic> friends) {
    post(url + 'playlist/sharedPlaylists/remove',
        {'user_id': userId, 'playlist_id': playlistId}, (response) {
      var _friend = friends.firstWhere((friend) => friend.id == userId);

      var _found = false;
      _playlistsNotSharedWithList.forEach((playlist) {
        if (playlist['id'] == playlistId) {
          playlist['notSharedWith'].add(_friend);
          _found = true;
        }
      });
      if (!_found) {
        List<dynamic> _emptyList = [];
        _emptyList.add(_friend);
        _playlistsNotSharedWithList.add(
          {'id': playlistId, 'notSharedWith': _emptyList},
        );
      }

      _playlistsSharedWithList.forEach((playlist) {
        if (playlist['id'] == playlistId) {
          var _newList = playlist['sharedWith'];
          _newList = _newList.where((friend) => friend.id != userId).toList();
          playlist['sharedWith'] = _newList;
        }
      });
      notifyListeners();
    });
  }

  void getFriends() {
    fetch(url + 'users/friends?user_id=$_userId', (response) {
      _friends = [];
      var friends = json.decode(response.body)['users'];
      friends.forEach((friend) {
        _friends.add(
          User(
              friend['id'].toString(),
              friend['firstname'],
              friend['lastname'],
              friend['image'],
              true,
              friend['recently_played'],
              friend['recently_played_at']),
        );
      });
    });
  }

  void setTracks(String playlistId, List<Track> newTracks) {
    List<Playlist> _newPlaylists = [];
    _playlists.forEach((playlist) {
      if (playlist.id == playlistId) {
        List<Track> allTracks = [...playlist.tracks, ...newTracks];
        _newPlaylists.add(
            Playlist(id: playlist.id, name: playlist.name, tracks: allTracks));
      } else
        _newPlaylists.add(playlist);
    });
    _playlists = _newPlaylists;
    notifyListeners();
  }

  Playlist getPlaylist(String playlistId) {
    Playlist _playlist;
    _playlists.forEach((playlist) {
      if (playlist.id == playlistId) return _playlist = playlist;
    });
    return _playlist;
  }

  Playlist getSharedPlaylist(String playlistId) {
    Playlist _playlist;
    _sharedPlaylists.forEach((playlist) {
      if (playlist.id == playlistId) return _playlist = playlist;
    });
    return _playlist;
  }

  List<dynamic> friendsSharedWith(String playlistId) {
    List<dynamic> _newFriends = [];
    _playlistsSharedWithList.forEach((playlist) {
      if (playlist['id'] == playlistId) {
        return _newFriends = playlist['sharedWith'];
      }
    });
    return _newFriends;
  }

  List<dynamic> friendsNotSharedWith(String playlistId) {
    List<dynamic> _newFriends = [];
    _playlistsNotSharedWithList.forEach((playlist) {
      if (playlist['id'] == playlistId) {
        return _newFriends = playlist['notSharedWith'];
      }
    });
    return _newFriends;
  }

  List<Playlist> allPlaylists() {
    return [..._playlists, ..._sharedPlaylists];
  }

  List<User> get friends => _friends;
  List<Playlist> get sharedPlaylists => _sharedPlaylists;
}
