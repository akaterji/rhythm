import 'package:flutter/material.dart';
import '../../Models/track.dart';
import '../Track_List/track_list_widget.dart';

class SearchWidget extends StatelessWidget {
  final List<Track> _trackList;

  SearchWidget(this._trackList);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TrackListWidget(_trackList, 'search', false),
    );
  }
}
