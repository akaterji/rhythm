import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rhythm_flutter/CustomBottomSheets/add_playlist_sheet.dart';
import 'package:rhythm_flutter/constants.dart';
import 'package:rhythm_flutter/customBottomSheet.dart';

import '../../Providers/playlists.dart';
import '../../Providers/playlist.dart';
import '../../Providers/tracks.dart';
import 'package:provider/provider.dart';

class PopupTracks extends StatefulWidget {
  final track_id;

  PopupTracks(this.track_id);

  @override
  _PopupTracksState createState() => _PopupTracksState();
}

class _PopupTracksState extends State<PopupTracks> {
  var _isInit = true;
  List<Playlist> _playlistsWithoutTrack;
  @override
  void didChangeDependencies() {
    if (_isInit) {}
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final _playlistsProvider = Provider.of<Playlists>(context);
    _playlistsWithoutTrack =
        _playlistsProvider.getPlaylistsWithoutTrack(widget.track_id);
    final _trackProvider = Provider.of<Tracks>(context);
    bool _isFavorite = _trackProvider.isFavorite(widget.track_id);
    List<Playlist> _playlists = _playlistsProvider.playlists;
    return Consumer<Playlists>(
      builder: (ctx, _playlistsProvider, _) => PopupMenuButton(
        itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
          PopupMenuItem<String>(
            value: '1',
            child: Row(
              children: <Widget>[
                Icon(Icons.playlist_add),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Add to playlist',
                )
              ],
            ),
          ),
          _isFavorite
              ? PopupMenuItem<String>(
                  value: '2',
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.favorite),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        'Liked',
                      )
                    ],
                  ),
                )
              : PopupMenuItem<String>(
                  value: '3',
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.favorite_border),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        'Like',
                      )
                    ],
                  ),
                ),
        ],
        onSelected: (value) {
          if (value == '1') {
            showModalBottomSheetApp(
              dismissOnTap: true,
              context: context,
              builder: (BuildContext ctx) {
                final _playlistsProvider2 = Provider.of<Playlists>(ctx);
                var _playlistsLength = _playlistsProvider2.playlists.length;
                _playlists = _playlistsProvider2.playlists;
                return Container(
                  height: MediaQuery.of(context).size.height * 0.5,
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Text('Choose a playlist'),
                        margin: EdgeInsets.only(top: 15),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.4,
                        child: ListView(
                          children: List.generate(
                            _playlistsLength + 1,
                            (index) {
                              return index == 0
                                  ? addPlaylists()
                                  : playlists(
                                      _playlists[index - 1].id,
                                      _playlists[index - 1].name,
                                      _playlists[index - 1].tracks.length == 0
                                          ? kDefualtPlaylistImage
                                          : _playlists[index - 1]
                                              .tracks[0]
                                              .getImage,
                                      _playlists[index - 1].tracks.length);
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            );
          } else if (value == '2') {
            _trackProvider.toggleLike(widget.track_id, true);
          } else if (value == '3') {
            _trackProvider.toggleLike(widget.track_id, false);
          }
        },
      ),
    );
  }

  Widget addPlaylists() {
    return InkWell(
      child: Align(
        alignment: Alignment.centerLeft,
        child: Container(
          margin: EdgeInsets.only(left: 10, top: 15),
          child: Row(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    color: Colors.black26,
                    border: Border.all(color: Colors.grey)),
                height: 75,
                width: 75,
                child: Icon(
                  Icons.add,
                  size: 30,
                  color: Colors.grey,
                ),
              ),
              Container(
                child: Text('New playlist'),
                width: 200,
                margin: EdgeInsets.only(left: 10),
              ),
            ],
          ),
        ),
      ),
      onTap: () {
        showModalBottomSheetApp(
          dismissOnTap: true,
          context: context,
          builder: (BuildContext context) {
            return AddPlaylistSheet();
          },
        );
      },
    );
  }

  Widget playlists(String id, String name, String imageURL, int numOfTracks) {
    return InkWell(
      child: Align(
        alignment: Alignment.centerLeft,
        child: Container(
          margin: EdgeInsets.only(left: 10, top: 15),
          height: 75,
          child: Row(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: FadeInImage.assetNetwork(
                    placeholder: 'assets/colorful_loader.gif',
                    image: imageURL,
                    height: 75,
                    width: 75),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        width: 200,
                        child: Text(name),
                        margin: EdgeInsets.only(left: 10),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                      width: 200,
                      child: Text(
                        ' ' + numOfTracks.toString() + ' songs',
                        style: TextStyle(color: Colors.grey),
                      ),
                      margin: EdgeInsets.only(left: 10)),
                ],
              ),
            ],
          ),
        ),
      ),
      onTap: () {
        var _exists =
            Provider.of<Playlists>(context).addTrack(id, widget.track_id);
        if (!_exists) {
          Navigator.of(context).pop();
          Scaffold.of(context).removeCurrentSnackBar();
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Added to ' + name),
          ));
        } else {
          Navigator.of(context).pop();
          Scaffold.of(context).removeCurrentSnackBar();
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text('This song is already in this playlist.'),
            ),
          );
        }
      },
    );
  }
}
