import 'package:flutter/material.dart';
import 'package:rhythm_flutter/Providers/playlists.dart';
import 'package:rhythm_flutter/Widgets/Mini_Player/mini_player.dart';
import '../Providers/search_playlist.dart';
import 'package:provider/provider.dart';
import '../Widgets/Playlist/playlist_track_list_add.dart';
import '../constants.dart';

class PlaylistAddPage extends StatefulWidget {
  static const route = '/playlist_add';
  @override
  _PlaylistAddPageState createState() => _PlaylistAddPageState();
}

class _PlaylistAddPageState extends State<PlaylistAddPage> {
  final _searchController = TextEditingController();
  var _isInit = true;
  var _searchBy = 'name';
  @override
  void didChangeDependencies() {
    if (kError) {
      kError = false;
      final _searchPlaylistProvider =
          Provider.of<SearchPlaylistProvider>(context);
      _searchPlaylistProvider.reset();
      _searchController.addListener(() {
        String text = _searchController.text;
        if (text.length > 2)
          _searchPlaylistProvider.search(text, _searchBy);
        else
          _searchPlaylistProvider.empty();
      });
    }
    if (_isInit) {
      final _searchPlaylistProvider =
          Provider.of<SearchPlaylistProvider>(context);
      _searchPlaylistProvider.reset();
      _searchController.addListener(() {
        String text = _searchController.text;
        if (text.length > 2)
          _searchPlaylistProvider.search(text, _searchBy);
        else
          _searchPlaylistProvider.empty();
      });
      _isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var deviceInfo = MediaQuery.of(context);
    final _searchPlaylistProvider =
        Provider.of<SearchPlaylistProvider>(context);
    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    _searchPlaylistProvider.setPlaylistIdAndTracks(
        routeArgs['id'], routeArgs['tracks']);
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            routeArgs['name'],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('DONE'),
              onPressed: () {
                _searchPlaylistProvider.addTracks();
                Provider.of<Playlists>(context).setTracks(
                    routeArgs['id'], _searchPlaylistProvider.selectedTracks);
                Navigator.pop(context);
              },
            )
          ],
        ),
        body: Stack(
          children: <Widget>[
            Container(
              width: deviceInfo.size.width,
              height: deviceInfo.size.height,
              child: ListView(
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      height: 50,
                      margin: EdgeInsets.only(top: 10),
                      width: deviceInfo.size.width * 0.9,
                      child: Center(
                        child: TextField(
                          controller: _searchController,
                          decoration: InputDecoration(
                              fillColor: Colors.grey[700],
                              filled: true,
                              hintText: 'Search',
                              border: OutlineInputBorder()),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: deviceInfo.size.width,
                    height: 30,
                    child: Center(
                      child: Row(
                        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              setState(
                                () {
                                  if (_searchBy != 'name') _searchBy = 'name';
                                  if (_searchController.text.length > 2)
                                    _searchPlaylistProvider.search(
                                        _searchController.text, _searchBy);
                                },
                              );
                            },
                            child: _searchBy == 'name'
                                ? Container(
                                    decoration: BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                            color: Colors.white, width: 1.0),
                                      ),
                                    ),
                                    width: deviceInfo.size.width / 3,
                                    child: Center(
                                      child: Text('Songs'),
                                    ),
                                  )
                                : Container(
                                    width: deviceInfo.size.width / 3,
                                    child: Center(
                                      child: Text(
                                        'Songs',
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                  ),
                          ),
                          InkWell(
                            onTap: () {
                              setState(() {
                                if (_searchBy != 'album') _searchBy = 'album';
                                if (_searchController.text.length > 2)
                                  _searchPlaylistProvider.search(
                                      _searchController.text, _searchBy);
                              });
                            },
                            child: _searchBy == 'album'
                                ? Container(
                                    decoration: BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                            color: Colors.white, width: 1.0),
                                      ),
                                    ),
                                    width: deviceInfo.size.width / 3,
                                    child: Center(
                                      child: Text('Albums'),
                                    ),
                                  )
                                : Container(
                                    width: deviceInfo.size.width / 3,
                                    child: Center(
                                      child: Text(
                                        'Albums',
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                  ),
                          ),
                          InkWell(
                            onTap: () {
                              setState(() {
                                if (_searchBy != 'artist') _searchBy = 'artist';
                                if (_searchController.text.length > 2)
                                  _searchPlaylistProvider.search(
                                      _searchController.text, _searchBy);
                              });
                            },
                            child: _searchBy == 'artist'
                                ? Container(
                                    decoration: BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                            color: Colors.white, width: 1.0),
                                      ),
                                    ),
                                    width: deviceInfo.size.width / 3,
                                    child: Center(
                                      child: Text('Artists'),
                                    ),
                                  )
                                : Container(
                                    width: deviceInfo.size.width / 3,
                                    child: Center(
                                      child: Text(
                                        'Artists',
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                  ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  _searchPlaylistProvider.tracks == []
                      ? Spacer()
                      : PlaylistAdd(_searchPlaylistProvider.tracks)
                ],
              ),
            ),
            MiniPlayer()
          ],
        ),
      ),
      onWillPop: () {
        _searchPlaylistProvider.reset();
        Navigator.pop(context, false);
        return Future.value(false);
      },
    );
  }
}
