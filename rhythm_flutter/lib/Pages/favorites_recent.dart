import 'package:flutter/material.dart';
import 'package:rhythm_flutter/Models/track.dart';
import 'package:rhythm_flutter/Providers/recently_played.dart';
import 'package:rhythm_flutter/Providers/tracks.dart';
import 'package:rhythm_flutter/Widgets/Mini_Player/mini_player.dart';
import 'package:rhythm_flutter/Widgets/Track_List/track_list_widget.dart';
import 'package:provider/provider.dart';

class FavoritesRecentPage extends StatelessWidget {
  static const route = '/favorites_recent';
  @override
  Widget build(BuildContext context) {
    var args =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    String _routingFrom = args['routingFrom'];
    List<Track> _tracks;
    Tracks _tracksProvider = Provider.of<Tracks>(context);
    RecentlyPlayed _recentlyPlayedProvider =
        Provider.of<RecentlyPlayed>(context);
    _tracks = _routingFrom == 'favorites'
        ? _tracksProvider.favoriteTracks
        : _recentlyPlayedProvider.tracks;
    if (_routingFrom == 'favorites')
      _tracks = _tracksProvider.favoriteTracks;
    else {
      _tracks = _recentlyPlayedProvider.tracks;
    }
    return Scaffold(
      appBar: AppBar(
        title: _routingFrom == 'favorites'
            ? Text('Likes')
            : Text('Recently played'),
      ),
      body: Stack(
        children: <Widget>[
          TrackListWidget(_tracks, _routingFrom, false),
          MiniPlayer()
        ],
      ),
    );
  }
}
