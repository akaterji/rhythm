<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Track;
use App\Events\FriendRequestEvent;
use App\Playlist;
use Carbon;

class UserController extends Controller
{
    public function getFullName(Request $request)
    {
        if (!$request->has('user_id')) {
            return response()->json(['message'=>'Unauthorized'], 405);
        }
        $user=User::find($request->user_id);
        $fullname = $user->firstname.' '.$user->lastname;
        return response()->json(['name'=>$fullname,'image'=>$user->image], 200);
    }
    public function search(Request $request)
    {
        if (!$request->has('text')) {
            return response()->json(['message'=>'Unauthorized'], 405);
        }
        $users = User::select('*')
        ->where("firstname", "LIKE", "%{$request->text}%")
        ->orwhere("lastname", "LIKE", "%{$request->text}%")
        ->orWhereRaw("concat(firstname, ' ', lastname) like '%{$request->text}%' ")
        ->get();
        $friends = Auth::user()->friends()->get();
        foreach ($users as $user) {
            if (sizeOf($friends)!=0) {
                $isFriend = false;
                foreach ($friends as $friend) {
                    if ($friend->id == $user->id) {
                        $isFriend = true;
                    }
                    if ($isFriend) {
                        $user->isFriend = true;
                        $user->isFriendRequested = false;
                        $user->isFriendRequesting = false;
                    } else {
                        $user->isFriend = false;
                        $user->isFriendRequested = $this->isFriendRequested($user->id);
                        $user->isFriendRequesting = $this->isFriendRequesting($user->id);
                    }
                }
            } else {
                $user->isFriend = false;
                $user->isFriendRequested = $this->isFriendRequested($user->id);
                $user->isFriendRequesting = $this->isFriendRequesting($user->id);
            }
        }
        
        return response()->json(["users"=>$users], 200);
    }
    public function addFriend(Request $request)
    {
        if (!$request->has('user_id')) {
            return response()->json(['message'=>'Unauthorized'], 405);
        }
        if (!$request->has('friend_id')) {
            return response()->json(['message'=>'Unauthorized'], 405);
        }

        $user = User::find($request->user_id);
        $user->addFriend($request->friend_id);
        $user = User::find($request->friend_id);
        $user->addFriend($request->user_id);
        return response()->json(['message'=>'added'], 200);
    }
    public function removeFriend(Request $request)
    {
        if (!$request->has('user_id')) {
            return response()->haredjson(['message'=>'Unauthorized'], 405);
        }
        if (!$request->has('friend_id')) {
            return response()->json(['message'=>'Unauthorized'], 405);
        }
        $user = User::find($request->user_id);
        $user->removeFriend($request->friend_id);
        $user = User::find($request->friend_id);
        $user->removeFriend($request->user_id);
        return response()->json(['message'=>'removed'], 200);
    }
    public function getFriends(Request $request)
    {
        if (!$request->has('user_id')) {
            return response()->json(['message'=>'Unauthorized'], 405);
        }
        $user = User::find($request->user_id);
        return response()->json(['users'=>$user->friends()->get()], 200);
    }
    private function checkIfFriend($id)
    {
        $user = Auth::user();
        $friends = $user->friends()->get();
        foreach ($friends as $friend) {
            if ($friend->id == $id) {
                return true;
            }
        }
        return false;
    }
    private function isFriendRequested($id)
    {
        $friendRequests = Auth::user()->friendRequests()->get();
        
        foreach ($friendRequests as $friendRequest) {
            if ($friendRequest->id == $id) {
                return true;
            }
        }
        return false;
    }
    public function sendFriendRequest(Request $request)
    {
        if (!$request->has('friend_id')) {
            return response()->json(['message'=>'Unauthorized'], 405);
        }
        Auth::user()->addFriendRequest($request->friend_id);
        $user = User::find($request->friend_id);
        $auth_user = Auth::user();
        $name = $auth_user->firstname.' '.$auth_user->lastname;
        event(new FriendRequestEvent($name, $request->friend_id));
        return response()->json(['added'=>true], 200);
    }
    public function removeFriendRequest(Request $request)
    {
        if (!$request->has('friend_id')) {
            return response()->json(['message'=>'Unauthorized'], 405);
        }
        Auth::user()->deleteFriendRequest($request->friend_id);
        return response()->json(['removed'=>true], 200);
    }
    private function isFriendRequesting($id)
    {
        $friendRequests = Auth::user()->friendRequesting()->get();
        foreach ($friendRequests as $friendRequest) {
            if ($friendRequest->id == $id) {
                return true;
            }
        }
        return false;
    }
    public function removeFriendRequestAfterAccepting(Request $request)
    {
        if (!$request->has('friend_id')) {
            return response()->json(['message'=>'Unauthorized'], 405);
        }
        User::find($request->friend_id)->deleteFriendRequest(Auth::id());
        return response()->json(['removed'=>true], 200);
    }
    public function addRecentlyPlayed(Request $request)
    {
        if (!$request->has('track_id')) {
            return response()->json(['message'=>'Unauthorized'], 405);
        }
        $user = Auth::user();
        $user->recently_played=$request->track_id;
        $user->recently_played_at = Carbon\Carbon::now();
        $user->save();
        return response()->json(['message'=>'added'], 200);
    }
    public function getRecentlyPlayed()
    {
        $friends=Auth::user()->friends()->get();
        $recentlyPlayed = [];
        foreach ($friends as $friend) {
            if ($track= Track::find($friend->recently_played)) {
                $track = $this->checkIfFavorite($track);
                $track->artist;
                $track->album;
                
                array_push($recentlyPlayed, ['track'=>$track,'friend'=>$friend,'played_at'=>$friend->recently_played_at]);
            }
        }
        return $recentlyPlayed;
    }
    private function checkIfFavorite($track)
    {
        $favorite_tracks = Auth::user()->favorites()->get();
        if (count($favorite_tracks) != 0) {
            $found = false;
            foreach ($favorite_tracks as $favorite_track) {
                if ($track->id == $favorite_track->id) {
                    $track->isFavorite = true;
                    $found = true;
                    break;
                }
                
                if (!$found) {
                    $track->isFavorite = false;
                }
            }
        } else {
            $track->isFavorite = false;
        }
        return $track;
    }
    public function addNewRecentlyPlayed(Request $request)
    {
        $user = Auth::user();
        $tracks= $user->recentlyPlayed()->orderBy('recently_played.created_at', 'Desc')->get();
        if (count($tracks)==0) {
            $user->recentlyPlayed()->attach($request->track_id);
            $track = Track::find($request->track_id);
            $track = $this->checkIfFavorite($track);
            $track->artist;
            $track->album;
            return response()->json(['tracks'=>$track], 200);
        }
        foreach ($tracks as $track) {
            if ($track->id == $request->track_id) {
                $user->recentlyPlayed()->detach($request->track_id);
                $user->recentlyPlayed()->attach($request->track_id);
                $tracks= $user->recentlyPlayed()->orderBy('recently_played.created_at', 'Desc')->get();
                foreach ($tracks as $track) {
                    foreach ($tracks as $track) {
                        $track = $this->checkIfFavorite($track);
                        $track->artist;
                        $track->album;
                    }
                }
                return response()->json(['tracks'=>$tracks], 200);
            }
        }
        if (count($tracks)<10) {
            $user->recentlyPlayed()->attach($request->track_id);
            $tracks= $user->recentlyPlayed()->orderBy('recently_played.created_at', 'Desc')->get();
            foreach ($tracks as $track) {
                foreach ($tracks as $track) {
                    $track = $this->checkIfFavorite($track);
                    $track->artist;
                    $track->album;
                }
            }
            return response()->json(['tracks'=>$tracks], 200);
        }
        
        $toDetachTrack = $tracks[count($tracks)-1];
        $user->recentlyPlayed()->detach($toDetachTrack->id);
        $user->recentlyPlayed()->attach($request->track_id);
        $tracks= $user->recentlyPlayed()->orderBy('recently_played.created_at', 'DESC')->get();
        foreach ($tracks as $track) {
            $track = $this->checkIfFavorite($track);
            $track->artist;
            $track->album;
        }
        return response()->json(['tracks'=>$tracks], 200);
    }
    public function getAllRecentlyPlayed(Request $request)
    {
        $tracks= Auth::user()->recentlyPlayed()->orderByDesc('recently_played.created_at')->get();
        foreach ($tracks as $track) {
            $track = $this->checkIfFavorite($track);
            $track->artist;
            $track->album;
        }
        return response()->json(['tracks'=>$tracks], 200);
    }
}
