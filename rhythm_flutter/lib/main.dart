import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rhythm_flutter/Pages/favorites_recent.dart';
import 'package:rhythm_flutter/Pages/search_friends_page.dart';
import 'package:rhythm_flutter/Providers/party_playlist.dart';
import 'package:rhythm_flutter/Providers/recently_played.dart';
import 'package:rhythm_flutter/Widgets/Playlist/playlist_widget_party.dart';
import './Pages/tracks_playlist_page.dart';
import './Pages/auth_page.dart';
import './Pages/splash_page.dart';
import './Pages/music_playback_page.dart';
import './Pages/party_owner_music_playback_page.dart';
import './Pages/party_member_music_playback_page.dart';
import './Pages/home_page.dart';
import './Pages/playlist_add_tracks_page.dart';
import './Pages/track_list_page.dart';

import './Providers/auth.dart';
import './Providers/track_player.dart';
import './Providers/tracks.dart';
import './Providers/playlists.dart';
import './Providers/playlist.dart';
import './Providers/search.dart';
import './Providers/search_playlist.dart';
import './Providers/profile.dart';
import './Providers/party.dart';
import './Providers/albums.dart';
import './Providers/party_lobby.dart';
import './Providers/party_member.dart';
import './Providers/share_playlist_search.dart';

void main() => runApp(
      Rhythm(),
    );

class Rhythm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: Auth(),
        ),
        ChangeNotifierProvider.value(
          value: PartyMember(),
        ),
        ChangeNotifierProvider.value(
          value: PartyPlaylist(),
        ),
        ChangeNotifierProvider.value(
          value: SharePlaylistSearch(),
        ),
        ChangeNotifierProxyProvider<Auth, Tracks>(
          builder: (ctx, auth, previousTrackList) => Tracks(auth.token,
              previousTrackList == null ? [] : previousTrackList.trackList),
        ),
        ChangeNotifierProxyProvider<Auth, Albums>(
          builder: (ctx, auth, previousAlbums) =>
              Albums(auth.token, auth.userID),
        ),
        ChangeNotifierProxyProvider<Auth, Playlists>(
          builder: (ctx, auth, previousPlaylists) => Playlists(
            auth.token,
            previousPlaylists == null ? [] : previousPlaylists.playlists,
            auth.userID,
          ),
        ),
        ChangeNotifierProxyProvider<Auth, Playlist>(
          builder: (ctx, auth, previousPlaylist) =>
              Playlist(userId: auth.userID, token: auth.token),
        ),
        ChangeNotifierProxyProvider<Auth, SearchProvider>(
          builder: (ctx, auth, previousSearch) => SearchProvider(auth.token),
        ),
        ChangeNotifierProxyProvider<Auth, TrackPlayer>(
          builder: (ctx, auth, previousTrackPlayer) =>
              TrackPlayer(auth.token, auth.userID),
        ),
        ChangeNotifierProxyProvider<Auth, SearchPlaylistProvider>(
          builder: (ctx, auth, previousSearchPlaylist) =>
              SearchPlaylistProvider(auth.token, auth.userID),
        ),
        ChangeNotifierProxyProvider<Auth, Profile>(
          builder: (ctx, auth, previousProfile) =>
              Profile(auth.token, auth.userID, auth.name, auth.userImage),
        ),
        ChangeNotifierProxyProvider<Auth, Party>(
          builder: (ctx, auth, previousParty) => Party(auth.token, auth.userID),
        ),
        ChangeNotifierProxyProvider<Auth, PartyLobby>(
          builder: (ctx, auth, previousPartyLobby) =>
              PartyLobby(auth.token, auth.userID),
        ),
        ChangeNotifierProxyProvider<Auth, RecentlyPlayed>(
          builder: (ctx, auth, previousRecentlyPlayed) =>
              RecentlyPlayed(auth.token, auth.userID),
        ),
      ],
      child: Consumer<Auth>(
        builder: (ctx, auth, _) => MaterialApp(
          theme: ThemeData(
            brightness: Brightness.dark,
            primaryColor: Colors.black,
            appBarTheme: AppBarTheme(color: Colors.grey[600]),
            primaryTextTheme: TextTheme(
              title: TextStyle(color: Colors.white),
            ),
          ),
          title: 'Rhythm',
          home: auth.isAuth
              ? HomePage()
              : FutureBuilder(
                  future: auth.tryAutoLogin(),
                  builder: (ctx, authResultSnapshot) =>
                      authResultSnapshot.connectionState ==
                              ConnectionState.waiting
                          ? SplashScreen()
                          : LoginScreen3(),
                ),
          routes: {
            HomePage.route: (context) => HomePage(),
            MusicPlaybackPage.route: (context) => MusicPlaybackPage(),
            TracksPlaylistPage.route: (context) => TracksPlaylistPage(),
            PlaylistWidgetParty.route: (context) => PlaylistWidgetParty(),
            PlaylistAddPage.route: (context) => PlaylistAddPage(),
            PartyMusicPlaybackPage.route: (context) => PartyMusicPlaybackPage(),
            SearchFriendsPage.route: (context) => SearchFriendsPage(),
            PartyMemberMusicPlaybackPage.route: (context) =>
                PartyMemberMusicPlaybackPage(),
            TrackListPage.route: (context) => TrackListPage(),
            FavoritesRecentPage.route: (context) => FavoritesRecentPage(),
          },
        ),
      ),
    );
  }
}
