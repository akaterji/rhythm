<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Track;
use Illuminate\Support\Facades\Auth;
use App\Album;
use App\Artist;

class TrackController extends Controller
{
    public function index()
    {
        $tracks = Track::all();
        $this->checkFavoriteTracks($tracks);
        foreach ($tracks as $track) {
            $track->artist;
            $track->album;
        }
        return response()->json($tracks, 200);
    }
    public function search(Request $request)
    {
        if (!$request->has('searchby')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        
        $searchBy = $request->searchby;
        if ($searchBy != 'name' && $searchBy != 'album' && $searchBy != 'artist') {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        if ($searchBy == 'name') {
            $tracks = Track::select('id', 'name', 'artist_id', 'album_id', 'url', 'image')
        ->where("name", "LIKE", "%{$request->text}%")
        ->get();
            foreach ($tracks as $track) {
                $track->artist;
                $track->album;
            }
            return response()->json(["tracks"=>$tracks], 200);
        }
        if ($searchBy == 'album') {
            $albums = Album::where("name", "LIKE", "%{$request->text}%")
        ->get();
            $tracks = [];
            foreach ($albums as $album) {
                $albumTracks = $album->tracks()->get();
                foreach ($albumTracks as $albumTrack) {
                    $albumTrack->artist;
                    $albumTrack->album;
                    array_push($tracks, $albumTrack);
                }
            }
            return response()->json(["tracks"=>$this->checkFavoriteTracks($tracks)], 200);
        }
        if ($searchBy == 'artist') {
            $artists = Artist::where("name", "LIKE", "%{$request->text}%")
        ->get();
            $tracks = [];
            foreach ($artists as $artist) {
                $artistTracks = $artist->tracks()->get();
                foreach ($artistTracks as $artistTrack) {
                    $artistTrack->artist;
                    $artistTrack->album;
                    array_push($tracks, $artistTrack);
                }
            }
            return response()->json(["tracks"=>$this->checkFavoriteTracks($tracks)], 200);
        }
    }
    public function addToFavorites(Request $request)
    {
        if (!$request->has('track_id')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        Auth::user()->favorites()->attach($request->track_id);
        return response()->json(["message"=>'Added to favorites'], 200);
    }
    public function removeFromFavorites(Request $request)
    {
        if (!$request->has('track_id')) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }
        Auth::user()->favorites()->detach($request->track_id);
        return response()->json(["message"=>'Removed from favorites'], 200);
    }
    public function getFavorites()
    {
        // return response()->json(["tracks"=>Auth::user()->favorites()->get()], 200);
        $tracks = Auth::user()->favorites()->get();
        foreach ($tracks as $track) {
            $track->artist;
            $track->album;
        }
        return response()->json(["tracks"=>$tracks], 200);
    }
    private function checkFavoriteTracks($tracks)
    {
        $favorite_tracks = Auth::user()->favorites()->get();
        if (count($favorite_tracks) != 0) {
            foreach ($tracks as $index=>$track) {
                $isFavorite = false;
                foreach ($favorite_tracks as $favorite_track) {
                    if ($track->id == $favorite_track->id) {
                        $isFavorite = true;
                    }
                }
                $isFavorite ? $tracks[$index]->isFavorite = true : $tracks[$index]->isFavorite = false;
            }
        } else {
            foreach ($tracks as $track) {
                $track->isFavorite = false;
            }
        }
        return $tracks;
    }
}
