import 'package:flutter/material.dart';
import 'package:rhythm_flutter/Widgets/Mini_Player/mini_player.dart';
import '../Providers/search.dart';
import 'package:provider/provider.dart';
import '../Widgets/Search/search_widget.dart';
import '../constants.dart';

class SearchTab extends StatefulWidget {
  @override
  _SearchTabState createState() => _SearchTabState();
}

class _SearchTabState extends State<SearchTab> {
  final _searchController = TextEditingController();
  var _isInit = false;
  var _searchBy = 'name';
  @override
  void didChangeDependencies() {
    if (!_isInit) {
      final _searchProvider = Provider.of<SearchProvider>(context);
      _searchController.addListener(() {
        String text = _searchController.text;
        if (text.length > 2)
          _searchProvider.search(text, _searchBy);
        else
          _searchProvider.empty();
      });
      _isInit = true;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var deviceInfo = MediaQuery.of(context);
    return Consumer<SearchProvider>(
      builder: (ctx, _searchProvider, _) => Scaffold(
        appBar: AppBar(
          title: Text('Search'),
        ),
        body: Stack(
          children: <Widget>[
            Container(
              width: deviceInfo.size.width,
              height: deviceInfo.size.height,
              child: ListView(
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      height: 50,
                      margin: EdgeInsets.only(top: 10),
                      width: deviceInfo.size.width * 0.9,
                      child: Center(
                        child: TextField(
                          controller: _searchController,
                          decoration: InputDecoration(
                              fillColor: Colors.grey[700],
                              filled: true,
                              hintText: 'Search',
                              border: OutlineInputBorder()),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: deviceInfo.size.width,
                    height: 30,
                    child: Center(
                      child: Row(
                        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              setState(
                                () {
                                  if (_searchBy != 'name') _searchBy = 'name';
                                  if (_searchController.text.length > 2)
                                    _searchProvider.search(
                                        _searchController.text, _searchBy);
                                },
                              );
                            },
                            child: _searchBy == 'name'
                                ? Container(
                                    decoration: BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                            color: Colors.white, width: 1.0),
                                      ),
                                    ),
                                    width: deviceInfo.size.width / 3,
                                    child: Center(
                                      child: Text('Songs'),
                                    ),
                                  )
                                : Container(
                                    width: deviceInfo.size.width / 3,
                                    child: Center(
                                      child: Text(
                                        'Songs',
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                  ),
                          ),
                          InkWell(
                            onTap: () {
                              setState(
                                () {
                                  if (_searchBy != 'album') _searchBy = 'album';
                                  if (_searchController.text.length > 2)
                                    _searchProvider.search(
                                        _searchController.text, _searchBy);
                                },
                              );
                            },
                            child: _searchBy == 'album'
                                ? Container(
                                    decoration: BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                            color: Colors.white, width: 1.0),
                                      ),
                                    ),
                                    width: deviceInfo.size.width / 3,
                                    child: Center(
                                      child: Text('Albums'),
                                    ),
                                  )
                                : Container(
                                    width: deviceInfo.size.width / 3,
                                    child: Center(
                                      child: Text(
                                        'Albums',
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                  ),
                          ),
                          InkWell(
                            onTap: () {
                              setState(() {
                                if (_searchBy != 'artist') _searchBy = 'artist';
                                if (_searchController.text.length > 2)
                                  _searchProvider.search(
                                      _searchController.text, _searchBy);
                              });
                            },
                            child: _searchBy == 'artist'
                                ? Container(
                                    decoration: BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                            color: Colors.white, width: 1.0),
                                      ),
                                    ),
                                    width: deviceInfo.size.width / 3,
                                    child: Center(
                                      child: Text('Artists'),
                                    ),
                                  )
                                : Container(
                                    width: deviceInfo.size.width / 3,
                                    child: Center(
                                      child: Text(
                                        'Artists',
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                  ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  _searchProvider.tracks == []
                      ? Spacer()
                      : SearchWidget(_searchProvider.tracks)
                ],
              ),
            ),
            MiniPlayer()
          ],
        ),
      ),
    );
  }
}
