import 'package:flutter/material.dart';
import 'package:rhythm_flutter/Providers/playlists.dart';
import 'package:rhythm_flutter/Providers/recently_played.dart';
import 'package:rhythm_flutter/Providers/tracks.dart';
import 'package:rhythm_flutter/Widgets/Mini_Player/mini_player.dart';
import '../Widgets/Track_List/track_list_widget.dart';
import '../Models/track.dart';
import '../Providers/albums.dart';
import 'package:provider/provider.dart';
import './playlist_add_tracks_page.dart';

class TrackListPage extends StatefulWidget {
  static const route = '/tracklist';

  @override
  _TrackListPageState createState() => _TrackListPageState();
}

class _TrackListPageState extends State<TrackListPage> {
  var _isInit = true;
  bool _isParty;
  Albums _albumProvider;
  RecentlyPlayed _recentlyPlayed;
  Playlists _playlistsProvider;
  Tracks _tracksProvider;
  List<Track> _tracks;
  String _name;
  String _playlistId;
  String _sharedBy;
  @override
  void didChangeDependencies() {
    if (_isInit) {
      final args =
          ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
      _isParty = args['partyMode'];
      var _routingFrom = args['routingFrom'];
      if (_routingFrom == 'playlist' || _routingFrom == 'shared') {
        _playlistId = args['id'];

        _name = args['name'];
        if (_routingFrom == 'shared') {
          _playlistsProvider = Provider.of<Playlists>(context);
          _sharedBy = _playlistsProvider
              .getSharedPlaylist(_playlistId)
              .sharedFriendName;
        }
      }
      _isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final args =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;

    final _routingFrom = args['routingFrom'];
    if (_routingFrom == 'favorites') {
      _tracksProvider = Provider.of<Tracks>(context);
      _tracks = _tracksProvider.favoriteTracks;
    } else if (_routingFrom == 'album') {
      _albumProvider = Provider.of<Albums>(context);
      _tracks = _albumProvider.tracks(args['id']);
    } else if (_routingFrom == 'recentlyPlayedAll') {
      _recentlyPlayed = Provider.of<RecentlyPlayed>(context);
      _tracks = _recentlyPlayed.tracks;
    } else {
      _playlistsProvider = Provider.of<Playlists>(context);
      _tracks = _playlistsProvider.getTracks(args['id']);
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(
          _routingFrom == 'favorites'
              ? 'Likes'
              : _routingFrom == 'album'
                  ? _albumProvider.getArtistName(
                      args['id'],
                    )
                  : '',
        ),
        actions: <Widget>[
          _routingFrom == 'playlist'
              ? IconButton(
                  icon: Icon(Icons.add),
                  onPressed: () {
                    Navigator.of(context)
                        .pushNamed(PlaylistAddPage.route, arguments: {
                      'id': _playlistId,
                      'tracks': _tracks,
                      'name': _name,
                    });
                  },
                )
              : SizedBox.shrink()
        ],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            child: ListView(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height * 0.3,
                  alignment: Alignment.center,
                  child: Container(
                    width: MediaQuery.of(context).size.height * 0.2,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: FadeInImage.assetNetwork(
                        placeholder: 'assets/colorful_loader.gif',
                        image: args['image'],
                      ),
                    ),
                  ),
                ),
                _routingFrom != 'favorites'
                    ? _routingFrom == 'shared'
                        ? Center(
                            child: Text(
                              args['name'] + ' - by ' + _sharedBy,
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 17),
                            ),
                          )
                        : Center(
                            child: Text(
                              args['name'],
                              style: TextStyle(fontSize: 19),
                            ),
                          )
                    : SizedBox.shrink(),
                _routingFrom == 'playlist'
                    ? _tracks.length == 0
                        ? Container(
                            margin: EdgeInsets.only(top: 10),
                            width: MediaQuery.of(context).size.width,
                            height: 250,
                            child: Center(
                              child: Column(
                                children: <Widget>[
                                  Divider(
                                    thickness: 4,
                                    color: Colors.redAccent,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 20),
                                    child: Column(
                                      children: <Widget>[
                                        Text(
                                          'You have no songs in this playlist.',
                                          style: TextStyle(fontSize: 20),
                                        ),
                                        SizedBox(
                                          height: 30,
                                        ),
                                        RaisedButton(
                                          color: Colors.redAccent,
                                          child: Text('Add songs'),
                                          onPressed: () {
                                            Navigator.of(context).pushNamed(
                                                PlaylistAddPage.route,
                                                arguments: {
                                                  'id': _playlistId,
                                                  'tracks': _tracks,
                                                  'name': _name,
                                                });
                                          },
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )
                        : TrackListWidget(
                            _tracks, 'playlist', _isParty, _playlistId)
                    : _routingFrom == 'shared'
                        ? _tracks.length == 0
                            ? Container(
                                margin: EdgeInsets.only(top: 10),
                                width: MediaQuery.of(context).size.width,
                                height: 250,
                                child: Center(
                                  child: Column(
                                    children: <Widget>[
                                      Divider(
                                        thickness: 4,
                                        color: Colors.redAccent,
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 20),
                                        child: Column(
                                          children: <Widget>[
                                            Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.8,
                                              child: Text(
                                                'Your friend has no songs in this playlist.',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(fontSize: 20),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              )
                            : TrackListWidget(
                                _tracks, 'shared', _isParty, _playlistId)
                        : _routingFrom == 'album'
                            ? TrackListWidget(_tracks, 'album', _isParty)
                            : _routingFrom == 'recentlyPlayedAll'
                                ? TrackListWidget(
                                    _tracks, 'recentlyPlayedAll', _isParty)
                                : TrackListWidget(
                                    _tracks, 'favorites', _isParty)
              ],
            ),
          ),
          MiniPlayer()
        ],
      ),
    );
  }
}
