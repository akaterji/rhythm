import 'package:flutter/material.dart';
import 'package:rhythm_flutter/Widgets/Playlist/add_playlist.dart';
import 'package:rhythm_flutter/Widgets/Playlist/playlist_details.dart';
import 'package:rhythm_flutter/constants.dart';
import 'package:provider/provider.dart';
import '../../Providers/playlists.dart';
import '../../Providers/playlist.dart';

class SharedPlaylistsWidget extends StatefulWidget {
  var _partyMode;
  SharedPlaylistsWidget(this._partyMode);
  @override
  _SharedPlaylistsWidgetState createState() => _SharedPlaylistsWidgetState();
}

class _SharedPlaylistsWidgetState extends State<SharedPlaylistsWidget> {
  var _isInit = true;
  Playlists _playlistsProvider;
  List<Playlist> _sharedPlaylists;

  var _partyMode;
  @override
  void didChangeDependencies() {
    if (_isInit) {
      _partyMode = widget._partyMode;
      _playlistsProvider = Provider.of<Playlists>(context);

      _isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var _deviceSize = MediaQuery.of(context).size;
    var _sharedPlaylistsRendered = 0;
    _sharedPlaylists = _playlistsProvider.sharedPlaylists;
    return Container(
      margin: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                child: Text(
                  'Shared playlists',
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w600),
                ),
                margin: EdgeInsets.only(left: 10, top: 10, bottom: 20),
              ),
            ],
          ),
          Container(
            height: _deviceSize.height * 0.3,
            child: ListView(
              children: <Widget>[
                Column(
                  children: List.generate(
                    (_sharedPlaylists.length / 3).ceil() + 1,
                    (index) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          _sharedPlaylistsRendered < _sharedPlaylists.length
                              ? PlaylistDetails(
                                  _sharedPlaylists[_sharedPlaylistsRendered]
                                          .tracks
                                          .isEmpty
                                      ? kDefualtPlaylistImage
                                      : _sharedPlaylists[
                                              _sharedPlaylistsRendered]
                                          .tracks[0]
                                          .getImage,
                                  _sharedPlaylists[_sharedPlaylistsRendered].id,
                                  _sharedPlaylists[_sharedPlaylistsRendered++]
                                      .name,
                                  true,
                                  _partyMode)
                              : SizedBox.shrink(),
                          _sharedPlaylistsRendered < _sharedPlaylists.length
                              ? PlaylistDetails(
                                  _sharedPlaylists[_sharedPlaylistsRendered]
                                          .tracks
                                          .isEmpty
                                      ? kDefualtPlaylistImage
                                      : _sharedPlaylists[
                                              _sharedPlaylistsRendered]
                                          .tracks[0]
                                          .getImage,
                                  _sharedPlaylists[_sharedPlaylistsRendered].id,
                                  _sharedPlaylists[_sharedPlaylistsRendered++]
                                      .name,
                                  true,
                                  _partyMode)
                              : SizedBox.shrink(),
                          _sharedPlaylistsRendered < _sharedPlaylists.length
                              ? PlaylistDetails(
                                  _sharedPlaylists[_sharedPlaylistsRendered]
                                          .tracks
                                          .isEmpty
                                      ? kDefualtPlaylistImage
                                      : _sharedPlaylists[
                                              _sharedPlaylistsRendered]
                                          .tracks[0]
                                          .getImage,
                                  _sharedPlaylists[_sharedPlaylistsRendered].id,
                                  _sharedPlaylists[_sharedPlaylistsRendered++]
                                      .name,
                                  true,
                                  _partyMode)
                              : SizedBox.shrink(),
                        ],
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
