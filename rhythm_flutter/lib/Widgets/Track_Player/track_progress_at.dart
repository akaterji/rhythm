import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import '../../Providers/track_player.dart';

class TrackProgressAt extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _trackProvider = Provider.of<TrackPlayer>(context);
    return Container(
      child: Text(
        _trackProvider.getPosition.toString().substring(2, 7),
        style: TextStyle(fontSize: 12),
      ),
      margin: EdgeInsets.only(left: 10),
    );
  }
}
