import 'package:flutter/material.dart';
import 'package:rhythm_flutter/CustomBottomSheets/add_playlist_sheet.dart';
import 'package:rhythm_flutter/customBottomSheet.dart';
import '../../Providers/playlists.dart';
import 'package:provider/provider.dart';

class AddNewPlaylistWidget extends StatefulWidget {
  final _isParty;
  AddNewPlaylistWidget(this._isParty);
  @override
  _AddNewPlaylistWidgetState createState() => _AddNewPlaylistWidgetState();
}

class _AddNewPlaylistWidgetState extends State<AddNewPlaylistWidget> {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Container(
        height: MediaQuery.of(context).size.height * 0.25,
        width: MediaQuery.of(context).size.width * 0.3,
        child: Column(
          children: <Widget>[
            InkWell(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.black26,
                  border: Border.all(color: Colors.grey),
                ),
                height: MediaQuery.of(context).size.height * 0.15,
                width: MediaQuery.of(context).size.width * 0.25,
                child: Icon(
                  Icons.add,
                  size: 75,
                  color: Colors.grey,
                ),
              ),
              onTap: () {
                showModalBottomSheetApp(
                  dismissOnTap: true,
                  context: context,
                  builder: (BuildContext ctx) {
                    return AddPlaylistSheet();
                  },
                );
              },
            ),
            widget._isParty
                ? SizedBox.shrink()
                : SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
            widget._isParty
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 7),
                        child: Text(
                          'New Playlist',
                          style: TextStyle(fontSize: 12),
                        ),
                      ),
                    ],
                  )
                : Row(
                    children: <Widget>[
                      Container(
                        child: Text(
                          'New Playlist',
                          textAlign: TextAlign.left,
                          style: TextStyle(fontSize: 12),
                        ),
                      ),
                    ],
                  )
          ],
        ),
      ),
    );
  }
}
