import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import '../../../Providers/party_member.dart';

class TrackProgressLeft extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _partyMemberProvider = Provider.of<PartyMember>(context);
    return Container(
      alignment: Alignment.centerRight,
      child: Text(_partyMemberProvider.isSet
          ? _partyMemberProvider.getTimeLeft().toString().substring(2, 7)
          : '00:00'),
      margin: EdgeInsets.only(right: 10),
    );
  }
}
