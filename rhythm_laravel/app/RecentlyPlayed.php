<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecentlyPlayed extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.u';
}
