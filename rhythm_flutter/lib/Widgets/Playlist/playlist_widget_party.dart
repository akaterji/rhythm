import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rhythm_flutter/Models/track.dart';
import 'package:rhythm_flutter/Pages/playlist_add_tracks_page.dart';
import 'package:rhythm_flutter/Widgets/Playlist/add_playlist.dart';
import 'package:rhythm_flutter/Widgets/Playlist/playlist_details.dart';
import 'package:rhythm_flutter/constants.dart';
import '../../Pages/party_owner_music_playback_page.dart';
import '../../Providers/playlists.dart';
import '../../Providers/playlist.dart';
import '../../Providers/party_playlist.dart';
import '../../Widgets/Playlist/playlist_list_widget.dart';
import '../../Widgets/Playlist/shared_playlist_list_widget.dart';

class PlaylistWidgetParty extends StatefulWidget {
  static const route = '/playlist_tab_copy';
  @override
  _PlaylistWidgetPartyState createState() => _PlaylistWidgetPartyState();
}

class _PlaylistWidgetPartyState extends State<PlaylistWidgetParty> {
  var _isInit = true;
  Playlists _playlistsProvider;
  List<Playlist> _playlists;
  List<Playlist> _sharedPlaylists;
  Playlist _selectedPlaylist;
  bool _isSelectedShared = false;
  PartyPlaylist _partyPlaylistProvider;
  TextEditingController _searchController;
  @override
  void initState() {
    _searchController = TextEditingController();
    _searchController.addListener(() {
      String text = _searchController.text;
      if (text.length == 0)
        _partyPlaylistProvider.reset();
      else {
        _partyPlaylistProvider.search(text);
      }
    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      _playlistsProvider = Provider.of<Playlists>(context);
      _partyPlaylistProvider = Provider.of<PartyPlaylist>(context);
      _playlists = _playlistsProvider.allPlaylists();
      _playlists.isEmpty
          ? _selectedPlaylist = null
          : _selectedPlaylist = _playlists[0];
      _isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var deviceInfo = MediaQuery.of(context).size;
    _playlistsProvider = Provider.of<Playlists>(context);
    _playlists = _playlistsProvider.playlists;
    _sharedPlaylists = _playlistsProvider.sharedPlaylists;
    if (_playlists.isNotEmpty) {
      _playlists.forEach((playlist) {
        if (playlist.id == _selectedPlaylist.id)
          _selectedPlaylist.setAllTracks(playlist.tracks);
      });
      if (_sharedPlaylists.isNotEmpty) {
        _sharedPlaylists.forEach(
          (playlist) {
            if (playlist.id == _selectedPlaylist.id) {
              _selectedPlaylist.setAllTracks(playlist.tracks);
            }
          },
        );
      }
      _partyPlaylistProvider.setTracks(_selectedPlaylist.tracks);
    }
    var _playlistsAdded = 0;
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        width: deviceInfo.width,
        height: deviceInfo.height,
        child: ListView(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Container(
                margin: EdgeInsets.only(top: 15),
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          'Select a playlist',
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      height: deviceInfo.height * 0.25,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: List.generate(
                          _playlists.length + 1 + _sharedPlaylists.length,
                          (index) {
                            return _playlists.isEmpty
                                ? _sharedPlaylists.isEmpty
                                    ? AddNewPlaylistWidget(true)
                                    : index == 0
                                        ? AddNewPlaylistWidget(true)
                                        : renderPlaylist(_playlistsAdded++)
                                : index == 0
                                    ? AddNewPlaylistWidget(true)
                                    : renderPlaylist(_playlistsAdded++);
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            _playlists.isEmpty
                ? SizedBox.shrink()
                : Container(
                    margin: EdgeInsets.only(top: 15),
                    height: deviceInfo.height * 0.5,
                    width: deviceInfo.width,
                    child: ListView(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(bottom: 10, left: 10),
                                child: Text(
                                  'Choosing from ' + _selectedPlaylist.name,
                                  style: TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.w600),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            )
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              height: deviceInfo.height * 0.2,
                              child: ListView(
                                scrollDirection: Axis.horizontal,
                                children: _searchController.text.length == 0
                                    ? _selectedPlaylist.tracks.length == 0
                                        ? List.generate(
                                            1,
                                            (index) {
                                              return Row(
                                                children: <Widget>[
                                                  addNewSongWidget(),
                                                ],
                                              );
                                            },
                                          )
                                        : List.generate(
                                            _selectedPlaylist.tracks.length,
                                            (index) {
                                            if (index == 0 &&
                                                !_isSelectedShared) {
                                              return Row(
                                                children: <Widget>[
                                                  addNewSongWidget(),
                                                  trackWidget(
                                                    _selectedPlaylist
                                                        .tracks[index],
                                                  ),
                                                ],
                                              );
                                            } else
                                              return trackWidget(
                                                  _selectedPlaylist
                                                      .tracks[index]);
                                          })
                                    : _partyPlaylistProvider.result.map(
                                        (track) {
                                          return trackWidget(track);
                                        },
                                      ).toList(),
                              ),
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: Container(
                                margin: EdgeInsets.only(top: 15),
                                child: Center(
                                  child: TextField(
                                    controller: _searchController,
                                    decoration: InputDecoration(
                                      focusColor: Colors.redAccent,
                                      fillColor: Colors.grey[700],
                                      filled: true,
                                      hintText: 'Search',
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(12.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                width: deviceInfo.width * 0.7,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
          ],
        ),
      ),
    );
  }

  void selectPlaylist(String playlistId, bool _isShared) {
    setState(() {
      if (!_isShared) {
        _selectedPlaylist =
            _playlists.firstWhere((playlist) => playlist.id == playlistId);
        _searchController.text = '';
        _isSelectedShared == true ? _isSelectedShared = false : null;
      } else {
        _selectedPlaylist = _sharedPlaylists
            .firstWhere((playlist) => playlist.id == playlistId);
        _searchController.text = '';
        _isSelectedShared == false ? _isSelectedShared = true : null;
      }
    });
  }

  Widget renderPlaylist(int index) {
    print(index);
    if (_playlists.length != null)
      return index >= _playlists.length
          ? _sharedPlaylists.length != 0
              ? PlaylistDetails(
                  _sharedPlaylists[index - _playlists.length].tracks.isEmpty
                      ? kDefualtPlaylistImage
                      : _sharedPlaylists[index - _playlists.length]
                          .tracks[0]
                          .getImage,
                  _sharedPlaylists[index - _playlists.length].id,
                  _sharedPlaylists[index - _playlists.length].name,
                  true,
                  true,
                  selectPlaylist,
                  _selectedPlaylist ==
                          _sharedPlaylists[index - _playlists.length]
                      ? true
                      : false)
              : SizedBox.shrink()
          : PlaylistDetails(
              _playlists[index].tracks.isEmpty
                  ? kDefualtPlaylistImage
                  : _playlists[index].tracks[0].getImage,
              _playlists[index].id,
              _playlists[index].name,
              false,
              true,
              selectPlaylist,
              _selectedPlaylist == _playlists[index] ? true : false);
  }

  Widget trackWidget(Track track) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.25,
      margin: EdgeInsets.only(left: 10),
      child: Column(
        children: <Widget>[
          InkWell(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: FadeInImage.assetNetwork(
                  placeholder: 'assets/colorful_loader.gif',
                  image: track.getImage,
                  height: MediaQuery.of(context).size.height * 0.15,
                ),
              ),
            ),
            onTap: () {
              Navigator.pushNamed(
                context,
                PartyMusicPlaybackPage.route,
                arguments: {
                  'index': _selectedPlaylist.tracks.indexOf(track),
                  'id': track.getImage,
                  'name': track.getName,
                  'album': track.getAlbum,
                  'artist': track.getArtist,
                  'url': track.getURL,
                  'image': track.getImage,
                  'trackList': _selectedPlaylist.tracks,
                },
              );
            },
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(top: 4),
              child: Text(
                track.getName + ' - ' + track.getArtist,
                style: TextStyle(fontSize: 12),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget addNewSongWidget() {
    return Container(
      // color: Colors.redAccent,
      width: MediaQuery.of(context).size.width * 0.25,
      margin: EdgeInsets.only(left: 10),
      child: Column(
        children: <Widget>[
          InkWell(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.black26,
                border: Border.all(color: Colors.grey),
              ),
              height: MediaQuery.of(context).size.height * 0.15,
              width: MediaQuery.of(context).size.width * 0.25,
              child: Icon(
                Icons.add,
                size: 75,
                color: Colors.grey,
              ),
            ),
            onTap: () {
              Navigator.of(context).pushNamed(
                PlaylistAddPage.route,
                arguments: {
                  'id': _selectedPlaylist.id,
                  'tracks': _selectedPlaylist.tracks,
                  'name': _selectedPlaylist.name
                },
              );
            },
          ),
          Expanded(
            child: Row(
              children: <Widget>[
                Container(
                  child: Text(
                    'New Song',
                    style: TextStyle(fontSize: 12),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
