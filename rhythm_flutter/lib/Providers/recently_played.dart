import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import '../constants.dart';
import '../Models/track.dart';

class RecentlyPlayed with ChangeNotifier {
  final String _token;
  final String _userId;
  bool _fetched = false;
  RecentlyPlayed(this._token, this._userId);

  List<Track> _tracks = [];

  Future<void> fetch(url, callback) async {
    try {
      final response =
          await http.get(url, headers: {'authorization': 'Bearer $_token'});
      if (response.statusCode != 200)
        throw json.decode(response.body)['message'];
      callback(response);
    } catch (error) {
      kError = true;
      key.currentState.removeCurrentSnackBar();
      key.currentState.showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          content: Text('Something went wrong'),
          action: SnackBarAction(
            label: 'Retry',
            onPressed: () {
              notifyListeners();
            },
          ),
        ),
      );
    }
  }

  Future<void> post(url, body, callback) async {
    try {
      final response = await http.post(url,
          headers: {'authorization': 'Bearer $_token'}, body: body);
      if (response.statusCode != 200)
        throw json.decode(response.body)['message'];

      callback(response);
    } catch (error) {
      kError = true;
      key.currentState.removeCurrentSnackBar();
      key.currentState.showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          content: Text('Something went wrong'),
          action: SnackBarAction(
            label: 'Retry',
            onPressed: () {
              notifyListeners();
            },
          ),
        ),
      );
    }
  }

  void fetchRecentlyPlayed() {
    fetch(
      kUrl + 'users/getAllRecentlyPlayed',
      (response) {
        _tracks = [];
        var tracks = json.decode(response.body)['tracks'];
        tracks.forEach(
          (track) {
            _tracks.add(
              Track(
                track['id'].toString(),
                track['name'],
                track['artist']['name'],
                track['album']['name'],
                track['url'],
                track['image'],
                track['isFavorite'],
              ),
            );
          },
        );
        notifyListeners();
      },
    );
  }

  List<Track> get tracks => _tracks;
}
