import 'package:flutter/widgets.dart';
import 'package:rhythm_flutter/Models/track.dart';

class PartyPlaylist with ChangeNotifier {
  List<Track> _tracks;
  String _playlistId;
  List<Track> _result = [];
  void search(String text) {
    _result = [];
    _tracks.forEach(
      (track) {
        if (track.getName.toLowerCase().contains(text)) {
          _result.add(track);
        }
      },
    );
    notifyListeners();
  }

  void reset() {
    _result = [];
    notifyListeners();
  }

  void setTracks(List<Track> tracks) => _tracks = tracks;
  List<Track> get result => _result;
}
