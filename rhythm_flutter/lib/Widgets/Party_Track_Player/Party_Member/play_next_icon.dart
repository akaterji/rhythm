import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../Providers/party_member.dart';

class PlayNextIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _partyMemberProvider = Provider.of<PartyMember>(context);
    return FloatingActionButton(
      heroTag: 'playNext',
      child: Icon(Icons.skip_next),
      onPressed: () {
        // _partyMemberProvider.playNext();
      },
    );
  }
}
