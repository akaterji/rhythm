import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rhythm_flutter/customBottomSheet.dart';

import '../../Providers/playlists.dart';
import '../../Providers/playlist.dart';
import '../../Providers/share_playlist_search.dart';

class PopupMenuButtonCustomPlaylist extends StatefulWidget {
  final String _playlistId;
  PopupMenuButtonCustomPlaylist(this._playlistId);

  @override
  _PopupMenuButtonCustomPlaylistState createState() =>
      _PopupMenuButtonCustomPlaylistState();
}

class _PopupMenuButtonCustomPlaylistState
    extends State<PopupMenuButtonCustomPlaylist> {
  Playlists _playlistsProvider;
  Playlist _playlistProvider;
  SharePlaylistSearch _searchProvider;
  TextEditingController _searchController;
  var _isInit = true;
  @override
  void initState() {
    _searchController = TextEditingController();
    _searchController.addListener(() {
      String text = _searchController.text;
      if (text.length == 0)
        _searchProvider.reset();
      else {
        _searchProvider.search(text);
      }
    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      _playlistsProvider = Provider.of<Playlists>(context);
      _playlistProvider = Provider.of<Playlist>(context);

      _isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    _playlistsProvider = Provider.of<Playlists>(context);
    return PopupMenuButton(
      itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
        PopupMenuItem<String>(
          value: '1',
          child: Row(
            children: <Widget>[
              Icon(Icons.delete),
              SizedBox(
                width: 10,
              ),
              Text(
                'Delete playlist',
              )
            ],
          ),
        ),
        PopupMenuItem<String>(
          value: '2',
          child: Row(
            children: <Widget>[
              Icon(Icons.share),
              SizedBox(
                width: 10,
              ),
              Text(
                'Share playlist',
              )
            ],
          ),
        ),
      ],
      onSelected: (value) async {
        if (value == '1') {
          _playlistsProvider.deletePlaylist(widget._playlistId);
        } else {
          showModalBottomSheetApp(
            dismissOnTap: true,
            context: context,
            builder: (BuildContext ctx) {
              var _playlistsProviderBottomSheet = Provider.of<Playlists>(ctx);
              List<dynamic> _playlistsNotSharedWithList =
                  _playlistsProviderBottomSheet
                      .playlistsNotSharedWithList(widget._playlistId);
              var _sharedWithCount = _playlistsProviderBottomSheet
                  .friendsSharedWith(widget._playlistId)
                  .length;
              _searchProvider = Provider.of<SharePlaylistSearch>(ctx);
              _searchProvider.setFriends(_playlistsNotSharedWithList);
              return Consumer<SharePlaylistSearch>(
                builder: (ctx, _searchProvider, _) => Container(
                  height: MediaQuery.of(ctx).size.height * 0.65,
                  child: ListView(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Search for friends',
                              style: TextStyle(fontSize: 15),
                            ),
                          ],
                        ),
                      ),
                      Divider(
                        color: Colors.white,
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          margin: EdgeInsets.only(top: 10),
                          width: MediaQuery.of(ctx).size.width * 0.75,
                          child: TextField(
                            controller: _searchController,
                            decoration: InputDecoration(
                              fillColor: Colors.grey[700],
                              filled: true,
                              hintText: 'Search',
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(15.0),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          margin: EdgeInsets.only(top: 10),
                          width: MediaQuery.of(ctx).size.width * 0.85,
                          child: Text(
                            'Share With',
                            style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.w600,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          margin: EdgeInsets.only(top: 20),
                          width: MediaQuery.of(ctx).size.width * 0.85,
                          height: MediaQuery.of(ctx).size.height * 0.15,
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: _searchProvider.result.isEmpty
                                ? _playlistsProviderBottomSheet
                                    .friendsNotSharedWith(widget._playlistId)
                                    .map(
                                    (friend) {
                                      return friendNotSharedWithWidget(
                                          friend.id,
                                          friend.firstName,
                                          friend.image,
                                          ctx);
                                    },
                                  ).toList()
                                : _searchProvider.result.map(
                                    (friend) {
                                      return friendNotSharedWithWidget(
                                          friend.id,
                                          friend.firstName,
                                          friend.image,
                                          ctx);
                                    },
                                  ).toList(),
                          ),
                        ),
                      ),
                      _sharedWithCount == 0
                          ? SizedBox.shrink()
                          : Divider(
                              thickness: 3,
                              color: Colors.redAccent,
                            ),
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          margin: EdgeInsets.only(top: 20),
                          width: MediaQuery.of(ctx).size.width * 0.85,
                          height: MediaQuery.of(ctx).size.height * 0.2,
                          child: Column(
                            children: <Widget>[
                              Container(
                                height: MediaQuery.of(ctx).size.height * 0.15,
                                child: ListView(
                                  scrollDirection: Axis.horizontal,
                                  children: _playlistsProviderBottomSheet
                                      .friendsSharedWith(widget._playlistId)
                                      .map(
                                    (friend) {
                                      return friendSharedWithWidget(friend.id,
                                          friend.firstName, friend.image, ctx);
                                    },
                                  ).toList(),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        }
      },
    );
  }

  Widget friendSharedWithWidget(
      String id, String name, String image, BuildContext ctx) {
    var _playlistsProviderBottomSheet = Provider.of<Playlists>(ctx);
    return InkWell(
      onTap: () {
        _playlistsProviderBottomSheet.removeShareWith(
            widget._playlistId, id, _playlistsProviderBottomSheet.friends);
      },
      child: Container(
        height: MediaQuery.of(ctx).size.height * 0.15,
        width: MediaQuery.of(ctx).size.width * 0.2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    height: MediaQuery.of(ctx).size.height * 0.08,
                    width: MediaQuery.of(ctx).size.width * 0.14,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: NetworkImage(image), fit: BoxFit.cover),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: 16,
                      width: 30,
                      margin: EdgeInsets.only(left: 40),
                      child: FloatingActionButton(
                        backgroundColor: Colors.red,
                        child: Icon(
                          Icons.close,
                          size: 8,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 5),
              child: Text(name),
            ),
          ],
        ),
      ),
    );
  }

  Widget friendNotSharedWithWidget(
      String id, String name, String image, BuildContext ctx) {
    var _playlistsProviderBottomSheet = Provider.of<Playlists>(ctx);
    return InkWell(
      onTap: () {
        _playlistsProviderBottomSheet.shareWith(
            widget._playlistId, id, _playlistsProviderBottomSheet.friends);
        _searchController.text = '';
      },
      child: Container(
        height: MediaQuery.of(ctx).size.height * 0.15,
        width: MediaQuery.of(ctx).size.width * 0.2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: MediaQuery.of(ctx).size.height * 0.1,
              width: MediaQuery.of(ctx).size.width * 0.17,
              decoration: BoxDecoration(
                color: Colors.red,
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: NetworkImage(image), fit: BoxFit.cover),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 5),
              child: Text(name),
            ),
          ],
        ),
      ),
    );
  }
}
