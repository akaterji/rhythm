<?php

namespace App\Http\Controllers;

use App\FcmToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FcmTokenController extends Controller
{
    public function store(Request $request)
    {
        $user = Auth::user();
        $fcm_token = new FcmToken();
        $fcm_token->user_id = $user->id;
        $fcm_token->token = $request->token;
        $fcm_token->save();
        return $fcm_token;
    }
    public function delete(Request $request)
    {
        $user = Auth::user();
        FcmToken::where([['user_id', $user->id],['token',$request->token]])->delete();
    }
}
