import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../constants.dart';
import 'package:firebase_database/firebase_database.dart';
import '../Models/user.dart';
import 'package:audioplayers/audioplayers.dart';
import 'dart:math';
import '../Models/track.dart';

class Party with ChangeNotifier {
  var _loading = false;
  final String _token;
  final String _userId;
  List<User> _friends = [];
  final url = kUrl;
  String _imageURL;
  bool _isInit = true;
  final databaseReference = FirebaseDatabase.instance.reference();
  AudioPlayer _trackPlayer = new AudioPlayer(playerId: 'partyOwner');
  var _position = Duration(seconds: 0);
  Duration _duration;
  Duration _timeLeft;
  var _firstIndex = false;
  List<Track> _shuffledList;
  var _shuffledIndex = 0;
  String _url;
  var _volume = 0.5;
  var _isSet = false;
  var _isRepeat = false;
  var _isShuffle = false;
  var _startedPlaying = false;
  var _isPlaying = false;
  var _positionListener;
  var _durationListener;
  List<Track> _trackList;
  int _index;
  String _name;
  Future<void> setAudioPlayer(
      String url, String imageURL, int index, List<Track> list) async {
    if (_url == url) return;
    _name = list[index].getName;
    _imageURL = imageURL;
    _trackPlayer.stop();
    _trackPlayer.release();
    _isSet = false;
    _isRepeat ? null : _isRepeat = false;
    _isShuffle ? null : _isShuffle = false;
    _startedPlaying = false;
    _isPlaying = false;
    _position = Duration(seconds: 0);
    _url = url;
    var _added = false;
    final result = await _trackPlayer.setUrl(_url);
    if (result == 1) {
      play();
      if (!_isShuffle) {
        _trackList = list;
        _index = index;
      }
      _durationListener = _trackPlayer.onDurationChanged.listen((Duration d) {
        _duration = d;
        if (_added) {
          updateFirebase({
            'duration': _duration.toString().substring(2, 7),
          });
        } else {
          updateFirebase(
            {
              'duration': _duration.toString().substring(2, 7),
              'image': list[index].getImage,
            },
          );
          _added = true;
        }
        _isSet = true;
        notifyListeners();
      });
      _trackPlayer.onPlayerError.listen((msg) {
        print('audioPlayer error : $msg');
      });
    }
    createParty();
    notifyListeners();
  }

  reset() {
    _duration = Duration(seconds: 0);
    _timeLeft = Duration(seconds: 0);
    _url = '';
    _isSet = false;
    _startedPlaying = false;
    _isPlaying = false;
    _name = '';
  }

  play() async {
    _startedPlaying
        ? updateFirebase(
            {'isPlaying': true},
          )
        : null;
    _trackPlayer.resume();
    _startedPlaying = true;
    _isPlaying = true;

    _positionListener =
        _trackPlayer.onAudioPositionChanged.listen((Duration p) {
      if (p.inSeconds == _duration.inSeconds) {
        if (_isRepeat) {
          _position = Duration(seconds: 0);
          _trackPlayer.stop();
          _trackPlayer.resume();
          notifyListeners();
        } else
          playNext();
      }

      _position = p;
      updateFirebase(
        {
          'position': _position.toString().substring(2, 7),
        },
      );
      notifyListeners();
    });
  }

  pause() async {
    await _trackPlayer.pause();
    _isPlaying = false;
    updateFirebase(
      {'isPlaying': _isPlaying},
    );
    notifyListeners();
  }

  changeVolume(double volume) {
    _volume = volume;
    _trackPlayer.setVolume(_volume);
    notifyListeners();
  }

  Party(this._token, this._userId);

  Future<void> fetch(url, callback) async {
    try {
      final response =
          await http.get(url, headers: {'authorization': 'Bearer $_token'});
      if (response.statusCode != 200) {
        throw json.decode(response.body)['message'];
      }
      callback(response);
    } catch (error) {
      kError = true;
      kErrorMessage = error;
      notifyListeners();
    }
  }

  Future<void> post(String url, dynamic body, callback) async {
    try {
      final response = await http.post(url,
          headers: {'authorization': 'Bearer $_token'}, body: body);
      if (response.statusCode != 200) {
        throw json.decode(response.body)['message'];
      }
      callback(response);
    } catch (error) {
      kError = true;
      kErrorMessage = error;
      notifyListeners();
    }
  }

  void updateFirebase(Map<String, dynamic> value) async {
    await databaseReference.child('Party').child(_userId).update(value);
  }

  void createParty() {
    databaseReference.child('Party').child(_userId).set(
      {
        'name': _name,
        'url': _url,
        'isPlaying': _isPlaying,
        'shuffle': _isShuffle,
        'repeat': _isRepeat,
        'position': '00:00',
        'duration': '00:00',
        'track_id': _trackList[_index].getId,
        'user_id': _userId,
        'image': _trackList[_index].getImage,
        'artist': _trackList[_index].getArtist,
        'album': _trackList[_index].getAlbum,
      },
    );
  }

  void exit() {
    _trackPlayer.release();
    reset();
    _durationListener.cancel();
    _positionListener.cancel();
    databaseReference.child('Party').child(_userId).remove();
    kIsParty = false;
    Future.delayed(
      Duration(milliseconds: 500),
      () {
        notifyListeners();
      },
    );
  }

  String get trackId => _trackList[_index].getId;
  bool get isSet => _isSet;

  bool get didTrackStart => _startedPlaying;

  bool get isTrackPlaying => _isPlaying;

  bool get isRepeat => _isRepeat;

  bool get isShuffle => _isShuffle;

  Duration get getPosition => _position;

  Duration get getDuration => _duration;

  double get getVolume => _volume;

  Duration getTimeLeft() {
    _timeLeft = _duration - _position;
    return _timeLeft;
  }

  String get getURL => _url;
  String get getName => _name;
  void setPosition(Duration p) {
    _position = p;
    notifyListeners();
  }

  void seekToSecond(int second) {
    Duration newDuration = Duration(seconds: second);
    _trackPlayer.seek(newDuration);
  }

  void toggleRepeat() {
    if (_isShuffle) _isShuffle = false;
    _isRepeat = !_isRepeat;
    notifyListeners();
    updateFirebase(
      {'repeat': _isRepeat},
    );
  }

  void toggleShuffle() {
    if (_isRepeat) _isRepeat = false;
    _isShuffle = !_isShuffle;
    _firstIndex = false;
    if (_isShuffle) {
      _shuffledList = [..._trackList];
      _shuffledIndex = 0;
      _shuffledList.shuffle();
      _firstIndex = true;
    }

    notifyListeners();
    updateFirebase(
      {'shuffle': _isShuffle},
    );
  }

  void playNext() {
    reset();
    if (_isShuffle) {
      if (_firstIndex) {
        setAudioPlayer(
            _shuffledList[_shuffledIndex].getURL,
            _shuffledList[_shuffledIndex].getImage,
            _shuffledIndex,
            _shuffledList);
        _firstIndex = false;
      } else {
        if (_shuffledIndex + 1 < _shuffledList.length)
          _shuffledIndex++;
        else
          _shuffledIndex = 0;
        setAudioPlayer(
            _shuffledList[_shuffledIndex].getURL,
            _shuffledList[_shuffledIndex].getImage,
            _shuffledIndex,
            _shuffledList);
      }
    } else {
      if (_index + 1 < _trackList.length)
        _index++;
      else
        _index = 0;

      setAudioPlayer(_trackList[_index].getURL, _trackList[_index].getImage,
          _index, _trackList);
    }
  }

  void playPrev() {
    reset();
    if (_isShuffle) {
      if (_shuffledIndex - 1 > -1)
        _shuffledIndex--;
      else
        _shuffledIndex = _shuffledList.length - 1;
      setAudioPlayer(
          _shuffledList[_shuffledIndex].getURL,
          _shuffledList[_shuffledIndex].getImage,
          _shuffledIndex,
          _shuffledList);
    } else {
      if (_index - 1 > -1)
        _index--;
      else
        _index = _trackList.length - 1;
      setAudioPlayer(_trackList[_index].getURL, _trackList[_index].getImage,
          _index, _trackList);
    }
  }

  int get index => _index;
  String get id => _trackList[_index].getId;
  String get album => _trackList[_index].getAlbum;
  String get artist => _trackList[_index].getArtist;
  String get image => _imageURL;
  List<Track> get tracks => _trackList;
}
