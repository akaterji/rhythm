import 'package:flutter/material.dart';
import 'package:marquee/marquee.dart';
import 'package:rhythm_flutter/Models/track.dart';
import 'package:rhythm_flutter/Pages/music_playback_page.dart';
import 'package:rhythm_flutter/Providers/tracks.dart';
import '../../Providers/track_player.dart';
import 'package:provider/provider.dart';

class MiniPlayer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;
    bool _isFavorite;
    Tracks _trackProvider;
    TrackPlayer _trackPlayerProvider = Provider.of<TrackPlayer>(context);
    if (_trackPlayerProvider.didTrackStart) {
      _trackProvider = Provider.of<Tracks>(context);
      _isFavorite = _trackProvider.isFavorite(_trackPlayerProvider.trackId);
    }
    return _trackPlayerProvider.didTrackStart
        ? Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: EdgeInsets.only(bottom: 15),
              decoration: BoxDecoration(
                // color: Color.fromRGBO(0xcc, 0x40, 0xcc, 1), //purple feti7
                // color: Color.fromRGBO(148, 0, 211, 1), // was gonna go with this
                color: Colors.redAccent,
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
              ),
              width: deviceSize.width * 0.95,
              height: 50,
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pushNamed(
                    MusicPlaybackPage.route,
                    arguments: {
                      'url': _trackPlayerProvider.getURL,
                      'currentlyPlaying': _trackPlayerProvider.currentlyPlaying,
                      'image': _trackPlayerProvider.image,
                      'trackList': _trackPlayerProvider.trackList,
                      '_index': _trackPlayerProvider.index
                    },
                  );
                },
                child: Row(
                  children: <Widget>[
                    IconButton(
                      padding: EdgeInsets.only(bottom: 5),
                      icon: Icon(
                        Icons.keyboard_arrow_up,
                        size: 35,
                      ),
                      onPressed: () {
                        Navigator.of(context).pushNamed(
                          MusicPlaybackPage.route,
                          arguments: {
                            'url': _trackPlayerProvider.getURL,
                            'currentlyPlaying':
                                _trackPlayerProvider.currentlyPlaying,
                            'image': _trackPlayerProvider.image,
                            'trackList': _trackPlayerProvider.trackList,
                            '_index': _trackPlayerProvider.index
                          },
                        );
                      },
                    ),
                    Expanded(
                      child: Container(
                        child: Marquee(
                          text: _trackPlayerProvider.getName +
                              ' - ' +
                              _trackPlayerProvider.artist,
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w600),
                          blankSpace: 50,
                        ),
                      ),
                    ),
                    _isFavorite
                        ? IconButton(
                            padding: EdgeInsets.only(left: 25),
                            iconSize: 30,
                            icon: Icon(Icons.favorite),
                            onPressed: () {
                              _trackProvider.toggleLike(
                                  _trackPlayerProvider.trackId, true);
                            },
                          )
                        : IconButton(
                            padding: EdgeInsets.only(left: 25),
                            iconSize: 30,
                            icon: Icon(Icons.favorite_border),
                            onPressed: () {
                              _trackProvider.toggleLike(
                                  _trackPlayerProvider.trackId, false);
                            },
                          ),
                    _trackPlayerProvider.isTrackPlaying
                        ? IconButton(
                            iconSize: 30,
                            color: Colors.white,
                            icon: Icon(Icons.pause_circle_filled),
                            onPressed: () {
                              _trackPlayerProvider.pause();
                            },
                          )
                        : IconButton(
                            // icon: Icon(Icons.play_circle_filled)
                            iconSize: 30,
                            color: Colors.white,
                            icon: Icon(Icons.play_circle_filled),
                            onPressed: () {
                              _trackPlayerProvider.play();
                            },
                          ),
                  ],
                ),
              ),
            ),
          )
        : SizedBox.shrink();
  }
}
