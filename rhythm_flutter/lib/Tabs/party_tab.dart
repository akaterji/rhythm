import 'package:flutter/material.dart';
import 'package:rhythm_flutter/Pages/party_owner_music_playback_page.dart';
import 'package:rhythm_flutter/Providers/party.dart';
import 'package:rhythm_flutter/Providers/party_member.dart';
import 'package:rhythm_flutter/Widgets/Mini_Player/mini_player.dart';
import '../Widgets/Party/party_list.dart';
import '../Widgets/Playlist/playlist_widget_party.dart';
import 'package:provider/provider.dart';

class PartyTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Party _partyProvider = Provider.of<Party>(context);
    PartyMember _partyMemberProvider = Provider.of<PartyMember>(context);
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Party',
          ),
          actions: <Widget>[
            _partyProvider.didTrackStart
                ? Row(
                    children: <Widget>[
                      Text(
                        _partyProvider.getName,
                        style: TextStyle(fontSize: 20),
                      ),
                      IconButton(
                        icon: Icon(Icons.arrow_forward),
                        onPressed: () {
                          Navigator.pushNamed(
                            context,
                            PartyMusicPlaybackPage.route,
                            arguments: {
                              'index': _partyProvider.index,
                              'id': _partyProvider.id,
                              'name': _partyProvider.getName,
                              'album': _partyProvider.album,
                              'artist': _partyProvider.artist,
                              'url': _partyProvider.getURL,
                              'image': _partyProvider.image,
                              'trackList': _partyProvider.tracks,
                            },
                          );
                        },
                      ),
                    ],
                  )
                : Row(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          Icons.add,
                          size: 28,
                        ),
                        onPressed: () {
                          _partyMemberProvider.enteredParty
                              ? showDialog(
                                  context: context,
                                  barrierDismissible: true,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: Text("Creating a new party"),
                                      content: Text(
                                          "You are already in a party, are you sure you want to quit?"),
                                      actions: <Widget>[
                                        // usually buttons at the bottom of the dialog
                                        FlatButton(
                                          child: Text("Cancel"),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                        FlatButton(
                                          child: Text("Yes"),
                                          onPressed: () {
                                            _partyMemberProvider.exit();
                                            Navigator.of(context)
                                                .pushReplacementNamed(
                                              PlaylistWidgetParty.route,
                                              arguments: {'isParty': true},
                                            );
                                          },
                                        ),
                                      ],
                                    );
                                  },
                                )
                              : Navigator.pushNamed(
                                  context,
                                  PlaylistWidgetParty.route,
                                  arguments: {'isParty': true},
                                );
                        },
                      ),
                    ],
                  )
          ],
        ),
        body: Stack(
          children: <Widget>[
            Container(
              child: ListView(
                children: <Widget>[
                  PartyList(),
                ],
              ),
            ),
            MiniPlayer()
          ],
        ));
  }
}
