import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../Pages/music_playback_page.dart';
import '../../Models/track.dart';
import '../../Providers/search_playlist.dart';
import '../../constants.dart';

class PlaylistAdd extends StatefulWidget {
  final List<Track> _trackList;
  PlaylistAdd(this._trackList);

  @override
  _PlaylistAddState createState() => _PlaylistAddState();
}

class _PlaylistAddState extends State<PlaylistAdd> {
  @override
  Widget build(BuildContext context) {
    var _searchPlaylistProvider = Provider.of<SearchPlaylistProvider>(context);
    return Container(
      height: MediaQuery.of(context).size.height * 0.66,
      child: ListView(
          children: widget._trackList.map((item) {
        return InkWell(
          onTap: () {
            Navigator.pushNamed(context, MusicPlaybackPage.route, arguments: {
              'index': widget._trackList.indexOf(item),
              'id': item.getImage,
              'name': item.getName,
              'album': item.getAlbum,
              'artist': item.getArtist,
              'url': item.getURL,
              'image': item.getImage,
              'trackList': widget._trackList,
              'currentlyPlaying': 'playlistSearch'
            });
          },
          child: Container(
            margin: EdgeInsets.only(top: 15),
            height: 100,
            child: Row(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: FadeInImage.assetNetwork(
                    placeholder: 'assets/colorful_loader.gif',
                    image: item.getImage,
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      child: Text(item.getName),
                      margin: EdgeInsets.only(left: 10),
                      width: 200,
                    ),
                    Container(
                      child: Text(
                        item.getArtist,
                        style: TextStyle(color: Colors.grey),
                      ),
                      margin: EdgeInsets.only(left: 10),
                      width: 200,
                    ),
                  ],
                ),
                Spacer(), // fix it to make it position of pop up menu to be dynamic
                Checkbox(
                  activeColor: Colors.white,
                  checkColor: Colors.black,
                  onChanged: (checked) {
                    _searchPlaylistProvider.select(item.getId);
                  },
                  value: item.selected,
                )
              ],
            ),
          ),
        );
      }).toList()),
    );
  }
}
