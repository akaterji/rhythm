import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import '../constants.dart';
import '../Models/album.dart';
import '../Models/track.dart';

class Albums with ChangeNotifier {
  final String _token;
  final String _userId;
  Albums(this._token, this._userId);

  List<Album> _albums = [];

  Future<void> fetch(url, callback) async {
    try {
      final response =
          await http.get(url, headers: {'authorization': 'Bearer $_token'});
      if (response.statusCode != 200)
        throw json.decode(response.body)['message'];
      callback(response);
    } catch (error) {
      kError = true;
      key.currentState.removeCurrentSnackBar();
      key.currentState.showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          content: Text('Something went wrong'),
          action: SnackBarAction(
            label: 'Retry',
            onPressed: () {
              notifyListeners();
            },
          ),
        ),
      );
    }
  }

  Future<void> post(url, body, callback) async {
    try {
      final response = await http.post(url,
          headers: {'authorization': 'Bearer $_token'}, body: body);
      if (response.statusCode != 200)
        throw json.decode(response.body)['message'];

      callback(response);
    } catch (error) {
      kError = true;
      key.currentState.removeCurrentSnackBar();
      key.currentState.showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          content: Text('Something went wrong'),
          action: SnackBarAction(
            label: 'Retry',
            onPressed: () {
              notifyListeners();
            },
          ),
        ),
      );
    }
  }

  void fetchAlbums() {
    fetch(
      kUrl + 'albums',
      (response) {
        _albums = [];
        var albums = json.decode(response.body);
        albums.forEach(
          (album) {
            List<Track> _tracks = listOfTracks(album['tracks']);
            _albums.add(
              Album(album['id'].toString(), album['name'],
                  album['artist']['name'], _tracks, album['imageURL']),
            );
          },
        );
        notifyListeners();
      },
    );
  }

  List<Track> listOfTracks(List<dynamic> data) {
    List<Track> _trackList = [];
    List<dynamic> _tracks = data;
    _tracks.forEach((track) {
      _trackList.add(
        Track(
          track['id'].toString(),
          track['name'],
          track['artist']['name'],
          track['album']['name'],
          track['url'],
          track['image'],
          track['isFavorite'],
        ),
      );
    });
    return _trackList;
  }

  List<Track> tracks(String albumId) {
    List<Track> _list;
    _albums.forEach((album) {
      if (album.id == albumId) {
        _list = album.trackList;
        return;
      }
    });
    return _list;
  }

  String getArtistName(String albumId) {
    String _artistName = '';
    _albums.forEach((album) {
      if (album.id == albumId) return _artistName = album.artistName;
    });
    return _artistName;
  }

  List<Album> get albums => _albums;
}
