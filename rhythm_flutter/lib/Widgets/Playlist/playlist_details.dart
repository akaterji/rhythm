import 'package:flutter/material.dart';
import 'package:rhythm_flutter/Pages/track_list_page.dart';
import 'package:rhythm_flutter/Widgets/Playlist/PopupMenuButtonCustomPlaylist.dart';
import 'package:rhythm_flutter/Widgets/Playlist/PopupMenuButtonCustomSharedPlaylist.dart';

class PlaylistDetails extends StatelessWidget {
  final _imageURL;
  final _playlistName;
  final _playlistId;
  final _partyMode;
  final _isShared;
  final _selectPlaylist;
  final _selected;
  PlaylistDetails(this._imageURL, this._playlistId, this._playlistName,
      this._isShared, this._partyMode,
      [this._selectPlaylist, this._selected]);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.25,
      width: _partyMode
          ? MediaQuery.of(context).size.width * 0.27
          : MediaQuery.of(context).size.width * 0.3,
      child: Column(
        children: <Widget>[
          _partyMode
              ? _selected
                  ? InkWell(
                      child: Align(
                        // alignment: Alignment.centerLeft,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            border: Border.all(color: Colors.red, width: 2),
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: FadeInImage.assetNetwork(
                              placeholder: 'assets/colorful_loader.gif',
                              image: _imageURL,
                              height: MediaQuery.of(context).size.height * 0.15,
                            ),
                          ),
                        ),
                      ),
                      onTap: () {
                        _partyMode
                            ? _selectPlaylist(this._playlistId, this._isShared)
                            : Navigator.of(context).pushNamed(
                                TrackListPage.route,
                                arguments: {
                                  'id': _playlistId,
                                  'name': _playlistName,
                                  'image': _imageURL,
                                  'routingFrom':
                                      _isShared ? 'shared' : 'playlist',
                                  'partyMode': _partyMode
                                },
                              );
                      },
                    )
                  : InkWell(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: FadeInImage.assetNetwork(
                          placeholder: 'assets/colorful_loader.gif',
                          image: _imageURL,
                          height: MediaQuery.of(context).size.height * 0.15,
                        ),
                      ),
                      onTap: () {
                        _partyMode
                            ? _selectPlaylist(this._playlistId, this._isShared)
                            : Navigator.of(context).pushNamed(
                                TrackListPage.route,
                                arguments: {
                                  'id': _playlistId,
                                  'name': _playlistName,
                                  'image': _imageURL,
                                  'routingFrom':
                                      _isShared ? 'shared' : 'playlist',
                                  'partyMode': _partyMode
                                },
                              );
                      },
                    )
              : InkWell(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: FadeInImage.assetNetwork(
                      placeholder: 'assets/colorful_loader.gif',
                      image: _imageURL,
                      height: MediaQuery.of(context).size.height * 0.15,
                    ),
                  ),
                  onTap: () {
                    _partyMode
                        ? _selectPlaylist(this._playlistId)
                        : Navigator.of(context).pushNamed(
                            TrackListPage.route,
                            arguments: {
                              'id': _playlistId,
                              'name': _playlistName,
                              'image': _imageURL,
                              'routingFrom': _isShared ? 'shared' : 'playlist',
                              'partyMode': _partyMode
                            },
                          );
                  },
                ),
          Container(
            child: _partyMode
                ? Row(
                    mainAxisAlignment: _partyMode
                        ? MainAxisAlignment.center
                        : MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 7),
                              child: Text(
                                _playlistName,
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 12),
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            _isShared
                                ? Text(
                                    '(Shared)',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.grey),
                                  )
                                : SizedBox.shrink()
                          ],
                        ),
                      ),
                    ],
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        // width: 45,
                        child: Container(
                          margin: EdgeInsets.only(left: 10, top: 7),
                          child: Text(
                            _playlistName,
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 10),
                          ),
                        ),
                      ),
                      // Spacer(),
                      _isShared
                          ? PopupMenuButtonCustomSharedPlaylist(_playlistId)
                          : PopupMenuButtonCustomPlaylist(_playlistId)
                    ],
                  ),
          ),
        ],
      ),
    );
  }
}
