import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../Providers/track_player.dart';

class ShuffleIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _trackProvider = Provider.of<TrackPlayer>(context);
    return Container(
      width: 40,
      height: 40,
      child: RawMaterialButton(
        shape: CircleBorder(),
        elevation: 0.0,
        child: _trackProvider.isShuffle
            ? Icon(
                Icons.shuffle,
                color: Colors.red,
              )
            : Icon(Icons.shuffle),
        onPressed: () {
          _trackProvider.toggleShuffle();
        },
      ),
    );
  }
}
