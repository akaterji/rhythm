import 'package:flutter/widgets.dart';
import 'package:audioplayers/audioplayers.dart';
import 'dart:math';
import '../Models/track.dart';
import 'package:http/http.dart' as http;

import '../constants.dart';

class TrackPlayer with ChangeNotifier {
  final String _token;
  final String _userId;
  TrackPlayer(this._token, this._userId);
  AudioPlayer _trackPlayer = new AudioPlayer(playerId: 'localPlayer');
  var _position = Duration(seconds: 0);
  Duration _duration;
  Duration _timeLeft;
  List<Track> _shuffledList;
  String _url;
  String _currentlyPlaying = '';
  String _trackId;
  var _shouldRefresh = false;
  var _volume = 0.5;
  var _isSet = false;
  var _isRepeat = false;
  var _isShuffle = false;
  var _startedPlaying = false;
  var _isPlaying = false;
  var _firstIndex = false;
  var _newTrack = false;
  String _imageURL = '';
  List<Track> _trackList;
  int _index;
  var _shuffledIndex = 0;
  String _name;
  Future<void> setAudioPlayer(
      String url, String imageURL, int index, List<Track> list) async {
    if (_url == url) if (!_shouldRefresh) return;
    http.post(
      kUrl + 'users/recentlyPlayed',
      body: {'track_id': list[index].getId},
      headers: {'authorization': 'Bearer $_token'},
    );
    http.post(
      kUrl + 'users/addRecentlyPlayed',
      body: {'track_id': list[index].getId},
      headers: {'authorization': 'Bearer $_token'},
    );
    _newTrack = true;
    _trackId = list[index].getId;
    _shouldRefresh = false;
    _name = list[index].getName;
    _imageURL = imageURL;
    _trackPlayer.stop();
    _trackPlayer.release();
    _isSet = false;
    _startedPlaying = false;
    _isPlaying = false;
    _position = Duration(seconds: 0);
    _url = url;
    final result = await _trackPlayer.setUrl(_url);
    if (result == 1) {
      play();
      if (!_isShuffle) {
        _trackList = list;
        _index = index;
      }
      _trackPlayer.onDurationChanged.listen((Duration d) {
        _duration = d;
        _isSet = true;
        notifyListeners();
      });
      _trackPlayer.onPlayerError.listen((msg) {
        print('audioPlayer error : $msg');
      });
    }
    notifyListeners();
  }

  void setCurrentlyPlaying(String mode) {
    if (_currentlyPlaying != mode) {
      _shouldRefresh = true;
      _currentlyPlaying = mode;
      // _isShuffle = false;
      // _isRepeat = false;
      // notifyListeners();
    }
  }

  reset() {
    _duration = Duration(seconds: 0);
    _timeLeft = Duration(seconds: 0);
    _url = '';
    _isSet = false;
    _startedPlaying = false;
    _isPlaying = false;
    _name = '';
  }

  play() async {
    _trackPlayer.resume();
    _startedPlaying = true;
    _isPlaying = true;
    _trackPlayer.onAudioPositionChanged.listen((Duration p) {
      if (p.inSeconds == _duration.inSeconds) {
        if (_isRepeat) {
          _position = Duration(seconds: 0);
          _trackPlayer.stop();
          _trackPlayer.resume();
          notifyListeners();
        } else
          playNext();
      }
      _position = p;
      notifyListeners();
    });
  }

  pause() async {
    await _trackPlayer.pause();
    _isPlaying = false;
    notifyListeners();
  }

  stop() async {
    await _trackPlayer.stop();
    _isPlaying = false;
    _position = Duration(seconds: 0);
    reset();
    _startedPlaying = false;
    notifyListeners();
  }

  changeVolume(double volume) {
    _volume = volume;
    _trackPlayer.setVolume(_volume);
    notifyListeners();
  }

  bool get isSet => _isSet;

  bool get didTrackStart => _startedPlaying;

  bool get isTrackPlaying => _isPlaying;

  bool get isRepeat => _isRepeat;

  bool get isShuffle => _isShuffle;

  Duration get getPosition => _position;

  Duration get getDuration => _duration;

  double get getVolume => _volume;

  Duration getTimeLeft() {
    _timeLeft = _duration - _position;
    return _timeLeft;
  }

  String get getURL => _url;
  String get getName => _name;
  void setPosition(Duration p) {
    _position = p;
    notifyListeners();
  }

  void seekToSecond(int second) {
    Duration newDuration = Duration(seconds: second);
    _trackPlayer.seek(newDuration);
  }

  void toggleRepeat() {
    if (_isShuffle) _isShuffle = false;
    _isRepeat = !_isRepeat;
    notifyListeners();
  }

  void toggleShuffle() {
    if (_isRepeat) _isRepeat = false;
    _isShuffle = !_isShuffle;
    _firstIndex = false;
    if (_isShuffle) {
      _shuffledList = [..._trackList];
      _shuffledIndex = 0;
      _shuffledList.shuffle();
      _firstIndex = true;
    }

    notifyListeners();
  }

  void playNext() {
    reset();
    if (_isShuffle) {
      if (_firstIndex) {
        setAudioPlayer(
            _shuffledList[_shuffledIndex].getURL,
            _shuffledList[_shuffledIndex].getImage,
            _shuffledIndex,
            _shuffledList);
        _firstIndex = false;
      } else {
        if (_shuffledIndex + 1 < _shuffledList.length)
          _shuffledIndex++;
        else
          _shuffledIndex = 0;
        setAudioPlayer(
            _shuffledList[_shuffledIndex].getURL,
            _shuffledList[_shuffledIndex].getImage,
            _shuffledIndex,
            _shuffledList);
      }
    } else {
      if (_index + 1 < _trackList.length)
        _index++;
      else
        _index = 0;

      setAudioPlayer(_trackList[_index].getURL, _trackList[_index].getImage,
          _index, _trackList);
    }
  }

  void playPrev() {
    reset();
    if (_isShuffle) {
      if (_shuffledIndex - 1 > -1)
        _shuffledIndex--;
      else
        _shuffledIndex = _shuffledList.length - 1;
      setAudioPlayer(
          _shuffledList[_shuffledIndex].getURL,
          _shuffledList[_shuffledIndex].getImage,
          _shuffledIndex,
          _shuffledList);
    } else {
      if (_index - 1 > -1)
        _index--;
      else
        _index = _trackList.length - 1;
      setAudioPlayer(_trackList[_index].getURL, _trackList[_index].getImage,
          _index, _trackList);
    }
  }

  void fetchedRecentlyPlayed() {
    _newTrack = false;
  }

  bool get newTrack => _newTrack;
  String get album {
    if (_isShuffle) {
      return _trackList[_shuffledIndex].getAlbum;
    }

    return _trackList[_index].getAlbum;
  }

  String get artist {
    if (_isShuffle) {
      return _trackList[_shuffledIndex].getArtist;
    }

    return _trackList[_index].getArtist;
  }

  String get trackId {
    return _trackId;
  }

  String get currentlyPlaying => _currentlyPlaying;
  String get image => _imageURL;
  List<Track> get trackList => _trackList;
  int get index {
    if (_isShuffle)
      return _shuffledIndex;
    else
      return _index;
  }
}
