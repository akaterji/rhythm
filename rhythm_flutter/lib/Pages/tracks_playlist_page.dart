import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rhythm_flutter/Providers/playlists.dart';
import 'package:rhythm_flutter/Widgets/Track_List/PopupTracks.dart';
import '../Pages/music_playback_page.dart';
import '../Pages/party_owner_music_playback_page.dart';
import '../Pages/playlist_add_tracks_page.dart';

import '../Providers/playlist.dart';

class TracksPlaylistPage extends StatefulWidget {
  static const route = '/tracksPlaylistPage';

  @override
  _TracksPlaylistPageState createState() => _TracksPlaylistPageState();
}

class _TracksPlaylistPageState extends State<TracksPlaylistPage> {
  var _isInit = false;
  var _playlistProvider;
  var _playlistsProvider;
  bool _partyMode;
  bool _sharedPlaylist;
  @override
  void didChangeDependencies() {
    if (!_isInit) {
      final args =
          ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
      final String _playlistName = args['name'];
      final String _playlistId = args['id'];
      _playlistProvider = Provider.of<Playlist>(context);
      _playlistsProvider = Provider.of<Playlists>(context);
      args['partyMode'] ? _partyMode = true : _partyMode = false;
      args['sharedPlaylist'] ? _sharedPlaylist = true : _sharedPlaylist = false;
      _playlistProvider.setPlaylist(_playlistId, _playlistName);
      _playlistProvider.getTracks();
      _isInit = true;
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: !_sharedPlaylist
          ? AppBar(
              title: Text(_playlistProvider.name),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.pushNamed(context, PlaylistAddPage.route,
                        arguments: {
                          'name': _playlistProvider.name,
                          'id': _playlistProvider.id,
                          'tracks': _playlistProvider.tracks,
                        });
                  },
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                )
              ],
            )
          : AppBar(
              title: Text(_playlistProvider.name),
            ),
      body: Consumer<Playlist>(
        builder: (ctx, _playlistProvider, _) => Container(
          child: Column(
              children: _playlistProvider.tracks.map(
            (item) {
              return InkWell(
                onTap: () {
                  _partyMode
                      ? Navigator.pushNamed(
                          context,
                          PartyMusicPlaybackPage.route,
                          arguments: {
                            'index': _playlistProvider.tracks.indexOf(item),
                            'id': item.getId,
                            'name': item.getName,
                            'album': item.getAlbum,
                            'artist': item.getArtist,
                            'url': item.getURL,
                            'image': item.getImage,
                            'trackList': _playlistProvider.tracks,
                            'image': item.getImage
                          },
                        )
                      : Navigator.pushNamed(
                          context,
                          MusicPlaybackPage.route,
                          arguments: {
                            'index': _playlistProvider.tracks.indexOf(item),
                            'id': item.getId,
                            'name': item.getName,
                            'album': item.getAlbum,
                            'artist': item.getArtist,
                            'url': item.getURL,
                            'image': item.getImage,
                            'trackList': _playlistProvider.tracks,
                            'currentlyPlaying': 'playlist'
                          },
                        );
                },
                child: Container(
                  margin: EdgeInsets.only(top: 15),
                  height: 100,
                  child: Row(
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: FadeInImage.assetNetwork(
                          placeholder: 'assets/colorful_loader.gif',
                          image: item.getImage,
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text(item.getName),
                          Text(
                            item.getArtist,
                            style: TextStyle(color: Colors.grey),
                          ),
                        ],
                      ),
                      Spacer(),
                      if (!_sharedPlaylist)
                        InkWell(
                          child: Icon(
                            Icons.delete,
                          ),
                          onTap: () {
                            _playlistsProvider.removeTrack(
                                _playlistProvider.id, item.getId);
                          },
                        )
                      else
                        PopupTracks(item.getId)
                      // PopupTracks()
                    ],
                  ),
                ),
              );
            },
          ).toList()),
        ),
      ),
    );
  }
}
