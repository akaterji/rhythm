<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Track extends Model
{
    public function playlists()
    {
        return $this->belongsToMany('App\Playlist');
    }
    public function album()
    {
        return $this->belongsTo('App\Album');
    }
    public function artist()
    {
        return $this->belongsTo('App\Artist');
    }
}
