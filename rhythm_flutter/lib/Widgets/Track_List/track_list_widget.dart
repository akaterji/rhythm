import 'package:flutter/material.dart';
import 'package:rhythm_flutter/Pages/party_owner_music_playback_page.dart';
import 'package:rhythm_flutter/Providers/playlist.dart';
import 'package:rhythm_flutter/Providers/playlists.dart';
import 'package:rhythm_flutter/Providers/party.dart';
import 'package:rhythm_flutter/Providers/party_member.dart';
import 'package:rhythm_flutter/Widgets/Playlist/PopupPlaylistTracks.dart';
import '../../Pages/music_playback_page.dart';
import './PopupTracks.dart';
import '../../Models/track.dart';
import 'package:provider/provider.dart';

class TrackListWidget extends StatefulWidget {
  final List<Track> _trackList;
  final String _currentlyPlaying;
  final _playlistId;
  final bool _isParty;
  TrackListWidget(this._trackList, this._currentlyPlaying, this._isParty,
      [this._playlistId]);

  @override
  _TrackListWidgetState createState() => _TrackListWidgetState();
}

class _TrackListWidgetState extends State<TrackListWidget> {
  var _isInit = true;
  Playlists _playlistsProvider;
  Playlist _playlist;
  Party _partyProvider;
  PartyMember _partyMemberProvider;
  @override
  void didChangeDependencies() {
    if (_isInit) {
      _partyProvider = Provider.of<Party>(context);
      _partyMemberProvider = Provider.of<PartyMember>(context);
      _playlistsProvider = Provider.of<Playlists>(context);
      if (widget._currentlyPlaying == 'shared')
        _playlist = _playlistsProvider.getSharedPlaylist(widget._playlistId);
      else if (widget._currentlyPlaying == 'playlist')
        _playlist = _playlistsProvider.getPlaylist(widget._playlistId);
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget._currentlyPlaying == 'recentlyPlayedAll'
          ? MediaQuery.of(context).size.height
          : widget._currentlyPlaying == 'search'
              ? MediaQuery.of(context).size.height * 0.66
              : MediaQuery.of(context).size.height * 0.5,
      child: ListView(
          children: widget._trackList.map((item) {
        return InkWell(
          onTap: () {
            widget._isParty
                ? Navigator.pushNamed(
                    context,
                    PartyMusicPlaybackPage.route,
                    arguments: {
                      'index': _playlist.tracks.indexOf(item),
                      'id': item.getImage,
                      'name': item.getName,
                      'album': item.getAlbum,
                      'artist': item.getArtist,
                      'url': item.getURL,
                      'image': item.getImage,
                      'trackList': _playlist.tracks,
                    },
                  )
                : widget._currentlyPlaying == 'Playlist'
                    ? _partyProvider.didTrackStart
                        ? showDialog(
                            context: context,
                            barrierDismissible: true,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text("Quitting your party"),
                                content: Text(
                                    "Leaving your party will force all members to be kicked, are you sure you want to quit?"),
                                actions: <Widget>[
                                  // usually buttons at the bottom of the dialog
                                  FlatButton(
                                    child: Text("Cancel"),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  FlatButton(
                                    child: Text("Yes"),
                                    onPressed: () {
                                      _partyMemberProvider.exit();
                                      Navigator.pushReplacementNamed(
                                        context,
                                        MusicPlaybackPage.route,
                                        arguments: {
                                          'index':
                                              _playlist.tracks.indexOf(item),
                                          'id': item.getImage,
                                          'name': item.getName,
                                          'album': item.getAlbum,
                                          'artist': item.getArtist,
                                          'url': item.getURL,
                                          'image': item.getImage,
                                          'trackList': _playlist.tracks,
                                          'currentlyPlaying':
                                              widget._currentlyPlaying
                                        },
                                      );
                                    },
                                  ),
                                ],
                              );
                            },
                          )
                        : _partyMemberProvider.enteredParty
                            ? showDialog(
                                context: context,
                                barrierDismissible: true,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text("Leaving party"),
                                    content: Text(
                                        "You are already in a party, are you sure you want to quit?"),
                                    actions: <Widget>[
                                      // usually buttons at the bottom of the dialog
                                      FlatButton(
                                        child: Text("Cancel"),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                      FlatButton(
                                        child: Text("Yes"),
                                        onPressed: () {
                                          _partyMemberProvider.exit();
                                          Navigator.pushReplacementNamed(
                                            context,
                                            MusicPlaybackPage.route,
                                            arguments: {
                                              'index': _playlist.tracks
                                                  .indexOf(item),
                                              'id': item.getImage,
                                              'name': item.getName,
                                              'album': item.getAlbum,
                                              'artist': item.getArtist,
                                              'url': item.getURL,
                                              'image': item.getImage,
                                              'trackList': _playlist.tracks,
                                              'currentlyPlaying':
                                                  widget._currentlyPlaying
                                            },
                                          );
                                        },
                                      ),
                                    ],
                                  );
                                },
                              )
                            : Navigator.pushReplacementNamed(
                                context,
                                MusicPlaybackPage.route,
                                arguments: {
                                  'index': _playlist.tracks.indexOf(item),
                                  'id': item.getImage,
                                  'name': item.getName,
                                  'album': item.getAlbum,
                                  'artist': item.getArtist,
                                  'url': item.getURL,
                                  'image': item.getImage,
                                  'trackList': _playlist.tracks,
                                  'currentlyPlaying': widget._currentlyPlaying
                                },
                              )
                    : _partyProvider.didTrackStart
                        ? showDialog(
                            context: context,
                            barrierDismissible: true,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text("Quitting your party"),
                                content: Text(
                                    "Leaving your party will force all members to be kicked, are you sure you want to quit?"),
                                actions: <Widget>[
                                  // usually buttons at the bottom of the dialog
                                  FlatButton(
                                    child: Text("Cancel"),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  FlatButton(
                                    child: Text("Yes"),
                                    onPressed: () {
                                      _partyMemberProvider.exit();
                                      Navigator.pushReplacementNamed(
                                        context,
                                        MusicPlaybackPage.route,
                                        arguments: {
                                          'index':
                                              widget._trackList.indexOf(item),
                                          'id': item.getImage,
                                          'name': item.getName,
                                          'album': item.getAlbum,
                                          'artist': item.getArtist,
                                          'url': item.getURL,
                                          'image': item.getImage,
                                          'trackList': widget._trackList,
                                          'currentlyPlaying':
                                              widget._currentlyPlaying
                                        },
                                      );
                                    },
                                  ),
                                ],
                              );
                            },
                          )
                        : _partyMemberProvider.enteredParty
                            ? showDialog(
                                context: context,
                                barrierDismissible: true,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text("Leaving party"),
                                    content: Text(
                                        "You are already in a party, are you sure you want to quit?"),
                                    actions: <Widget>[
                                      // usually buttons at the bottom of the dialog
                                      FlatButton(
                                        child: Text("Cancel"),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                      FlatButton(
                                        child: Text("Yes"),
                                        onPressed: () {
                                          _partyMemberProvider.exit();
                                          Navigator.pushReplacementNamed(
                                            context,
                                            MusicPlaybackPage.route,
                                            arguments: {
                                              'index': widget._trackList
                                                  .indexOf(item),
                                              'id': item.getImage,
                                              'name': item.getName,
                                              'album': item.getAlbum,
                                              'artist': item.getArtist,
                                              'url': item.getURL,
                                              'image': item.getImage,
                                              'trackList': widget._trackList,
                                              'currentlyPlaying':
                                                  widget._currentlyPlaying
                                            },
                                          );
                                        },
                                      ),
                                    ],
                                  );
                                },
                              )
                            : Navigator.pushNamed(
                                context,
                                MusicPlaybackPage.route,
                                arguments: {
                                  'index': widget._trackList.indexOf(item),
                                  'id': item.getImage,
                                  'name': item.getName,
                                  'album': item.getAlbum,
                                  'artist': item.getArtist,
                                  'url': item.getURL,
                                  'image': item.getImage,
                                  'trackList': widget._trackList,
                                  'currentlyPlaying': widget._currentlyPlaying
                                },
                              );
          },
          child: Container(
            margin: EdgeInsets.only(top: 15),
            height: 100,
            child: Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(5),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(12.0),
                    child: FadeInImage.assetNetwork(
                        placeholder: 'assets/colorful_loader.gif',
                        image: item.getImage,
                        height: 85,
                        width: 85),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      width: 200,
                      child: Text(
                        item.getName,
                      ),
                    ),
                    Container(
                      width: 200,
                      child: Text(
                        item.getArtist,
                        style: TextStyle(color: Colors.grey),
                      ),
                    ),
                  ],
                ),
                Spacer(), // fix it to make it position of pop up menu to be dynamic
                widget._currentlyPlaying == 'playlist'
                    ? PopupPlaylistTracks(item.getId, widget._playlistId)
                    : PopupTracks(item.getId)
              ],
            ),
          ),
        );
      }).toList()),
    );
  }
}
