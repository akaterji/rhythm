import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../Providers/party_member.dart';

class PlayPrevIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _partyMemberProvider = Provider.of<PartyMember>(context);
    return FloatingActionButton(
      heroTag: 'playPrev',
      child: Icon(Icons.skip_previous),
      onPressed: () {
        // _partyMemberProvider.playPrev();
      },
    );
  }
}
