import 'package:flutter/material.dart';
import 'package:rhythm_flutter/Pages/music_playback_page.dart';
import 'package:rhythm_flutter/Providers/party.dart';
import 'package:rhythm_flutter/Providers/party_member.dart';
import 'package:rhythm_flutter/Providers/tracks.dart';
import 'package:provider/provider.dart';
import 'package:rhythm_flutter/Widgets/Track_List/PopupTracks.dart';

class FavoritesWidget extends StatefulWidget {
  @override
  _FavoritesWidgetState createState() => _FavoritesWidgetState();
}

class _FavoritesWidgetState extends State<FavoritesWidget> {
  var _isInit = true;
  Party _partyProvider;
  PartyMember _partyMemberProvider;
  @override
  void didChangeDependencies() {
    if (_isInit) {
      _partyProvider = Provider.of<Party>(context);
      _partyMemberProvider = Provider.of<PartyMember>(context);
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var _tracksProvider = Provider.of<Tracks>(context);
    var _tracks = _tracksProvider.favoriteTracks;
    return Column(
        children: _tracks.map(
      (item) {
        return InkWell(
          onTap: () {
            _partyProvider.didTrackStart
                ? showDialog(
                    context: context,
                    barrierDismissible: true,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("Quitting your party"),
                        content: Text(
                            "Leaving your party will force all members to be kicked, are you sure you want to quit?"),
                        actions: <Widget>[
                          // usually buttons at the bottom of the dialog
                          FlatButton(
                            child: Text("Cancel"),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          FlatButton(
                            child: Text("Yes"),
                            onPressed: () {
                              _partyMemberProvider.exit();
                              Navigator.pushReplacementNamed(
                                context,
                                MusicPlaybackPage.route,
                                arguments: {
                                  'index': _tracks.indexOf(item),
                                  'id': item.getImage,
                                  'name': item.getName,
                                  'album': item.getAlbum,
                                  'artist': item.getArtist,
                                  'url': item.getURL,
                                  'image': item.getImage,
                                  'trackList': _tracks,
                                  'currentlyPlaying': 'favorites'
                                },
                              );
                            },
                          ),
                        ],
                      );
                    },
                  )
                : _partyMemberProvider.enteredParty
                    ? showDialog(
                        context: context,
                        barrierDismissible: true,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("Leaving party"),
                            content: Text(
                                "You are already in a party, are you sure you want to quit?"),
                            actions: <Widget>[
                              // usually buttons at the bottom of the dialog
                              FlatButton(
                                child: Text("Cancel"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                              FlatButton(
                                child: Text("Yes"),
                                onPressed: () {
                                  _partyMemberProvider.exit();
                                  Navigator.pushReplacementNamed(
                                    context,
                                    MusicPlaybackPage.route,
                                    arguments: {
                                      'index': _tracks.indexOf(item),
                                      'id': item.getImage,
                                      'name': item.getName,
                                      'album': item.getAlbum,
                                      'artist': item.getArtist,
                                      'url': item.getURL,
                                      'image': item.getImage,
                                      'trackList': _tracks,
                                      'currentlyPlaying': 'favorites'
                                    },
                                  );
                                },
                              ),
                            ],
                          );
                        },
                      )
                    : Navigator.pushNamed(
                        context,
                        MusicPlaybackPage.route,
                        arguments: {
                          'index': _tracks.indexOf(item),
                          'id': item.getImage,
                          'name': item.getName,
                          'album': item.getAlbum,
                          'artist': item.getArtist,
                          'url': item.getURL,
                          'image': item.getImage,
                          'trackList': _tracks,
                          'currentlyPlaying': 'favorites'
                        },
                      );
          },
          child: Container(
            margin: EdgeInsets.only(top: 15),
            height: 100,
            child: Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(5),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(12.0),
                    child: FadeInImage.assetNetwork(
                        placeholder: 'assets/colorful_loader.gif',
                        image: item.getImage,
                        height: 85,
                        width: 85),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      width: 200,
                      child: Text(
                        item.getName,
                      ),
                    ),
                    Container(
                      width: 200,
                      child: Text(
                        item.getArtist,
                        style: TextStyle(color: Colors.grey),
                      ),
                    ),
                  ],
                ),
                Spacer(), // fix it to make it position of pop up menu to be dynamic
                PopupTracks(item.getId)
              ],
            ),
          ),
        );
      },
    ).toList());
  }
}
