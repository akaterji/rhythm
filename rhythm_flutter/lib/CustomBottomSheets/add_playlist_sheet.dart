import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../Providers/playlists.dart';

class AddPlaylistSheet extends StatefulWidget {
  @override
  _AddPlaylistSheetState createState() => _AddPlaylistSheetState();
}

class _AddPlaylistSheetState extends State<AddPlaylistSheet> {
  GlobalKey<FormState> _playlistKey = GlobalKey();
  TextEditingController _newPlaylistController;
  @override
  void initState() {
    _newPlaylistController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.3,
      child: ListView(
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 10),
                child: Text(
                  'Choose a name',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
              Spacer(),
              RaisedButton(
                child: Text(
                  "Done",
                  style: TextStyle(color: Colors.black),
                ),
                color: Colors.white,
                onPressed: () {
                  if (_playlistKey.currentState.validate()) {
                    Provider.of<Playlists>(context)
                        .addPlaylist(_newPlaylistController.text);
                    Navigator.of(context).pop();
                  }
                },
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(30.0),
                ),
              ),
            ],
          ),
          Form(
            key: _playlistKey,
            child: Container(
              margin: EdgeInsets.all(18),
              child: TextFormField(
                validator: (val) {
                  if (val.isEmpty)
                    return 'Please enter a name';
                  else
                    return null;
                },
                controller: _newPlaylistController,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
