import 'package:flutter/material.dart';
import 'package:rhythm_flutter/Providers/playlists.dart';
import 'package:provider/provider.dart';
import './PopupMenuButtonCustomPlaylist.dart';

import '../../Providers/playlist.dart';

import '../../Pages/tracks_playlist_page.dart';

class PlaylistListWidget extends StatelessWidget {
  final bool _partyMode;
  PlaylistListWidget(this._partyMode);
  @override
  Widget build(BuildContext context) {
    Playlists _playlistsProvider = Provider.of<Playlists>(context);
    List<Playlist> _playlists = _playlistsProvider.playlists;
    return Container(
      child: Column(
          children: _playlists.map(
        (item) {
          return InkWell(
            onTap: () {
              Navigator.pushNamed(
                context,
                TracksPlaylistPage.route,
                arguments: {
                  'id': item.id,
                  'name': item.name,
                  'trackList': item.tracks,
                  'partyMode': _partyMode,
                  'sharedPlaylist': false
                },
              );
            },
            child: Container(
              margin: EdgeInsets.only(top: 15),
              height: 100,
              child: Row(
                children: <Widget>[
                  // Image.network(item.getImage),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text(item.name),
                    ],
                  ),
                  Spacer(), // fix it to make it position of pop up menu to be dynamic
                  PopupMenuButtonCustomPlaylist(item.id)
                ],
              ),
            ),
          );
        },
      ).toList()),
    );
  }
}
