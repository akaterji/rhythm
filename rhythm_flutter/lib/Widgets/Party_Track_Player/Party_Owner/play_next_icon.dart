import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../Providers/party.dart';

class PlayNextIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _partyProvider = Provider.of<Party>(context);
    return IconButton(
      icon: Icon(Icons.skip_next, size: 30),
      onPressed: () {
        _partyProvider.playNext();
      },
    );
  }
}
