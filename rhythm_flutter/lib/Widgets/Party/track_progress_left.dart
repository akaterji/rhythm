import 'package:flutter/material.dart';

class PartyListTrackProgressLeft extends StatelessWidget {
  final _left;
  final _isListening;
  PartyListTrackProgressLeft(this._left, this._isListening);
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      child: Text(
        _left,
        style: TextStyle(
            fontSize: 12,
            color: this._isListening ? Colors.white : Colors.black),
      ),
      margin: EdgeInsets.only(right: 10),
    );
  }
}
