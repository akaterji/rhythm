<?php

namespace App\Listeners;

use App\User;
use App\Events\FriendRequestEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class SendPushNotificationToClient
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FriendRequestEvent  $event
     * @return void
     */
    public function handle(FriendRequestEvent $event)
    {
        $title = 'Friend Request';
        $body = $event->name. ' wants to add you';
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)
                    ->setSound('default')
                    ->setClickAction('FLUTTER_NOTIFICATION_CLICK');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'my_data']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $tokens = User::find($event->friend_id)->fcmTokens()->get();
        foreach ($tokens as $token) {
            $downstreamResponse = FCM::sendTo($token->token, $option, $notification, $data);
        }
    }
}
